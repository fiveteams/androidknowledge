package androidknowledge.five.com.study.activity;

import android.content.Intent;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;


import androidknowledge.five.com.study.presenter.activitypresenter.StudyChildActivityPresenter;

public class StudyChildActivity extends BaseActivityPresenter<StudyChildActivityPresenter>{

    @Override
    public Class<StudyChildActivityPresenter> getPresenter() {
        return StudyChildActivityPresenter.class;
    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void initMethod() {
        super.initMethod();
        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        setTitleShowOrHint(true);
        setImageBack(true);
        setTitleName(title);
    }
}
