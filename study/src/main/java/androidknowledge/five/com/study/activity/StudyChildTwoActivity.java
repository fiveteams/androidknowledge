package androidknowledge.five.com.study.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;

import androidknowledge.five.com.study.presenter.activitypresenter.StudyTwoActivityPresenter;

public class StudyChildTwoActivity extends BaseActivityPresenter<StudyTwoActivityPresenter>{

    @Override
    public Class<StudyTwoActivityPresenter> getPresenter() {
        return StudyTwoActivityPresenter.class;
    }
}
