package androidknowledge.five.com.study.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.study.R;
import androidknowledge.five.com.study.activity.LinkActivity;
import androidknowledge.five.com.study.activity.StudyChildActivity;
import androidknowledge.five.com.study.adapter.StudyChildAdapter;
import androidknowledge.five.com.study.model.StudyChildBean;

/**
 * 作者：马利亚
 * 日期：2018/12/14
 * 内容：点击学习里面的类
 */
public class StudyChildActivityPresenter extends AppDelegate {
    private Context mContext;
    private MaterialRefreshLayout mRefresh;
    private RecyclerView mRecycle;
    private StudyChildAdapter mStudyChildAdapter;
    private String mStudyId;
    private int mIsList;
    private SimpleDraweeView mTitleImage;
    private StudyChildBean mStudyChildBean;
    private TextView mStudyBotton;

    @Override
    public int getlayoutId() {
        return R.layout.activity_study_child;
    }

    @Override
    public void initData() {
        //初始化控件
        mStudyBotton = (TextView) get(R.id.tv_study_bottom_txt);
        mRefresh = (MaterialRefreshLayout) get(R.id.mrl_opensource_frament_refresh);
        mRecycle = (RecyclerView) get(R.id.rv_opensource_frament_recycle);
        mTitleImage = get(R.id.sdv_study_child);
        //获取id
        Intent intent = ((StudyChildActivity) mContext).getIntent();
        mStudyId = intent.getStringExtra("study_id");
        mIsList = intent.getIntExtra("isList", 0);
        //设置适配器
        mStudyChildAdapter = new StudyChildAdapter(mContext);
        LinearLayoutManager mlinearLayoutManager = new LinearLayoutManager(mContext);
        mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecycle.setLayoutManager(mlinearLayoutManager);
        mRecycle.setAdapter(mStudyChildAdapter);
        //刷新
        initRefreshLayout();
        doHttp();
    }

    /**
     * 刷新
     */
    private void initRefreshLayout() {
        //设置支持下拉加载更多
        mRefresh.setLoadMore(true);
        //刷新以及加载回调
        mRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                //向服务器请求数据
                doHttp();
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                doHttp();
            }
        });
    }

    /**
     * 解析数据
     */
    private void doHttp() {
        new Utility(mContext).isLoadCache(Http.STUDY_CHILD, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                mStudyChildBean = JsonToBean.jsonToBean(data, StudyChildBean.class);
                if (!mStudyChildBean.isIsTop()) {
                    mTitleImage.setVisibility(View.VISIBLE);
                    mTitleImage.setImageURI(mStudyChildBean.getTopimg());
                    String title = mStudyChildBean.getTitle();
                    mStudyBotton.setText(title + "");
                    mStudyBotton.setVisibility(View.VISIBLE);
                    mTitleImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String link = mStudyChildBean.getLink();
                            Intent intent = new Intent(((StudyChildActivity) mContext), LinkActivity.class);
                            intent.putExtra("link", link);
                            mContext.startActivity(intent);
                        }
                    });
                } else {
                    mStudyBotton.setVisibility(View.GONE);
                    mTitleImage.setVisibility(View.GONE);
                }
                mStudyChildAdapter.setList(mStudyChildBean.getItems());
                mRefresh.finishRefresh();
                mRefresh.finishRefreshLoadMore();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.STUDY_CHILD + mStudyId + ".txt", null);

    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }
}
