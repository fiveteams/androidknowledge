package androidknowledge.five.com.study.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.study.R;
import androidknowledge.five.com.study.activity.LinkActivity;
import androidknowledge.five.com.study.activity.StudyChildTwoActivity;
import androidknowledge.five.com.study.model.StudyChildTwoBean;

/**
 * 作者：马利亚
 * 日期：2018/12/19
 * 内容：
 */
public class StudyChildTwoAdapter extends RecyclerView.Adapter<StudyChildTwoAdapter.MyViewHolderrr>{
    private Context mContext;
    private List<StudyChildTwoBean.ItemsBean> mList=new ArrayList<>();

    public StudyChildTwoAdapter(Context context) {
        this.mContext=context;
    }

    @NonNull
    @Override
    public StudyChildTwoAdapter.MyViewHolderrr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=View.inflate(mContext, R.layout.study_child_two_layout,null);
        MyViewHolderrr myViewHolderrr = new MyViewHolderrr(view);
        return myViewHolderrr;
    }

    @Override
    public void onBindViewHolder(@NonNull StudyChildTwoAdapter.MyViewHolderrr myViewHolderrr, final int i) {
        myViewHolderrr.tv_study_child_two.setText(mList.get(i).getTitle());
        myViewHolderrr.mRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             Intent intent = new Intent(((StudyChildTwoActivity) mContext), LinkActivity.class);
             String link = mList.get(i).getLink();
             intent.putExtra("link",link);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<StudyChildTwoBean.ItemsBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolderrr extends RecyclerView.ViewHolder {
        private RelativeLayout mRelativeLayout;
        private TextView tv_study_child_two;

        public MyViewHolderrr(@NonNull View itemView) {
            super(itemView);
            mRelativeLayout=(RelativeLayout)itemView.findViewById(R.id.mRelativeLayout);
            tv_study_child_two=(TextView)itemView.findViewById(R.id.tv_study_child_two);
        }
    }
}
