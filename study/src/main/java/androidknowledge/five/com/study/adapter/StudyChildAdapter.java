package androidknowledge.five.com.study.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.study.R;
import androidknowledge.five.com.study.activity.LinkActivity;
import androidknowledge.five.com.study.activity.StudyChildActivity;
import androidknowledge.five.com.study.model.StudyChildBean;

/**
 * 作者：马利亚
 * 日期：2018/12/13
 * 内容：
 */
public class StudyChildAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<StudyChildBean.ItemsBean> mList = new ArrayList<>();

    public StudyChildAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = View.inflate(mContext, R.layout.adapter_article_layout, null);
            return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        if (viewHolder instanceof MyViewHolder){
            ((MyViewHolder)viewHolder).mImg.setImageURI(mList.get(i).getImage());
            ((MyViewHolder)viewHolder).mTitle.setText(mList.get(i).getTitle());
            ((MyViewHolder)viewHolder).mName.setText(mList.get(i).getAuthor());
            ((MyViewHolder)viewHolder).mIntro.setText(mList.get(i).getDesc());
            ((MyViewHolder)viewHolder).mTime.setText(mList.get(i).getTime());
            ((MyViewHolder)viewHolder).mRellayout1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String link = mList.get(i).getLink();
                    Intent intent = new Intent(((StudyChildActivity)mContext), LinkActivity.class);
                    intent.putExtra("link",link);
                    mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<StudyChildBean.ItemsBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout mRellayout1;
        TextView mTime;
        TextView mIntro;
        TextView mName;
        TextView mTitle;
        SimpleDraweeView mImg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mRellayout1 = (RelativeLayout) itemView.findViewById(R.id.rl_article_frament_rellayout1);
            mTime = (TextView) itemView.findViewById(R.id.tv_article_frament_time);
            mImg = (SimpleDraweeView) itemView.findViewById(R.id.iv_article_frament_img);
            mTitle = (TextView) itemView.findViewById(R.id.tv_article_frament_title);
            mName = (TextView) itemView.findViewById(R.id.tv_article_frament_name);
            mIntro = (TextView) itemView.findViewById(R.id.tv_article_frament_intro);
        }
    }

}
