package androidknowledge.five.com.study.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.LodingDialog;

import androidknowledge.five.com.study.R;
import androidknowledge.five.com.study.activity.LinkActivity;


/**
 * 作者：马利亚
 * 日期：2018/12/13
 * 内容：
 */
public class LinkActivityPresenter extends AppDelegate{
    private Context mContext;
    private WebView mWeb;
    private LodingDialog mLodingDialog;
    private String mNameEmail;

    @Override
    public int getlayoutId() {
        return R.layout.activity_link;
    }

    @Override
    public void initData() {
        //初始化控件
        mWeb=(WebView)get(R.id.wb_link_activity_web);
        Intent intent = ((LinkActivity) mContext).getIntent();
        String link = intent.getStringExtra("link");
        mWeb.loadUrl(link);
        mLodingDialog = new LodingDialog.Builder(mContext).create();
        mWeb.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mLodingDialog.hide();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mLodingDialog.show();
            }
        });
    }

    @Override
    public void getContext(Context context) {
        this.mContext=context;
    }

    public void setWebDestroy() {
        mWeb.destroy();
    }
}
