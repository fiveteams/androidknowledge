package androidknowledge.five.com.study.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;

import java.util.List;

import androidknowledge.five.com.study.R;
import androidknowledge.five.com.study.activity.StudyChildTwoActivity;
import androidknowledge.five.com.study.adapter.StudyChildTwoAdapter;
import androidknowledge.five.com.study.model.StudyChildTwoBean;

/**
 * 作者：马利亚
 * 日期：2018/12/19
 * 内容：
 */
public class StudyTwoActivityPresenter extends AppDelegate{

    private Context mContext;
    private MaterialRefreshLayout mRefresh;
    private String mStudyId;
    private int mIsList;
    private StudyChildTwoAdapter mStudyChildTwoAdapter;
    private RecyclerView mStudyTwoActivityRecycler;

    @Override
    public int getlayoutId() {
        return R.layout.activity_study_two;
    }

    @Override
    public void initData() {
        //获取控件
        mStudyTwoActivityRecycler=(RecyclerView)get(R.id.study_two_activity_recycler);
        mRefresh=(MaterialRefreshLayout) get(R.id.mrl_studychild_two_refresh);
        //获取id
        Intent intent = ((StudyChildTwoActivity) mContext).getIntent();
        mStudyId = intent.getStringExtra("study_id");
        //设置适配器
        mStudyChildTwoAdapter = new StudyChildTwoAdapter(mContext);
        LinearLayoutManager mlinearLayoutManager = new LinearLayoutManager(mContext);
        mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mStudyTwoActivityRecycler.setLayoutManager(mlinearLayoutManager);
        mStudyTwoActivityRecycler.setAdapter(mStudyChildTwoAdapter);
        //刷新
        initRefreshLayout();
        doHttp();
    }
    /**
     * 刷新
     * */
    private void initRefreshLayout() {
        //设置支持下拉加载更多
        mRefresh.setLoadMore(true);
        //刷新以及加载回调
        mRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                //向服务器请求数据
                doHttp();

            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                    doHttp();
            }
        });
    }
    /**
     * 解析数据
     * */
    private void doHttp() {
        new Utility(mContext).isLoadCache(Http.STUDY_CHILD,true,true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                StudyChildTwoBean mStudyChildTwoBean=JsonToBean.jsonToBean(data, StudyChildTwoBean.class);
                List<StudyChildTwoBean.ItemsBean> items = mStudyChildTwoBean.getItems();
                mStudyChildTwoAdapter.setList(items);
                mRefresh.finishRefresh();
                mRefresh.finishRefreshLoadMore();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.STUDY_CHILD+mStudyId+".txt",null);

    }


    @Override
    public void getContext(Context context) {
        this.mContext=context;
    }
}
