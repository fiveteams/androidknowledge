package androidknowledge.five.com.study.activity;
import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import androidknowledge.five.com.study.presenter.activitypresenter.LinkActivityPresenter;

public class LinkActivity extends BaseActivityPresenter<LinkActivityPresenter>{


    @Override
    public Class<LinkActivityPresenter> getPresenter() {
        return LinkActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("详情");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.setWebDestroy();
    }
}
