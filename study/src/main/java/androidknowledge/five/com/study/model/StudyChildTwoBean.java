package androidknowledge.five.com.study.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * 作者：马利亚
 * 日期：2018/12/19
 * 内容：
 */
public class StudyChildTwoBean implements IModel{

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * title : Kotlin 鏁欑▼
         * link : http://www.runoob.com/kotlin/kotlin-tutorial.html
         * id : kotlin_01
         */

        private String title;
        private String link;
        private String id;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
