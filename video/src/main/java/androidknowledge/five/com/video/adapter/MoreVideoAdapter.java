package androidknowledge.five.com.video.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.video.fragment.MoreVideoFragment;
import androidknowledge.five.com.video.model.MoreVideoBean;
import androidknowledge.five.com.video.model.MoreVideoTitleBean;

/**
 * @author:杜威
 * @Date:2018/12/19
 * @time:下午 2:49
 * @description:video头部
 */
public class MoreVideoAdapter extends FragmentPagerAdapter {

    private List<String> mTitleList = new ArrayList<>();
    private List<MoreVideoFragment> mFragmentList = new ArrayList<>();
    private List<MoreVideoTitleBean.ItemsBean> mItemList = new ArrayList<>();
    public MoreVideoAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        MoreVideoFragment moreVideoFragment = mFragmentList.get(i);
        Bundle bundle = new Bundle();
        bundle.putString("type",mItemList.get(i).getType());
        moreVideoFragment.setArguments(bundle);
        return mFragmentList.get(i);
    }

    @Override
    public int getCount() {
        return mTitleList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }

    public void setList(List<String> mTitleList, List<MoreVideoFragment> mFragmentList, List<MoreVideoTitleBean.ItemsBean> items) {
        this.mTitleList = mTitleList;
        this.mFragmentList = mFragmentList;
        this.mItemList = items;
        notifyDataSetChanged();
    }
}
