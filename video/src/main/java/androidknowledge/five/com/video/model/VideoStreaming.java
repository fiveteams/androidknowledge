package androidknowledge.five.com.video.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * @author:杜威
 * @Date:2018/12/18
 * @time:下午 7:49
 * @description:
 */
public class VideoStreaming implements IModel {

    /**
     * dltype : 1
     * exem : 0
     * fl : {"cnt":2,"fi":[{"id":100701,"name":"msd","lmt":0,"sb":1,"cname":"标清;(270P)","br":11,"profile":2,"drm":0,"video":1,"audio":1,"fs":1336491,"super":0,"sl":1},{"id":2,"name":"mp4","lmt":0,"sb":1,"cname":"高清;(480P)","br":16,"profile":1,"drm":0,"video":1,"audio":1,"fs":3764190,"super":0,"sl":0}]}
     * hs : 0
     * ip : 103.66.217.232
     * ls : 0
     * preview : 110
     * s : o
     * sfl : {"cnt":0}
     * tm : 1545133699
     * vl : {"cnt":1,"vi":[{"br":11,"ch":0,"cl":{"fc":0,"keyid":"q1424b6snvr.100701"},"ct":21600,"drm":0,"dsb":0,"fmd5":"06877918bbadf720edfbbc61a67d2962","fn":"q1424b6snvr.m701.mp4","fs":1336491,"fst":5,"fvkey":"D55D470698003FE876597A250D9F39799B706854CDA054996E74DEDE649C8E8EB25415B7D6EA8161D0C60C8BC55F9AFE4AF71644DC8BDF52AABCA6DB97654ABA7BED496AFDF337C473127B15AF672AFBB801157024D70ED561E0193229A74845180EF060CB05FF97","head":0,"hevc":0,"iflag":0,"level":0,"lnk":"q1424b6snvr","logo":1,"mst":8,"pl":null,"share":1,"sp":0,"st":2,"tail":0,"td":"110.40","ti":"1-1 课程简介","tie":0,"type":5124,"ul":{"ui":[{"url":"http://203.205.158.76/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/","vt":200,"dtc":0,"dt":2},{"url":"http://203.205.158.84/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/","vt":200,"dtc":0,"dt":2},{"url":"http://203.205.158.85/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/","vt":200,"dtc":0,"dt":2},{"url":"http://video.dispatch.tc.qq.com/","vt":0,"dtc":0,"dt":2}]},"vh":272,"vid":"q1424b6snvr","videotype":0,"vr":0,"vst":2,"vw":480,"wh":1.7647059,"wl":{"wi":[]},"uptime":1520483786,"fvideo":0,"fvpint":0,"swhdcp":0,"ad":{"adsid":"","adpinfo":""}}]}
     */

    private int dltype;
    private int exem;
    private FlBean fl;
    private int hs;
    private String ip;
    private int ls;
    private int preview;
    private String s;
    private SflBean sfl;
    private int tm;
    private VlBean vl;

    public int getDltype() {
        return dltype;
    }

    public void setDltype(int dltype) {
        this.dltype = dltype;
    }

    public int getExem() {
        return exem;
    }

    public void setExem(int exem) {
        this.exem = exem;
    }

    public FlBean getFl() {
        return fl;
    }

    public void setFl(FlBean fl) {
        this.fl = fl;
    }

    public int getHs() {
        return hs;
    }

    public void setHs(int hs) {
        this.hs = hs;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getLs() {
        return ls;
    }

    public void setLs(int ls) {
        this.ls = ls;
    }

    public int getPreview() {
        return preview;
    }

    public void setPreview(int preview) {
        this.preview = preview;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public SflBean getSfl() {
        return sfl;
    }

    public void setSfl(SflBean sfl) {
        this.sfl = sfl;
    }

    public int getTm() {
        return tm;
    }

    public void setTm(int tm) {
        this.tm = tm;
    }

    public VlBean getVl() {
        return vl;
    }

    public void setVl(VlBean vl) {
        this.vl = vl;
    }

    public static class FlBean {
        /**
         * cnt : 2
         * fi : [{"id":100701,"name":"msd","lmt":0,"sb":1,"cname":"标清;(270P)","br":11,"profile":2,"drm":0,"video":1,"audio":1,"fs":1336491,"super":0,"sl":1},{"id":2,"name":"mp4","lmt":0,"sb":1,"cname":"高清;(480P)","br":16,"profile":1,"drm":0,"video":1,"audio":1,"fs":3764190,"super":0,"sl":0}]
         */

        private int cnt;
        private List<FiBean> fi;

        public int getCnt() {
            return cnt;
        }

        public void setCnt(int cnt) {
            this.cnt = cnt;
        }

        public List<FiBean> getFi() {
            return fi;
        }

        public void setFi(List<FiBean> fi) {
            this.fi = fi;
        }

        public static class FiBean {
            /**
             * id : 100701
             * name : msd
             * lmt : 0
             * sb : 1
             * cname : 标清;(270P)
             * br : 11
             * profile : 2
             * drm : 0
             * video : 1
             * audio : 1
             * fs : 1336491
             * super : 0
             * sl : 1
             */

            private int id;
            private String name;
            private int lmt;
            private int sb;
            private String cname;
            private int br;
            private int profile;
            private int drm;
            private int video;
            private int audio;
            private int fs;
            private int superX;
            private int sl;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getLmt() {
                return lmt;
            }

            public void setLmt(int lmt) {
                this.lmt = lmt;
            }

            public int getSb() {
                return sb;
            }

            public void setSb(int sb) {
                this.sb = sb;
            }

            public String getCname() {
                return cname;
            }

            public void setCname(String cname) {
                this.cname = cname;
            }

            public int getBr() {
                return br;
            }

            public void setBr(int br) {
                this.br = br;
            }

            public int getProfile() {
                return profile;
            }

            public void setProfile(int profile) {
                this.profile = profile;
            }

            public int getDrm() {
                return drm;
            }

            public void setDrm(int drm) {
                this.drm = drm;
            }

            public int getVideo() {
                return video;
            }

            public void setVideo(int video) {
                this.video = video;
            }

            public int getAudio() {
                return audio;
            }

            public void setAudio(int audio) {
                this.audio = audio;
            }

            public int getFs() {
                return fs;
            }

            public void setFs(int fs) {
                this.fs = fs;
            }

            public int getSuperX() {
                return superX;
            }

            public void setSuperX(int superX) {
                this.superX = superX;
            }

            public int getSl() {
                return sl;
            }

            public void setSl(int sl) {
                this.sl = sl;
            }
        }
    }

    public static class SflBean {
        /**
         * cnt : 0
         */

        private int cnt;

        public int getCnt() {
            return cnt;
        }

        public void setCnt(int cnt) {
            this.cnt = cnt;
        }
    }

    public static class VlBean {
        /**
         * cnt : 1
         * vi : [{"br":11,"ch":0,"cl":{"fc":0,"keyid":"q1424b6snvr.100701"},"ct":21600,"drm":0,"dsb":0,"fmd5":"06877918bbadf720edfbbc61a67d2962","fn":"q1424b6snvr.m701.mp4","fs":1336491,"fst":5,"fvkey":"D55D470698003FE876597A250D9F39799B706854CDA054996E74DEDE649C8E8EB25415B7D6EA8161D0C60C8BC55F9AFE4AF71644DC8BDF52AABCA6DB97654ABA7BED496AFDF337C473127B15AF672AFBB801157024D70ED561E0193229A74845180EF060CB05FF97","head":0,"hevc":0,"iflag":0,"level":0,"lnk":"q1424b6snvr","logo":1,"mst":8,"pl":null,"share":1,"sp":0,"st":2,"tail":0,"td":"110.40","ti":"1-1 课程简介","tie":0,"type":5124,"ul":{"ui":[{"url":"http://203.205.158.76/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/","vt":200,"dtc":0,"dt":2},{"url":"http://203.205.158.84/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/","vt":200,"dtc":0,"dt":2},{"url":"http://203.205.158.85/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/","vt":200,"dtc":0,"dt":2},{"url":"http://video.dispatch.tc.qq.com/","vt":0,"dtc":0,"dt":2}]},"vh":272,"vid":"q1424b6snvr","videotype":0,"vr":0,"vst":2,"vw":480,"wh":1.7647059,"wl":{"wi":[]},"uptime":1520483786,"fvideo":0,"fvpint":0,"swhdcp":0,"ad":{"adsid":"","adpinfo":""}}]
         */

        private int cnt;
        private List<ViBean> vi;

        public int getCnt() {
            return cnt;
        }

        public void setCnt(int cnt) {
            this.cnt = cnt;
        }

        public List<ViBean> getVi() {
            return vi;
        }

        public void setVi(List<ViBean> vi) {
            this.vi = vi;
        }

        public static class ViBean {
            /**
             * br : 11
             * ch : 0
             * cl : {"fc":0,"keyid":"q1424b6snvr.100701"}
             * ct : 21600
             * drm : 0
             * dsb : 0
             * fmd5 : 06877918bbadf720edfbbc61a67d2962
             * fn : q1424b6snvr.m701.mp4
             * fs : 1336491
             * fst : 5
             * fvkey : D55D470698003FE876597A250D9F39799B706854CDA054996E74DEDE649C8E8EB25415B7D6EA8161D0C60C8BC55F9AFE4AF71644DC8BDF52AABCA6DB97654ABA7BED496AFDF337C473127B15AF672AFBB801157024D70ED561E0193229A74845180EF060CB05FF97
             * head : 0
             * hevc : 0
             * iflag : 0
             * level : 0
             * lnk : q1424b6snvr
             * logo : 1
             * mst : 8
             * pl : null
             * share : 1
             * sp : 0
             * st : 2
             * tail : 0
             * td : 110.40
             * ti : 1-1 课程简介
             * tie : 0
             * type : 5124
             * ul : {"ui":[{"url":"http://203.205.158.76/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/","vt":200,"dtc":0,"dt":2},{"url":"http://203.205.158.84/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/","vt":200,"dtc":0,"dt":2},{"url":"http://203.205.158.85/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/","vt":200,"dtc":0,"dt":2},{"url":"http://video.dispatch.tc.qq.com/","vt":0,"dtc":0,"dt":2}]}
             * vh : 272
             * vid : q1424b6snvr
             * videotype : 0
             * vr : 0
             * vst : 2
             * vw : 480
             * wh : 1.7647059
             * wl : {"wi":[]}
             * uptime : 1520483786
             * fvideo : 0
             * fvpint : 0
             * swhdcp : 0
             * ad : {"adsid":"","adpinfo":""}
             */

            private int br;
            private int ch;
            private ClBean cl;
            private int ct;
            private int drm;
            private int dsb;
            private String fmd5;
            private String fn;
            private int fs;
            private int fst;
            private String fvkey;
            private int head;
            private int hevc;
            private int iflag;
            private int level;
            private String lnk;
            private int logo;
            private int mst;
            private Object pl;
            private int share;
            private int sp;
            private int st;
            private int tail;
            private String td;
            private String ti;
            private int tie;
            private int type;
            private UlBean ul;
            private int vh;
            private String vid;
            private int videotype;
            private int vr;
            private int vst;
            private int vw;
            private double wh;
            private WlBean wl;
            private int uptime;
            private int fvideo;
            private int fvpint;
            private int swhdcp;
            private AdBean ad;

            public int getBr() {
                return br;
            }

            public void setBr(int br) {
                this.br = br;
            }

            public int getCh() {
                return ch;
            }

            public void setCh(int ch) {
                this.ch = ch;
            }

            public ClBean getCl() {
                return cl;
            }

            public void setCl(ClBean cl) {
                this.cl = cl;
            }

            public int getCt() {
                return ct;
            }

            public void setCt(int ct) {
                this.ct = ct;
            }

            public int getDrm() {
                return drm;
            }

            public void setDrm(int drm) {
                this.drm = drm;
            }

            public int getDsb() {
                return dsb;
            }

            public void setDsb(int dsb) {
                this.dsb = dsb;
            }

            public String getFmd5() {
                return fmd5;
            }

            public void setFmd5(String fmd5) {
                this.fmd5 = fmd5;
            }

            public String getFn() {
                return fn;
            }

            public void setFn(String fn) {
                this.fn = fn;
            }

            public int getFs() {
                return fs;
            }

            public void setFs(int fs) {
                this.fs = fs;
            }

            public int getFst() {
                return fst;
            }

            public void setFst(int fst) {
                this.fst = fst;
            }

            public String getFvkey() {
                return fvkey;
            }

            public void setFvkey(String fvkey) {
                this.fvkey = fvkey;
            }

            public int getHead() {
                return head;
            }

            public void setHead(int head) {
                this.head = head;
            }

            public int getHevc() {
                return hevc;
            }

            public void setHevc(int hevc) {
                this.hevc = hevc;
            }

            public int getIflag() {
                return iflag;
            }

            public void setIflag(int iflag) {
                this.iflag = iflag;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getLnk() {
                return lnk;
            }

            public void setLnk(String lnk) {
                this.lnk = lnk;
            }

            public int getLogo() {
                return logo;
            }

            public void setLogo(int logo) {
                this.logo = logo;
            }

            public int getMst() {
                return mst;
            }

            public void setMst(int mst) {
                this.mst = mst;
            }

            public Object getPl() {
                return pl;
            }

            public void setPl(Object pl) {
                this.pl = pl;
            }

            public int getShare() {
                return share;
            }

            public void setShare(int share) {
                this.share = share;
            }

            public int getSp() {
                return sp;
            }

            public void setSp(int sp) {
                this.sp = sp;
            }

            public int getSt() {
                return st;
            }

            public void setSt(int st) {
                this.st = st;
            }

            public int getTail() {
                return tail;
            }

            public void setTail(int tail) {
                this.tail = tail;
            }

            public String getTd() {
                return td;
            }

            public void setTd(String td) {
                this.td = td;
            }

            public String getTi() {
                return ti;
            }

            public void setTi(String ti) {
                this.ti = ti;
            }

            public int getTie() {
                return tie;
            }

            public void setTie(int tie) {
                this.tie = tie;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public UlBean getUl() {
                return ul;
            }

            public void setUl(UlBean ul) {
                this.ul = ul;
            }

            public int getVh() {
                return vh;
            }

            public void setVh(int vh) {
                this.vh = vh;
            }

            public String getVid() {
                return vid;
            }

            public void setVid(String vid) {
                this.vid = vid;
            }

            public int getVideotype() {
                return videotype;
            }

            public void setVideotype(int videotype) {
                this.videotype = videotype;
            }

            public int getVr() {
                return vr;
            }

            public void setVr(int vr) {
                this.vr = vr;
            }

            public int getVst() {
                return vst;
            }

            public void setVst(int vst) {
                this.vst = vst;
            }

            public int getVw() {
                return vw;
            }

            public void setVw(int vw) {
                this.vw = vw;
            }

            public double getWh() {
                return wh;
            }

            public void setWh(double wh) {
                this.wh = wh;
            }

            public WlBean getWl() {
                return wl;
            }

            public void setWl(WlBean wl) {
                this.wl = wl;
            }

            public int getUptime() {
                return uptime;
            }

            public void setUptime(int uptime) {
                this.uptime = uptime;
            }

            public int getFvideo() {
                return fvideo;
            }

            public void setFvideo(int fvideo) {
                this.fvideo = fvideo;
            }

            public int getFvpint() {
                return fvpint;
            }

            public void setFvpint(int fvpint) {
                this.fvpint = fvpint;
            }

            public int getSwhdcp() {
                return swhdcp;
            }

            public void setSwhdcp(int swhdcp) {
                this.swhdcp = swhdcp;
            }

            public AdBean getAd() {
                return ad;
            }

            public void setAd(AdBean ad) {
                this.ad = ad;
            }

            public static class ClBean {
                /**
                 * fc : 0
                 * keyid : q1424b6snvr.100701
                 */

                private int fc;
                private String keyid;

                public int getFc() {
                    return fc;
                }

                public void setFc(int fc) {
                    this.fc = fc;
                }

                public String getKeyid() {
                    return keyid;
                }

                public void setKeyid(String keyid) {
                    this.keyid = keyid;
                }
            }

            public static class UlBean {
                private List<UiBean> ui;

                public List<UiBean> getUi() {
                    return ui;
                }

                public void setUi(List<UiBean> ui) {
                    this.ui = ui;
                }

                public static class UiBean {
                    /**
                     * url : http://203.205.158.76/vhot2.qqvideo.tc.qq.com/AYcX5mbZ6P50lDx2umEa-Vu0pwMXc5EMBTH1n95puYo8/
                     * vt : 200
                     * dtc : 0
                     * dt : 2
                     */

                    private String url;
                    private int vt;
                    private int dtc;
                    private int dt;

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    public int getVt() {
                        return vt;
                    }

                    public void setVt(int vt) {
                        this.vt = vt;
                    }

                    public int getDtc() {
                        return dtc;
                    }

                    public void setDtc(int dtc) {
                        this.dtc = dtc;
                    }

                    public int getDt() {
                        return dt;
                    }

                    public void setDt(int dt) {
                        this.dt = dt;
                    }
                }
            }

            public static class WlBean {
                private List<?> wi;

                public List<?> getWi() {
                    return wi;
                }

                public void setWi(List<?> wi) {
                    this.wi = wi;
                }
            }

            public static class AdBean {
                /**
                 * adsid :
                 * adpinfo :
                 */

                private String adsid;
                private String adpinfo;

                public String getAdsid() {
                    return adsid;
                }

                public void setAdsid(String adsid) {
                    this.adsid = adsid;
                }

                public String getAdpinfo() {
                    return adpinfo;
                }

                public void setAdpinfo(String adpinfo) {
                    this.adpinfo = adpinfo;
                }
            }
        }
    }
}
