package androidknowledge.five.com.video.presenter.activitypresenter;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.video.R;
import androidknowledge.five.com.video.activity.MoreVideoActivity;
import androidknowledge.five.com.video.adapter.MoreVideoAdapter;
import androidknowledge.five.com.video.fragment.MoreVideoFragment;
import androidknowledge.five.com.video.model.MoreVideoTitleBean;

/**
 * @author:杜威
 * @Date:2018/12/19
 * @time:下午 2:26
 * @description:更多视频展示
 */
public class MoreVideoActivityPresenter extends AppDelegate {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private Context mContext;
    private List<MoreVideoFragment> mFragmentList = new ArrayList<>();
    private List<String> mTitleList = new ArrayList<>();
    private MoreVideoAdapter mVideoAdapter;

    /**
     * 获取布局
     */
    @Override
    public int getlayoutId() {
        return R.layout.activity_morevideo;
    }

    /**
     * 初始化方法
     */
    @Override
    public void initData() {
        //初始化控件
        mViewPager = get(R.id.vp_morevideo);
        mTabLayout = get(R.id.tl_morevideo_title);
        //绑定ViewPager
        mTabLayout.setupWithViewPager(mViewPager);
        //设置适配器
        mVideoAdapter = new MoreVideoAdapter(((MoreVideoActivity) mContext).getSupportFragmentManager());
        mViewPager.setAdapter(mVideoAdapter);
        //缓存数据
        mViewPager.setOffscreenPageLimit(3);
        //请求tablayout头部信息
        doHttpTitle();
    }

    /**
     * tabLayout头部数据请求
     */
    private void doHttpTitle() {
        new Utility(mContext).isLoadCache(Http.MORE_VIDEO_TITLE, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                //json转换
                MoreVideoTitleBean moreVideoTitleBean = JsonToBean.jsonToBean(data, MoreVideoTitleBean.class);
                List<MoreVideoTitleBean.ItemsBean> items = moreVideoTitleBean.getItems();
                for (int i = 0; i < items.size(); i++) {
                    //根据头部数量添加集合
                    mFragmentList.add(new MoreVideoFragment());
                    mTitleList.add(items.get(i).getName());
                }
                mVideoAdapter.setList(mTitleList, mFragmentList, items);
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.MORE_VIDEO_TITLE, null);
    }

    /**
     * 获取上下文
     */
    @Override
    public void getContext(Context context) {
        mContext = context;
    }
}
