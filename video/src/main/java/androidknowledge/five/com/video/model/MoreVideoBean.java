package androidknowledge.five.com.video.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * @author:杜威
 * @Date:2018/12/19
 * @time:下午 2:42
 * @description:
 */
public class MoreVideoBean implements IModel{

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * image : https://img.mukewang.com/57919306000175bf06000356-240-135.jpg
         * title : Android Studio宸ュ叿璁茶В
         * isvip : true
         * code : 8826
         * money : 888
         * position : live_0530_03
         */

        private String image;
        private String title;
        private boolean isvip;
        private String code;
        private String money;
        private String position;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean isIsvip() {
            return isvip;
        }

        public void setIsvip(boolean isvip) {
            this.isvip = isvip;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }
    }
}
