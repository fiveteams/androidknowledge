package androidknowledge.five.com.video.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * @author:杜威
 * @Date:2018/12/19
 * @time:下午 2:41
 * @description:
 */
public class MoreVideoTitleBean implements IModel{

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * name : Android
         * type : 0
         */

        private String name;
        private String type;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
