package androidknowledge.five.com.video.presenter.fragmentpresenter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.basemodel.IModel;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.Logger;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.video.R;
import androidknowledge.five.com.video.adapter.MoreVideoAdapter;
import androidknowledge.five.com.video.adapter.MoreVideoInfoAdapter;
import androidknowledge.five.com.video.model.MoreVideoBean;

/**
 * @author:杜威
 * @Date:2018/12/19
 * @time:下午 2:52
 * @description:更多视频展示
 */
public class MoreVideoFragmentPresenter extends AppDelegate {
    private String mType;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private List<MoreVideoBean.ItemsBean> mItems = new ArrayList<>();
    private MoreVideoInfoAdapter mInfoAdapter;
    private MaterialRefreshLayout mRefresh;

    /**
     * 获取布局
     */
    @Override
    public int getlayoutId() {
        return R.layout.fragment_more_video;
    }

    /**
     * 初始化方法
     */
    @Override
    public void initData() {
        //初始化控件
        mRecyclerView = get(R.id.rv_more_video);
        mRefresh = get(R.id.mrl_more_video);
        //刷新监听
        mRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                doHttpMoreVideo();
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                super.onRefreshLoadMore(materialRefreshLayout);
                doHttpMoreVideo();
            }
        });
        //设置布局管理器
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        //设置适配器
        mInfoAdapter = new MoreVideoInfoAdapter(mContext);
        mRecyclerView.setAdapter(mInfoAdapter);
        //请求更多视频数据
        doHttpMoreVideo();
    }

    /**
     * 获取更多视频数据
     */
    private void doHttpMoreVideo() {
        new Utility(mContext).isLoadCache(Http.MORE_VIDEO_INFO, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null) {
                    return;
                }
                //json解析
                MoreVideoBean moreVideoBean = JsonToBean.jsonToBean(data, MoreVideoBean.class);
                mItems = moreVideoBean.getItems();
                mInfoAdapter.setList(mItems);
                //停止刷新加载
                mRefresh.finishRefresh();
                mRefresh.finishRefreshLoadMore();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.MORE_VIDEO_INFO + mType + ".txt", null);
    }

    /**
     * 获取context
     */
    @Override
    public void getContext(Context context) {
        mContext = context;
    }

    /**
     * 获取视频类型
     *
     * @param type
     */
    public void setType(String type) {
        this.mType = type;
    }
}
