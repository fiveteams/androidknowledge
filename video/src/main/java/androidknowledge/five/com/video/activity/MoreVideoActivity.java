package androidknowledge.five.com.video.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;

import androidknowledge.five.com.video.presenter.activitypresenter.MoreVideoActivityPresenter;

/**
 * @author:杜威
 * @Date:2018/12/19
 * @time:下午 2:26
 * @description:更多视频
 */
public class MoreVideoActivity extends BaseActivityPresenter<MoreVideoActivityPresenter> {
    @Override
    public Class<MoreVideoActivityPresenter> getPresenter() {
        return MoreVideoActivityPresenter.class;
    }

    @Override
    public void initMethod() {
        super.initMethod();
        setTitleName("更多视频");
    }
}
