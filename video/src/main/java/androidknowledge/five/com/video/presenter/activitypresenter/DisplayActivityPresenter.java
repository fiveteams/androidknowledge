package androidknowledge.five.com.video.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.mvplibrary.mvp.basemodel.IModel;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;

import java.util.List;
import java.util.Random;

import androidknowledge.five.com.video.R;
import androidknowledge.five.com.video.activity.DisplayActivity;
import androidknowledge.five.com.video.adapter.DisplayAdapter;
import androidknowledge.five.com.video.model.DisplayBean;
import androidknowledge.five.com.video.model.GetPlayBean;
import androidknowledge.five.com.video.model.VideoStreaming;
import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;
import master.flame.danmaku.controller.DrawHandler;
import master.flame.danmaku.danmaku.model.BaseDanmaku;
import master.flame.danmaku.danmaku.model.DanmakuTimer;
import master.flame.danmaku.danmaku.model.IDanmakus;
import master.flame.danmaku.danmaku.model.android.DanmakuContext;
import master.flame.danmaku.danmaku.model.android.Danmakus;
import master.flame.danmaku.danmaku.parser.BaseDanmakuParser;
import master.flame.danmaku.ui.widget.DanmakuView;

/**
 * @author:杜威
 * @Date:2018/12/18
 * @time:上午 11:27
 * @description:视频播放页面
 */
public class DisplayActivityPresenter extends AppDelegate {

    private JzvdStd mVideoDisplay;
    private TextView mInfo;
    private RecyclerView mVideoList;
    private Context mContext;
    private String mVideoPosition;
    private DisplayAdapter mDisplayAdapter;
    private String mVideoUrl;
    private List<DisplayBean.VideoListBean> mVideoList1;
    private int mPosition = 0;

    /**
     * 获取布局
     */
    @Override
    public int getlayoutId() {
        return R.layout.activity_display;
    }

    /**
     * 初始化方法
     */
    @Override
    public void initData() {
        //初始化控件
        mVideoDisplay = get(R.id.js_display);
        mInfo = get(R.id.tv_display_text);
        mVideoList = get(R.id.rv_display_list);
        //获取videoPosition
        Intent intent = ((DisplayActivity) mContext).getIntent();
        mVideoPosition = intent.getStringExtra("videoPosition");
        //设置布局管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mVideoList.setLayoutManager(linearLayoutManager);
        //设置适配器
        mDisplayAdapter = new DisplayAdapter(mContext);
        mVideoList.setAdapter(mDisplayAdapter);
        //接口回调
        mDisplayAdapter.setClick(new DisplayAdapter.Click() {
            @Override
            public void setClick(String url, int position) {
                mVideoUrl = url;//获取视频url
                mPosition = position;//获取position
                doHttpVideoStreaming(mPosition);//改变视频
            }
        });
        //获取视频基本信息
        doHttpVideo();
    }

    /**
     * 获取视频流
     */
    private void doHttpVideoStreaming(int position) {
        new Utility(mContext).isLoadCache(Http.VIDEO_STREAMING, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                String substring = data.substring(13, data.length() - 1);
                VideoStreaming videoStreaming = JsonToBean.jsonToBean(substring, VideoStreaming.class);
                VideoStreaming.VlBean.ViBean viBean = videoStreaming.getVl().getVi().get(0);
                String videoUrl = viBean.getUl().getUi().get(2).getUrl() + viBean.getFn() + "?vkey=" + viBean.getFvkey();
                //视频播放
                mVideoDisplay.setUp(videoUrl, mVideoList1.get(mPosition).getVideo_title(), Jzvd.SCREEN_WINDOW_NORMAL);
                mVideoDisplay.startVideo();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.VIDEO_STREAMING + mVideoUrl, null);
    }

    /**
     * 获取视频基本信息
     */
    private void doHttpVideo() {
        new Utility(mContext).isLoadCache(Http.DISPLAY_URL, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                //解析json
                DisplayBean displayBean = JsonToBean.jsonToBean(data, DisplayBean.class);
                mVideoList1 = displayBean.getVideo_list();
                mInfo.setText(displayBean.getVideo_desc());
                mDisplayAdapter.setList(displayBean.getVideo_list());
                //第一个条目默认选中
                mVideoList1.get(0).setCheck(true);
                mVideoUrl = mVideoList1.get(0).getVideo_url();
                //播放视频
                doHttpVideoStreaming(mPosition);
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.DISPLAY_URL + mVideoPosition + ".txt", null);
    }
    /**
     * 获取context
     */
    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }
}
