package androidknowledge.five.com.video.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * @author:杜威
 * @Date:2018/12/18
 * @time:下午 3:52
 * @description:更多视频下条目信息
 */
public class DisplayBean implements IModel{

    /**
     * title : 浜掑姩鐩存挱APP寮€鍙戝叆闂�
     * video_url : q1424b6snvr
     * video_desc : 鏂楅奔锛屾槧瀹紝鑺辨鈥﹁繖浜涘湪浣犵敓娲讳腑涓嶅彲缂虹殑鐩存挱APP鐨勭幇鐘舵槸浠€涔堬紵娴佺▼鏄粈涔堬紵宸ュ叿鏈夊摢浜涳紝鐩存挱SDK鎬庝箞閫夋嫨锛屾湰璇剧▼灏嗕负浣犳彮绉樿繖浜涢棶棰橈紝璁╃洿鎾瑼PP寮€鍙戜笉鍐嶇绉橈紒
     * isList : false
     * video_list : [{"video_title":"1-1 璇剧▼绠\u20ac浠�","video_url":"q1424b6snvr","video_time":"","video_id":0},{"video_title":"1-2 鐩存挱鐜扮姸","video_url":"l14247ox1kx","video_time":"","video_id":0},{"video_title":"2-1 鐩存挱娴佺▼","video_url":"k1424ztzz5n","video_time":"","video_id":0},{"video_title":"2-2 鐩存挱娴佺▼涔嬮噰闆�","video_url":"o1424td3t51","video_time":"","video_id":0},{"video_title":"2-3 鐩存挱娴佺▼涔嬪墠澶勭悊","video_url":"s14246jayip","video_time":"","video_id":0},{"video_title":"2-4 鐩存挱娴佺▼涔嬬紪鐮�","video_url":"o1424bb7uid","video_time":"","video_id":0},{"video_title":"2-5 鐩存挱娴佺▼涔嬫帹娴佷笌浼樺寲","video_url":"k1424qr9vnz","video_time":"","video_id":0},{"video_title":"2-6 鐩存挱娴佺▼涔嬫湇鍔＄","video_url":"h1424hkeicu","video_time":"","video_id":0},{"video_title":"2-7 鐩存挱娴佺▼涔嬪鎴风","video_url":"y1424ocyomv","video_time":"","video_id":0},{"video_title":"2-8 鐩存挱娴佺▼涔嬩氦浜掔郴缁�","video_url":"x1424lip19l","video_time":"","video_id":0},{"video_title":"2-9 鐩存挱娴佺▼ 宸ュ叿","video_url":"k1424gf79xe","video_time":"","video_id":0},{"video_title":"3-1 鐩存挱SDK鐨勫姣�","video_url":"v1424ip9n4t","video_time":"","video_id":0},{"video_title":"4-1 璇剧▼鎬荤粨","video_url":"x142444rdke","video_time":"","video_id":0}]
     */

    private String title;
    private String video_url;
    private String video_desc;
    private boolean isList;
    private List<VideoListBean> video_list;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_desc() {
        return video_desc;
    }

    public void setVideo_desc(String video_desc) {
        this.video_desc = video_desc;
    }

    public boolean isIsList() {
        return isList;
    }

    public void setIsList(boolean isList) {
        this.isList = isList;
    }

    public List<VideoListBean> getVideo_list() {
        return video_list;
    }

    public void setVideo_list(List<VideoListBean> video_list) {
        this.video_list = video_list;
    }

    public static class VideoListBean {
        /**
         * video_title : 1-1 璇剧▼绠€浠�
         * video_url : q1424b6snvr
         * video_time :
         * video_id : 0
         */

        private String video_title;
        private String video_url;
        private String video_time;
        private int video_id;
        private boolean isCheck;

        public boolean isCheck() {
            return isCheck;
        }

        public void setCheck(boolean check) {
            isCheck = check;
        }

        public String getVideo_title() {
            return video_title;
        }

        public void setVideo_title(String video_title) {
            this.video_title = video_title;
        }

        public String getVideo_url() {
            return video_url;
        }

        public void setVideo_url(String video_url) {
            this.video_url = video_url;
        }

        public String getVideo_time() {
            return video_time;
        }

        public void setVideo_time(String video_time) {
            this.video_time = video_time;
        }

        public int getVideo_id() {
            return video_id;
        }

        public void setVideo_id(int video_id) {
            this.video_id = video_id;
        }
    }
}
