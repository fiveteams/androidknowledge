package androidknowledge.five.com.video.activity;

import android.content.Intent;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;

import androidknowledge.five.com.video.presenter.activitypresenter.DisplayActivityPresenter;
import cn.jzvd.Jzvd;

/**
 * @author:杜威
 * @Date:2018/12/18
 * @time:上午 11:25
 * @description:
 */
public class DisplayActivity extends BaseActivityPresenter<DisplayActivityPresenter> {
    @Override
    public Class<DisplayActivityPresenter> getPresenter() {
        return DisplayActivityPresenter.class;
    }

    @Override
    public void onBackPressed() {
        if (Jzvd.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
    *   初始化
    */
    @Override
    public void initMethod() {
        super.initMethod();
        Intent intent = getIntent();
        String videoTitle = intent.getStringExtra("videoTitle");
        setTitleShowOrHint(true);
        setTitleName(videoTitle);
        setImageBack(true);
    }
}
