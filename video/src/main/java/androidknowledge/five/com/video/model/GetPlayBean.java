package androidknowledge.five.com.video.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * @author:杜威
 * @Date:2018/12/21
 * @time:上午 9:38
 * @description:
 */
public class GetPlayBean implements IModel {

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * open_user : 26382
         * open_nickname : .=hhh
         * open_content : face1
         * open_time : 2018-12-20 16:37:58
         */

        private String open_user;
        private String open_nickname;
        private String open_content;
        private String open_time;

        public String getOpen_user() {
            return open_user;
        }

        public void setOpen_user(String open_user) {
            this.open_user = open_user;
        }

        public String getOpen_nickname() {
            return open_nickname;
        }

        public void setOpen_nickname(String open_nickname) {
            this.open_nickname = open_nickname;
        }

        public String getOpen_content() {
            return open_content;
        }

        public void setOpen_content(String open_content) {
            this.open_content = open_content;
        }

        public String getOpen_time() {
            return open_time;
        }

        public void setOpen_time(String open_time) {
            this.open_time = open_time;
        }
    }
}
