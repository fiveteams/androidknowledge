package androidknowledge.five.com.video.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.video.R;
import androidknowledge.five.com.video.activity.DisplayActivity;
import androidknowledge.five.com.video.activity.MoreVideoActivity;
import androidknowledge.five.com.video.model.MoreVideoBean;

/**
 * @author:杜威
 * @Date:2018/12/19
 * @time:下午 4:03
 * @description:视频条目展示
 */
public class MoreVideoInfoAdapter extends RecyclerView.Adapter<MoreVideoInfoAdapter.MyViewHodler> {

    private Context mContext;
    private List<MoreVideoBean.ItemsBean> mMoreVideoList = new ArrayList<>();

    public MoreVideoInfoAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(mContext, R.layout.adapter_more_video_info, null);
        MyViewHodler hodler = new MyViewHodler(view);
        hodler.mSimpleDraweeView = view.findViewById(R.id.sdv_more_video_item_image);
        hodler.mTextView = view.findViewById(R.id.tv_more_video_item_title);
        return hodler;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHodler hodler, final int position) {
        hodler.mTextView.setText(mMoreVideoList.get(position).getTitle());
        hodler.mSimpleDraweeView.setImageURI(mMoreVideoList.get(position).getImage());
        hodler.mSimpleDraweeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(((MoreVideoActivity)mContext), DisplayActivity.class);
                intent.putExtra("videoPosition",mMoreVideoList.get(position).getPosition());
                intent.putExtra("videoTitle",mMoreVideoList.get(position).getTitle());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMoreVideoList.size();
    }

    /**
     * 获取集合
     */
    public void setList(List<MoreVideoBean.ItemsBean> list) {
        this.mMoreVideoList = list;
        notifyDataSetChanged();
    }

    /**
     * ViewHodler
     */
    public class MyViewHodler extends RecyclerView.ViewHolder {
        SimpleDraweeView mSimpleDraweeView;
        TextView mTextView;

        public MyViewHodler(@NonNull View itemView) {
            super(itemView);
        }
    }
}
