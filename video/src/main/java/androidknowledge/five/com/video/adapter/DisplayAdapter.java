package androidknowledge.five.com.video.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.video.R;
import androidknowledge.five.com.video.model.DisplayBean;

/**
 * @author:杜威
 * @Date:2018/12/18
 * @time:下午 4:31
 * @description:
 */
public class DisplayAdapter extends RecyclerView.Adapter<DisplayAdapter.MyViewHolder> {
    private Context mContext;
    private List<DisplayBean.VideoListBean> mList = new ArrayList<>();

    public DisplayAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(mContext, R.layout.adapter_display_list, null);
        MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.mImageView = view.findViewById(R.id.iv_adapter_display_image);
        viewHolder.mTextView = view.findViewById(R.id.tv_adapter_display_text);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.mTextView.setText(mList.get(position).getVideo_title());
        if (mList.get(position).isCheck()){
            holder.mTextView.setTextColor(mContext.getResources().getColor(R.color.color_d43c3c));
            holder.mImageView.setImageResource(R.drawable.gan_yes);
        }else{
            holder.mTextView.setTextColor(mContext.getResources().getColor(R.color.color_000000));
            holder.mImageView.setImageResource(R.drawable.gan_no);
        }
        holder.mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i <mList.size() ; i++) {
                    if (i==position){
                        mList.get(i).setCheck(true);
                    }else{
                        mList.get(i).setCheck(false);
                    }
                }
                click.setClick(mList.get(position).getVideo_url(),position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<DisplayBean.VideoListBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageView;
        TextView mTextView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    private Click click;

    public void setClick(Click click) {
        this.click = click;
    }

    public interface Click{
        void setClick(String url, int position);
    }
}
