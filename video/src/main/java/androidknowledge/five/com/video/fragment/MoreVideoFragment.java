package androidknowledge.five.com.video.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mvplibrary.mvp.presenter.BaseFragmentPresenter;

import androidknowledge.five.com.video.R;
import androidknowledge.five.com.video.presenter.fragmentpresenter.MoreVideoFragmentPresenter;

/**
 * @author:杜威
 * @Date:2018/12/18
 * @time:上午 11:27
 * @description:
 */
public class MoreVideoFragment extends BaseFragmentPresenter<MoreVideoFragmentPresenter> {


    @Override
    public Class<MoreVideoFragmentPresenter> getPresenter() {
        return MoreVideoFragmentPresenter.class;
    }

    @Override
    public void initWeight() {
        super.initWeight();
        Bundle bundle = getArguments();
        String type = bundle.getString("type");
        presenter.setType(type);
    }
}
