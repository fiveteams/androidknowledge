package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.BbsActivityPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/20
 * 内容：查看论坛
 */
public class BbsActivity extends BaseActivityPresenter<BbsActivityPresenter> {
    @Override
    public Class<BbsActivityPresenter> getPresenter() {
        return BbsActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("论坛");

    }
}
