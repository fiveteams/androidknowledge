package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.AttentionActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.LoginActivityPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/12
 * 内容：关注我的
 **/
public class AttentionActivity extends BaseActivityPresenter<AttentionActivityPresenter>{
    @Override
    public Class<AttentionActivityPresenter> getPresenter() {
        return AttentionActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("关注我的");
    }
}
