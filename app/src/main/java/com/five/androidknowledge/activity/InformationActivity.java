package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.InformationActivityPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/19
 * 内容：
 */
public class InformationActivity extends BaseActivityPresenter<InformationActivityPresenter> {
    @Override
    public Class<InformationActivityPresenter> getPresenter() {
        return InformationActivityPresenter.class;
    }
}
