package com.five.androidknowledge.activity;


import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.UserActivityPresenter;

import me.leefeng.citypicker.CityPickerListener;

/**
 * 作者：刘进
 * 日期：2018/12/14
 * 内容：
 **/
public class UserActivity extends BaseActivityPresenter<UserActivityPresenter> implements CityPickerListener {
    @Override
    public Class<UserActivityPresenter> getPresenter() {
        return UserActivityPresenter.class;
    }

    @Override
    public void getCity(String s) {
       presenter.getCity(s);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        presenter.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("用户详情页面");
        presenter.onResume();
    }

}
