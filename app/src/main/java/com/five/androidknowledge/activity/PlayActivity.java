package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.PlayActivityPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/13
 * 内容：弹一弹
 */
public class PlayActivity extends BaseActivityPresenter<PlayActivityPresenter>{
    @Override
    public Class<PlayActivityPresenter> getPresenter() {
        return PlayActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void initMethod() {
        super.initMethod();
        setTitleName("弹一弹");
    }
}
