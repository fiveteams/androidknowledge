package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.MyShareActivityPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/26
 * 内容：我的分享
 **/
public class MyShareActivity extends BaseActivityPresenter<MyShareActivityPresenter>{
    @Override
    public Class<MyShareActivityPresenter> getPresenter() {
        return MyShareActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("我的分享");
    }
}
