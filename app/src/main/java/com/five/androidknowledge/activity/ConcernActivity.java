package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.ConcernActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.LoginActivityPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/12
 * 内容：我的关注
 **/
public class ConcernActivity extends BaseActivityPresenter<ConcernActivityPresenter>{
    @Override
    public Class<ConcernActivityPresenter> getPresenter() {
        return ConcernActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("我的关注");
        presenter.onResume();
    }
}
