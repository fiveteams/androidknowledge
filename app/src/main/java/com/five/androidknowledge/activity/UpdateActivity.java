package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.UpdateActivityPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/14
 * 内容：
 **/
public class UpdateActivity extends BaseActivityPresenter<UpdateActivityPresenter>{
    @Override
    public Class<UpdateActivityPresenter> getPresenter() {
        return UpdateActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("用户修改页面");
    }
}
