package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.LoginActivityPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/12
 * 内容：登录页面
 **/
public class LoginActivity  extends BaseActivityPresenter<LoginActivityPresenter>{
    @Override
    public Class<LoginActivityPresenter> getPresenter() {
        return LoginActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("登录页面");
    }
}
