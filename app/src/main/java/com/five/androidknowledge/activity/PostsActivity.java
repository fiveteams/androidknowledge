package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.LoginActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.PostsActivityPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/12
 * 内容：我的贴子
 **/
public class PostsActivity extends BaseActivityPresenter<PostsActivityPresenter>{
    @Override
    public Class<PostsActivityPresenter> getPresenter() {
        return PostsActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("我的贴子");
    }
}
