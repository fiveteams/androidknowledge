package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.LoginActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.RegisteredActivityPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/12
 * 内容：注册页面
 **/
public class RegisteredActivity extends BaseActivityPresenter<RegisteredActivityPresenter>{
    @Override
    public Class<RegisteredActivityPresenter> getPresenter() {
        return RegisteredActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("注册页面");
    }
}
