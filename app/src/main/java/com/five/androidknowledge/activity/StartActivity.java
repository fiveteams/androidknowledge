package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.StartActivityPresenter;

/**
 * @author:杜威
 * @Date:2018/12/11
 * @time:下午 3:11
 * @description:
 */
public class StartActivity extends BaseActivityPresenter<StartActivityPresenter> {
    @Override
    public Class<StartActivityPresenter> getPresenter() {
        return StartActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleShowOrHint(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.mediaPlayerDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.mediaPlayerPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.mediaPlayerStart();
    }


}
