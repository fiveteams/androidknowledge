package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.ViplActivityPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/27
 * 内容：
 */
public class VipActivity extends BaseActivityPresenter<ViplActivityPresenter> {

    @Override
    public Class<ViplActivityPresenter> getPresenter() {
        return ViplActivityPresenter.class;
    }
    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("VIP干货铺");

    }
}
