package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.TechnicalActivityPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/26
 * 内容：
 */
public class TechnicalActivity extends BaseActivityPresenter<TechnicalActivityPresenter> {
    @Override
    public Class<TechnicalActivityPresenter> getPresenter() {
        return TechnicalActivityPresenter.class;
    }
    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("技术博文");

    }
}
