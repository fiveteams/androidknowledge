package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.LookPostActivityPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/21
 * 内容：查看帖子
 */
public class LookPostActivity extends BaseActivityPresenter<LookPostActivityPresenter>{
    @Override
    public Class<LookPostActivityPresenter> getPresenter() {
        return LookPostActivityPresenter.class;
    }
}
