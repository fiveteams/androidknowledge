package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.BbsActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.SendPostActivityPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/20
 * 内容：发送论坛
 */
public class SendPostActivity extends BaseActivityPresenter<SendPostActivityPresenter> {
    @Override
    public Class<SendPostActivityPresenter> getPresenter() {
        return SendPostActivityPresenter.class;
    }
    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("发帖");

    }
}
