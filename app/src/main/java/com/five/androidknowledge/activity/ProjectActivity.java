package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.ProjectActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.ViplActivityPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/27
 * 内容：
 */
public class ProjectActivity extends BaseActivityPresenter<ProjectActivityPresenter> {

    @Override
    public Class<ProjectActivityPresenter> getPresenter() {
        return ProjectActivityPresenter.class;
    }
    @Override
    protected void onResume() {
        super.onResume();
        setTitleName("开源项目");

    }
}
