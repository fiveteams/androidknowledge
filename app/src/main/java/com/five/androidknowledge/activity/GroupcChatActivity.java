package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.GroupChatActivityPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/17
 * 内容：群聊
 */
public class GroupcChatActivity extends BaseActivityPresenter<GroupChatActivityPresenter>{

    @Override
    public Class<GroupChatActivityPresenter> getPresenter() {
        return GroupChatActivityPresenter.class;
    }

    @Override
    public void initMethod() {
        super.initMethod();
        setTitleName("群聊大厅");
        presenter.getSharedPreUtils();
    }
}
