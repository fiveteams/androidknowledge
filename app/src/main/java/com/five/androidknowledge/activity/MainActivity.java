package com.five.androidknowledge.activity;

import com.example.mvplibrary.mvp.presenter.BaseActivityPresenter;
import com.five.androidknowledge.presenter.activitypresenter.MainActivityPresenter;
/**
 * @author:杜威
 * @date:2018/12/10
 * @function:MainActivity视图
 * */
public class MainActivity extends BaseActivityPresenter<MainActivityPresenter> {
    @Override
    public Class<MainActivityPresenter> getPresenter() {
        return MainActivityPresenter.class;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleShowOrHint(true);
        setImageBack(false);
    }
}
