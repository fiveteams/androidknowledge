package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

/**
 * 作者：刘进
 * 日期：2018/12/13
 * 内容：
 **/
public class EmailBean implements IModel{


    /**
     * status : 0
     * code : 21310
     */

    private int status;
    private int code;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
