package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

/**
 * 作者：秦永聪
 * 日期：2018/12/27
 * 内容：
 */
public class VipBean implements IModel {


    /**
     * share_title : Android各种访问权限Permission详解
     * share_desc : 在Android的设计中，资源的访问或者网络连接，要想得到这些服务都需要声明其访问权限，否则将无法正常工作。在Android中这样的权限有很多种，这里将各类访问权限一一罗列出来，供大家使用时参考之用。
     * share_link : https://www.jianshu.com/p/f6f17befbd17
     * share_person : opopopop
     * share_user : abner23112
     * share_id : 0
     * share_time : 2018-12-26 21:43:32
     * id : 55
     */

    private String share_title;
    private String share_desc;
    private String share_link;
    private String share_person;
    private String share_user;
    private String share_id;
    private String share_time;
    private String id;

    public String getShare_title() {
        return share_title;
    }

    public void setShare_title(String share_title) {
        this.share_title = share_title;
    }

    public String getShare_desc() {
        return share_desc;
    }

    public void setShare_desc(String share_desc) {
        this.share_desc = share_desc;
    }

    public String getShare_link() {
        return share_link;
    }

    public void setShare_link(String share_link) {
        this.share_link = share_link;
    }

    public String getShare_person() {
        return share_person;
    }

    public void setShare_person(String share_person) {
        this.share_person = share_person;
    }

    public String getShare_user() {
        return share_user;
    }

    public void setShare_user(String share_user) {
        this.share_user = share_user;
    }

    public String getShare_id() {
        return share_id;
    }

    public void setShare_id(String share_id) {
        this.share_id = share_id;
    }

    public String getShare_time() {
        return share_time;
    }

    public void setShare_time(String share_time) {
        this.share_time = share_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
