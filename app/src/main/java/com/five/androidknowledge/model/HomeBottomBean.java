package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/12
 * 内容：
 */
public class HomeBottomBean implements IModel {

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * news_title : 社交媒体火爆的今天，网站seo还有用吗？
         * news_link : https://blog.csdn.net/lin000001/article/details/84108178
         * news_image : http://p99.pstatp.com/large/pgc-image/b17b2bd0152341a5825773130fd893e3
         * news_desc : 在当今社会中，各大网站的发展不仅仅需要依赖SEO的优化同时也需要SNS的辅助优化。什么SNS?SNS现如今的锋芒毕露已经将谷歌的光芒遮盖闲趣。而且在SNS中也对外
         * news_author :  lin000001
         * news_pic : https://avatar.csdn.net/7/2/8/3_lin000001.jpg
         * news_time : 2018-12-12 10:49:06
         * news_type : 1
         */

        private String news_title;
        private String news_link;
        private String news_image;
        private String news_desc;
        private String news_author;
        private String news_pic;
        private String news_time;
        private String news_type;

        public String getNews_title() {
            return news_title;
        }

        public void setNews_title(String news_title) {
            this.news_title = news_title;
        }

        public String getNews_link() {
            return news_link;
        }

        public void setNews_link(String news_link) {
            this.news_link = news_link;
        }

        public String getNews_image() {
            return news_image;
        }

        public void setNews_image(String news_image) {
            this.news_image = news_image;
        }

        public String getNews_desc() {
            return news_desc;
        }

        public void setNews_desc(String news_desc) {
            this.news_desc = news_desc;
        }

        public String getNews_author() {
            return news_author;
        }

        public void setNews_author(String news_author) {
            this.news_author = news_author;
        }

        public String getNews_pic() {
            return news_pic;
        }

        public void setNews_pic(String news_pic) {
            this.news_pic = news_pic;
        }

        public String getNews_time() {
            return news_time;
        }

        public void setNews_time(String news_time) {
            this.news_time = news_time;
        }

        public String getNews_type() {
            return news_type;
        }

        public void setNews_type(String news_type) {
            this.news_type = news_type;
        }
    }
}
