package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

/**
 * 作者：刘进
 * 日期：2018/12/17
 * 内容：
 **/
public class IsRegisteredBean implements IModel{

    /**
     * status : 0
     * message : ok
     */

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
