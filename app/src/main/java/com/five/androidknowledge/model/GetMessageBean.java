package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/24
 * 内容：
 */
public class GetMessageBean implements IModel {

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * nikename : AbnerMing
         * msg_time : 2018-07-12 09:52:27
         * msg_content : <p>图片看不了，真是的…… 图片看不了，真是的……&nbsp;</p>
         * msg_user : 888888
         * user_occupation : Android开发
         * msg_id : 63
         */

        private String nikename;
        private String msg_time;
        private String msg_content;
        private String msg_user;
        private String user_occupation;
        private String msg_id;

        public String getNikename() {
            return nikename;
        }

        public void setNikename(String nikename) {
            this.nikename = nikename;
        }

        public String getMsg_time() {
            return msg_time;
        }

        public void setMsg_time(String msg_time) {
            this.msg_time = msg_time;
        }

        public String getMsg_content() {
            return msg_content;
        }

        public void setMsg_content(String msg_content) {
            this.msg_content = msg_content;
        }

        public String getMsg_user() {
            return msg_user;
        }

        public void setMsg_user(String msg_user) {
            this.msg_user = msg_user;
        }

        public String getUser_occupation() {
            return user_occupation;
        }

        public void setUser_occupation(String user_occupation) {
            this.user_occupation = user_occupation;
        }

        public String getMsg_id() {
            return msg_id;
        }

        public void setMsg_id(String msg_id) {
            this.msg_id = msg_id;
        }
    }
}
