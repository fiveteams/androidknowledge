package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * 作者：刘进
 * 日期：2018/12/26
 * 内容：我的关注
 **/
public class ConcernBean implements IModel{

    /**
     * status : 0
     * items : [{"my_user":"abner26382","my_nick":"独家记忆","student_user":"abner16223","student_nick":"用户**1622311"},{"my_user":"abner26382","my_nick":"独家记忆","student_user":"","student_nick":""},{"my_user":"abner26382","my_nick":"独家记忆","student_user":"abner25875","student_nick":"2587560197@qq.com"}]
     */

    private int status;
    private List<ItemsBean> items;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * my_user : abner26382
         * my_nick : 独家记忆
         * student_user : abner16223
         * student_nick : 用户**1622311
         */

        private String my_user;
        private String my_nick;
        private String student_user;
        private String student_nick;

        public String getMy_user() {
            return my_user;
        }

        public void setMy_user(String my_user) {
            this.my_user = my_user;
        }

        public String getMy_nick() {
            return my_nick;
        }

        public void setMy_nick(String my_nick) {
            this.my_nick = my_nick;
        }

        public String getStudent_user() {
            return student_user;
        }

        public void setStudent_user(String student_user) {
            this.student_user = student_user;
        }

        public String getStudent_nick() {
            return student_nick;
        }

        public void setStudent_nick(String student_nick) {
            this.student_nick = student_nick;
        }
    }
}
