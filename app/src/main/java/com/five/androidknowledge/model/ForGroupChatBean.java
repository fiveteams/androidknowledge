package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/18
 * 内容：
 */
public class ForGroupChatBean implements IModel {

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * nickName :
         * msg_time : 2018-12-13 23:28:18
         * msg_content :
         * msg_user :
         * msg_id : 131
         */

        private String nickName;
        private String msg_time;
        private String msg_content;
        private String msg_user;
        private String msg_id;

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getMsg_time() {
            return msg_time;
        }

        public void setMsg_time(String msg_time) {
            this.msg_time = msg_time;
        }

        public String getMsg_content() {
            return msg_content;
        }

        public void setMsg_content(String msg_content) {
            this.msg_content = msg_content;
        }

        public String getMsg_user() {
            return msg_user;
        }

        public void setMsg_user(String msg_user) {
            this.msg_user = msg_user;
        }

        public String getMsg_id() {
            return msg_id;
        }

        public void setMsg_id(String msg_id) {
            this.msg_id = msg_id;
        }
    }
}
