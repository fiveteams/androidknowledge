package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * 作者：刘进
 * 日期：2018/12/27
 * 内容：我的贴子
 **/
public class PostsBean implements IModel{


    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * msg_nick : 独家记忆
         * msg_time : 2018-12-25 09:59:04
         * msg_content : xxxxxxxxxxxxxxxx
         * msg_user : abner26382
         * msg_id : 126
         * msg_hot : 0
         * msg_ck : 0
         * msg_hf : 0
         * msg_hf_author : 独家记忆
         * msg_hf_time : 2018-12-25 09:59:04
         * msg_day :
         * msg_title : hhhhhh
         */

        private String msg_nick;
        private String msg_time;
        private String msg_content;
        private String msg_user;
        private String msg_id;
        private String msg_hot;
        private String msg_ck;
        private String msg_hf;
        private String msg_hf_author;
        private String msg_hf_time;
        private String msg_day;
        private String msg_title;

        public String getMsg_nick() {
            return msg_nick;
        }

        public void setMsg_nick(String msg_nick) {
            this.msg_nick = msg_nick;
        }

        public String getMsg_time() {
            return msg_time;
        }

        public void setMsg_time(String msg_time) {
            this.msg_time = msg_time;
        }

        public String getMsg_content() {
            return msg_content;
        }

        public void setMsg_content(String msg_content) {
            this.msg_content = msg_content;
        }

        public String getMsg_user() {
            return msg_user;
        }

        public void setMsg_user(String msg_user) {
            this.msg_user = msg_user;
        }

        public String getMsg_id() {
            return msg_id;
        }

        public void setMsg_id(String msg_id) {
            this.msg_id = msg_id;
        }

        public String getMsg_hot() {
            return msg_hot;
        }

        public void setMsg_hot(String msg_hot) {
            this.msg_hot = msg_hot;
        }

        public String getMsg_ck() {
            return msg_ck;
        }

        public void setMsg_ck(String msg_ck) {
            this.msg_ck = msg_ck;
        }

        public String getMsg_hf() {
            return msg_hf;
        }

        public void setMsg_hf(String msg_hf) {
            this.msg_hf = msg_hf;
        }

        public String getMsg_hf_author() {
            return msg_hf_author;
        }

        public void setMsg_hf_author(String msg_hf_author) {
            this.msg_hf_author = msg_hf_author;
        }

        public String getMsg_hf_time() {
            return msg_hf_time;
        }

        public void setMsg_hf_time(String msg_hf_time) {
            this.msg_hf_time = msg_hf_time;
        }

        public String getMsg_day() {
            return msg_day;
        }

        public void setMsg_day(String msg_day) {
            this.msg_day = msg_day;
        }

        public String getMsg_title() {
            return msg_title;
        }

        public void setMsg_title(String msg_title) {
            this.msg_title = msg_title;
        }
    }
}
