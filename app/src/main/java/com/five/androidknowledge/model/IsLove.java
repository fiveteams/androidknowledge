package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

/**
 * 作者：秦永聪
 * 日期：2018/12/25
 * 内容：
 */
public class IsLove implements IModel {

    /**
     * status : 0
     */

    private int status;
    private boolean ischeck;

    public boolean isIscheck() {
        return ischeck;
    }

    public void setIscheck(boolean ischeck) {
        this.ischeck = ischeck;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
