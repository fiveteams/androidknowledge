package com.five.androidknowledge.model;

import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/26
 * 内容：技术博文
 */
public class TechnicalBean {

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * news_title : Dart Flutter 1.0 发布~入门初体验Android Studio开发
         * news_link : https://blog.csdn.net/Bluechalk/article/details/84657211
         * news_image : https://img-blog.csdnimg.cn/20181130165233371.png
         * news_desc : 本文介绍如何搭建Flutter开发环境，这是Dart入门的第一步。
         * news_author : Hynson
         * news_pic : https://avatar.csdn.net/C/1/6/3_bluechalk.jpg
         * news_time : 2018-12-12 11:57:05
         * news_type : 0
         */

        private String news_title;
        private String news_link;
        private String news_image;
        private String news_desc;
        private String news_author;
        private String news_pic;
        private String news_time;
        private String news_type;

        public String getNews_title() {
            return news_title;
        }

        public void setNews_title(String news_title) {
            this.news_title = news_title;
        }

        public String getNews_link() {
            return news_link;
        }

        public void setNews_link(String news_link) {
            this.news_link = news_link;
        }

        public String getNews_image() {
            return news_image;
        }

        public void setNews_image(String news_image) {
            this.news_image = news_image;
        }

        public String getNews_desc() {
            return news_desc;
        }

        public void setNews_desc(String news_desc) {
            this.news_desc = news_desc;
        }

        public String getNews_author() {
            return news_author;
        }

        public void setNews_author(String news_author) {
            this.news_author = news_author;
        }

        public String getNews_pic() {
            return news_pic;
        }

        public void setNews_pic(String news_pic) {
            this.news_pic = news_pic;
        }

        public String getNews_time() {
            return news_time;
        }

        public void setNews_time(String news_time) {
            this.news_time = news_time;
        }

        public String getNews_type() {
            return news_type;
        }

        public void setNews_type(String news_type) {
            this.news_type = news_type;
        }
    }
}
