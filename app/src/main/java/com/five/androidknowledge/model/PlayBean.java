package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/13
 * 内容：
 */
public class PlayBean implements IModel {

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * open_user : fanzhuopu@126.com
         * open_nickname : 动动
         * open_content : 123
         * open_time : 2018-12-13 15:31:24
         */

        private String open_user;
        private String open_nickname;
        private String open_content;
        private String open_time;

        public String getOpen_user() {
            return open_user;
        }

        public void setOpen_user(String open_user) {
            this.open_user = open_user;
        }

        public String getOpen_nickname() {
            return open_nickname;
        }

        public void setOpen_nickname(String open_nickname) {
            this.open_nickname = open_nickname;
        }

        public String getOpen_content() {
            return open_content;
        }

        public void setOpen_content(String open_content) {
            this.open_content = open_content;
        }

        public String getOpen_time() {
            return open_time;
        }

        public void setOpen_time(String open_time) {
            this.open_time = open_time;
        }
    }
}
