package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * 作者：马利亚
 * 日期：2018/12/14
 * 内容：
 */
public class ScripturesTitle implements IModel{

    private List<TitlesBean> titles;

    public List<TitlesBean> getTitles() {
        return titles;
    }

    public void setTitles(List<TitlesBean> titles) {
        this.titles = titles;
    }

    public static class TitlesBean {
        /**
         * name : 浼樿川鏂囩珷
         * id : 1
         */

        private String name;
        private String id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
