package com.five.androidknowledge.model;

import com.example.mvplibrary.mvp.basemodel.IModel;

import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/21
 * 内容：
 */
public class PostBean implements IModel {

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * msg_nick : AbnerMing
         * msg_time : 2018-06-22 17:40:16
         * msg_content : <p>Hello,CSDN里一个多月没见了，在这段时间里，是不是觉得我消失了，或者说这个CSDN不运营了呢，其实非也，我没有消失，而是在专心做一个小程序，一个服务Android开发者的一个平台，当然了我之前也做过几个，遗憾的是，反响平平，不好的成绩并不会击退我继续前进的步伐，这不，我的新产品又孕育而生了，希望这一次能够得到更多的关注。</p><p>其实，一周多以前我已经上线了，只不过觉得还不太完美，就迟迟没有做推广，想必有的朋友已经用过了，像对于之前我做的产品，在我眼里这个小程序绝对是我目前做的最优秀的一个产品,彻彻底底的，一个只谈技术，分享干货的一个平台，我们先看几张截图：</p><p style="text-align: center;"><img src="https://img-blog.csdn.net/20180503100745155" style="width: auto;height: auto;"><br></p><p style="text-align: center;"><img src="https://img-blog.csdn.net/2018050310075830" style="width: auto;height: auto;"><br></p><p style="text-align: center;"><img src="https://img-blog.csdn.net/20180503100809171" style="width: auto;height: auto;"><br></p><p style="text-align: center;"><img src="https://img-blog.csdn.net/20180503100818244" style="width: auto;height: auto;"><br></p><p style="text-align: left;"><br></p><p style="text-align: left;">上面的四张图是截的小程序主要的四个页面，想查看更多，不妨扫描下面的小程序，就可以直接到达。&nbsp;</p><p style="text-align: center;"><img src="https://img-blog.csdn.net/20180503100915585" style="width: auto; height: auto;">&nbsp;<br></p><p style="text-align: left;"><br></p><p>这个小程序，其实设计并没有花太多的时间，主要是内容的更新，花了我太久的时间，因为目前为止，小程序还不支持个人开发者使用webview，那么这就导致了，内容无法填充，我的解决办法就是，把内容封装成了接口，请求后进行填充，所以啊，每篇文章，我都是一段一段的复制过来的，好辛苦的。</p><p><br></p><p>目前为止，我也一直在不断增加新的功能，来开拓大家的视野，像时时新闻，股市直播，还有一些提升大家内涵的每日一文，每日推荐，还有天气预报，以及想到了大家，可能会无聊，就又加了，游戏工具，里面会不时的添加新的游戏，共大家消遣，总之吧，对于这个小程序，我会不停的改进，希望大家也能多多提出自己宝贵的意见。</p><p><br></p><p>服务到大家，能给大家带来帮助，是我的初衷，提升自己的技术，也是自己的出发点，有时我特别在乎自己的产品状况，但以前的产品“Android一周推荐”，“Android向导”，“一周推荐APP”，慢慢的走向默默无闻时，自己也没有那么的不开心，毕竟创作它们的时候，也是一种享受。</p><p><br></p><p>5月，我回来了，带着满满的诚意，接下来的时光，对于这个公众号，我会持续的更新，会有原创，也会给大家整理一些最新的资讯，或者找些技术干货，总之，不闲着。</p><p><br></p><p>最后一句，觉得小程序能给你带来帮助，不妨分享一下，AbnerMing在此感谢。</p>
         * msg_user : 888888
         * msg_id : 63
         * msg_hot : 0
         * msg_ck : 731
         * msg_hf : 1
         * msg_hf_author : AbnerMing
         * msg_hf_time : 2018-06-22 17:40:16
         * msg_day : 2018-6-22
         * msg_title : 2018年5月，Im back！
         */

        private String msg_nick;
        private String msg_time;
        private String msg_content;
        private String msg_user;
        private String msg_id;
        private String msg_hot;
        private String msg_ck;
        private String msg_hf;
        private String msg_hf_author;
        private String msg_hf_time;
        private String msg_day;
        private String msg_title;

        public String getMsg_nick() {
            return msg_nick;
        }

        public void setMsg_nick(String msg_nick) {
            this.msg_nick = msg_nick;
        }

        public String getMsg_time() {
            return msg_time;
        }

        public void setMsg_time(String msg_time) {
            this.msg_time = msg_time;
        }

        public String getMsg_content() {
            return msg_content;
        }

        public void setMsg_content(String msg_content) {
            this.msg_content = msg_content;
        }

        public String getMsg_user() {
            return msg_user;
        }

        public void setMsg_user(String msg_user) {
            this.msg_user = msg_user;
        }

        public String getMsg_id() {
            return msg_id;
        }

        public void setMsg_id(String msg_id) {
            this.msg_id = msg_id;
        }

        public String getMsg_hot() {
            return msg_hot;
        }

        public void setMsg_hot(String msg_hot) {
            this.msg_hot = msg_hot;
        }

        public String getMsg_ck() {
            return msg_ck;
        }

        public void setMsg_ck(String msg_ck) {
            this.msg_ck = msg_ck;
        }

        public String getMsg_hf() {
            return msg_hf;
        }

        public void setMsg_hf(String msg_hf) {
            this.msg_hf = msg_hf;
        }

        public String getMsg_hf_author() {
            return msg_hf_author;
        }

        public void setMsg_hf_author(String msg_hf_author) {
            this.msg_hf_author = msg_hf_author;
        }

        public String getMsg_hf_time() {
            return msg_hf_time;
        }

        public void setMsg_hf_time(String msg_hf_time) {
            this.msg_hf_time = msg_hf_time;
        }

        public String getMsg_day() {
            return msg_day;
        }

        public void setMsg_day(String msg_day) {
            this.msg_day = msg_day;
        }

        public String getMsg_title() {
            return msg_title;
        }

        public void setMsg_title(String msg_title) {
            this.msg_title = msg_title;
        }
    }
}
