package com.five.androidknowledge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.five.androidknowledge.R;
import com.five.androidknowledge.model.StudyBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author:杜威
 * @Date:2018/12/12
 * @time:下午 8:06
 * @description:
 */
public class StudyAdapter extends RecyclerView.Adapter<StudyAdapter.MyViewHolder> {

    private Context mContext;
    private List<StudyBean.ItemsBean> mList = new ArrayList<>();

    public StudyAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public StudyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(mContext, R.layout.adapter_study_item, null);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull StudyAdapter.MyViewHolder holder, final int position) {
        //赋值
        holder.mSimpledraweeView.setImageURI(mList.get(position).getImage());
        holder.mTextview.setText(mList.get(position).getTitle());
        //设置点击事件
        holder.mReltive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lister.success(mList,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<StudyBean.ItemsBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextview;
        private SimpleDraweeView mSimpledraweeView;
        private RelativeLayout mReltive;
        SimpleDraweeView simpleDraweeView;
        TextView textView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mReltive=(RelativeLayout)itemView.findViewById(R.id.rl_study_frament_reltive);
            mSimpledraweeView = itemView.findViewById(R.id.sdv_study_item_image);
            mTextview = itemView.findViewById(R.id.tv_study_item_title);
        }
    }
    private HttpLister lister;
    public void result(HttpLister lister){
        this.lister=lister;
    }
    public interface HttpLister{
        void success(List<StudyBean.ItemsBean> list,int i);
    }
}
