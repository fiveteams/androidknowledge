package com.five.androidknowledge.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.InformationActivity;
import com.five.androidknowledge.gif.GifTextView;
import com.five.androidknowledge.model.PlayBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/12
 * 内容：展示弹一弹
 */
public class PlayAdapter extends RecyclerView.Adapter<PlayAdapter.MyViewhodel> {
    private Context mContext;

    public PlayAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyViewhodel onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      //找布局
       View view=View.inflate(mContext,R.layout.adapter_play_layout,null);
        MyViewhodel myViewhodel = new MyViewhodel(view);
        return myViewhodel;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewhodel myViewhodel, final int i) {
         //赋值
        myViewhodel.tv_play_username.setText(playList.get(i).getOpen_nickname());
        myViewhodel.tv_play_time.setText(playList.get(i).getOpen_time());
        myViewhodel.tv_play_content.insertGif(playList.get(i).getOpen_content());
        //点击事件
        myViewhodel.img_play_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, InformationActivity.class);
                intent.putExtra("userId",playList.get(i).getOpen_user());
                intent.putExtra("nickname",playList.get(i).getOpen_nickname());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return playList.size();
    }
    private List<PlayBean.ItemsBean> playList = new ArrayList<>();
    public void setData(List<PlayBean.ItemsBean> list) {
        this.playList=list;
        notifyDataSetChanged();
    }

    public class MyViewhodel extends RecyclerView.ViewHolder {

        TextView tv_play_username,tv_play_time;
        ImageView img_play_img;
        GifTextView tv_play_content;

        public MyViewhodel(@NonNull View itemView) {
            super(itemView);
            //找控件
            tv_play_username= itemView.findViewById(R.id.tv_play_username);
            tv_play_time= itemView.findViewById(R.id.tv_play_time);
            tv_play_content= itemView.findViewById(R.id.tv_play_content);
            img_play_img= itemView.findViewById(R.id.img_play_img);
        }
    }
}
