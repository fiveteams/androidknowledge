package com.five.androidknowledge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.mvplibrary.utils.SharedPreUtils;
import com.five.androidknowledge.R;
import com.five.androidknowledge.model.ConcernBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：刘进
 * 日期：2018/12/26
 * 内容：关注的适配器
 **/
public class ConcernAdapter extends RecyclerView.Adapter<ConcernAdapter.MyAdapter>{
    private Context mContext;
    public ConcernAdapter(Context mContext) {
        this.mContext=mContext;
    }

    @NonNull
    @Override
    public ConcernAdapter.MyAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(mContext, R.layout.adapter_item_concern,null);

        MyAdapter viewHolder = new MyAdapter(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ConcernAdapter.MyAdapter myAdapter, int i) {
        myAdapter.mName.setText(mList.get(i).getStudent_nick());
        String student_user = mList.get(i).getStudent_user();
        SharedPreUtils.put(mContext,"studentUser",student_user);
        myAdapter.mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click.click(student_user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    private List<ConcernBean.ItemsBean> mList = new ArrayList<>();
    public void setList(List<ConcernBean.ItemsBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public class MyAdapter extends RecyclerView.ViewHolder {
         TextView mName;
         Button mCancel;

        public MyAdapter(@NonNull View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.tv_name);
            mCancel = itemView.findViewById(R.id.bt_cancel);
        }
    }
    public onClick click;
    public void result(onClick onClick){
        this.click=onClick;
    }
    public interface onClick{
        void click(String student_user);
    }
}
