package com.five.androidknowledge.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.BbsActivity;
import com.five.androidknowledge.activity.LookPostActivity;
import com.five.androidknowledge.model.GetMessageBean;
import com.five.androidknowledge.model.PostBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/24
 * 内容：
 */
public class GetMessageAdapter extends RecyclerView.Adapter<GetMessageAdapter.MyViewHolder>{
    private Context mContext;
    private List<GetMessageBean.ItemsBean> mList=new ArrayList<>();

    public GetMessageAdapter(Context context) {
        this.mContext=context;
    }

    @NonNull
    @Override
    public GetMessageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=View.inflate(mContext, R.layout.adapter_getmessage_layout,null);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GetMessageAdapter.MyViewHolder myViewHolder, final int i) {
          myViewHolder.mTime.setText(mList.get(i).getMsg_time());
          myViewHolder.mName.setText(mList.get(i).getNikename());
          myViewHolder.mIntro.setText(mList.get(i).getMsg_content());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<GetMessageBean.ItemsBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTime;
        private TextView mIntro;
        private TextView mName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mName=(TextView)itemView.findViewById(R.id.tv_getmessage_username);
            mTime=(TextView)itemView.findViewById(R.id.tv_getmessage_time);
            mIntro=(TextView)itemView.findViewById(R.id.tv_getmessage_content);
        }
    }
}
