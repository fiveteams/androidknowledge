package com.five.androidknowledge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvplibrary.utils.SharedPreUtils;
import com.five.androidknowledge.R;
import com.five.androidknowledge.model.ConcernBean;
import com.five.androidknowledge.model.PostsBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：刘进
 * 日期：2018/12/26
 * 内容：我的贴子
 **/
public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.MyAdapter>{
    private Context mContext;
    public PostsAdapter(Context mContext) {
        this.mContext=mContext;
    }

    @NonNull
    @Override
    public PostsAdapter.MyAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(mContext, R.layout.adapter_item_posts,null);
        MyAdapter viewHolder = new MyAdapter(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PostsAdapter.MyAdapter myAdapter, int i) {
        myAdapter.mTime.setText(mList.get(i).getMsg_time());
        myAdapter.mTitle.setText(mList.get(i).getMsg_title());
        myAdapter.mAuthor.setText(mList.get(i).getMsg_hf_author());
        myAdapter.mContent.setText(mList.get(i).getMsg_content());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    private List<PostsBean.ItemsBean> mList = new ArrayList<>();
    public void setList(List<PostsBean.ItemsBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public class MyAdapter extends RecyclerView.ViewHolder {

        TextView mTitle,mTime,mAuthor,mContent;

        public MyAdapter(@NonNull View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.tv_title1);
            mTime = itemView.findViewById(R.id.tv_time);
            mAuthor = itemView.findViewById(R.id.tv_author);
            mContent = itemView.findViewById(R.id.tv_content);
        }
    }
//    public onClick click;
//    public void result(onClick onClick){
//        this.click=onClick;
//    }
//    public interface onClick{
//        void click(String student_user);
//    }
}
