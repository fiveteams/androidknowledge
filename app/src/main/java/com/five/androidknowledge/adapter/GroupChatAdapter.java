package com.five.androidknowledge.adapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvplibrary.utils.SharedPreUtils;
import com.five.androidknowledge.R;
import com.five.androidknowledge.model.ForGroupChatBean;
import com.five.androidknowledge.model.PlayBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/12
 * 内容：展示论坛
 */
public class GroupChatAdapter extends RecyclerView.Adapter<GroupChatAdapter.MyViewhodel> {
    private Context mContext;

    public GroupChatAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyViewhodel onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       //找布局
        View view=View.inflate(mContext,R.layout.activity_message,null);
        MyViewhodel myViewhodel = new MyViewhodel(view);
        return myViewhodel;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewhodel myViewhodel, final int i) {
         //赋值
        String userNickName = SharedPreUtils.getString(mContext, "userNickName", "");
        String id = SharedPreUtils.getString(mContext, "id");
        int anInt = SharedPreUtils.getInt(mContext, id);
        myViewhodel.mTvTime.setText(mGroupchatList.get(i).getMsg_time());
        if(userNickName.equals(mGroupchatList.get(i).getNickName())){
            myViewhodel.rightLayout.setVisibility(View.VISIBLE);
            myViewhodel.leftLayout.setVisibility(View.GONE);
            myViewhodel.rightMsg.setText(mGroupchatList.get(i).getMsg_content());
            myViewhodel.mTvRightUsername.setText(mGroupchatList.get(i).getNickName());
        }
        else{
            myViewhodel.leftLayout.setVisibility(View.VISIBLE);
            myViewhodel.rightLayout.setVisibility(View.GONE);
            myViewhodel.leftMsg.setText(mGroupchatList.get(i).getMsg_content());
            myViewhodel.mTvLeftUsername.setText(mGroupchatList.get(i).getNickName());
        }

    }

    @Override
    public int getItemCount() {
        return mGroupchatList.size();
    }
    private List<ForGroupChatBean.ItemsBean> mGroupchatList = new ArrayList<>();
    public void setList(List<ForGroupChatBean.ItemsBean> list) {
        this.mGroupchatList = list;
        //刷新适配器
        notifyDataSetChanged();
    }

    public class MyViewhodel extends RecyclerView.ViewHolder {

        private RelativeLayout leftLayout,rightLayout;
        private  TextView leftMsg,rightMsg,mTvTime,mTvLeftUsername,mTvRightUsername;

        public MyViewhodel(@NonNull View itemView) {
            super(itemView);
            leftLayout =(RelativeLayout) itemView.findViewById(R.id.left_layout);
            rightLayout = (RelativeLayout) itemView.findViewById(R.id.right_layout);
            leftMsg = (TextView) itemView.findViewById(R.id.left_msg);
            rightMsg = (TextView) itemView.findViewById(R.id.right_msg);
            mTvTime=(TextView)itemView.findViewById(R.id.tv_time);
            mTvLeftUsername=(TextView)itemView.findViewById(R.id.tv_left_username);
            mTvRightUsername=(TextView)itemView.findViewById(R.id.tv_right_username);
        }
    }
}
