package com.five.androidknowledge.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.BbsActivity;
import com.five.androidknowledge.activity.LookPostActivity;
import com.five.androidknowledge.model.PostBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/21
 * 内容：
 */
public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder>{
    private Context mContext;
    private List<PostBean.ItemsBean> mList=new ArrayList<>();

    public PostAdapter(Context context) {
        this.mContext=context;
    }

    @NonNull
    @Override
    public PostAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=View.inflate(mContext, R.layout.adapter_post_layout,null);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PostAdapter.MyViewHolder myViewHolder, final int i) {
        myViewHolder.mTitle.setText(mList.get(i).getMsg_title());
        myViewHolder.mName.setText(mList.get(i).getMsg_hf_author());
        myViewHolder.mIntro.setText(mList.get(i).getMsg_content());
        myViewHolder.mTime.setText(mList.get(i).getMsg_time());
        //查看帖子
        myViewHolder.mRellayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, LookPostActivity.class);
                intent.putExtra("title",mList.get(i).getMsg_title());
                intent.putExtra("cont",mList.get(i).getMsg_content());
                intent.putExtra("id",mList.get(i).getMsg_id());
                ((BbsActivity)mContext).startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<PostBean.ItemsBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mRellayout1;
        private TextView mTime;
        private TextView mIntro;
        private TextView mName;
        private TextView mTitle;
        private SimpleDraweeView mImg;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mRellayout1= (LinearLayout) itemView.findViewById(R.id.rl_post_frament_rellayout1);
            mTime=(TextView)itemView.findViewById(R.id.tv_post_frament_time);
            mTitle=(TextView)itemView.findViewById(R.id.tv_post_frament_title);
            mName=(TextView)itemView.findViewById(R.id.tv_post_frament_name);
            mIntro=(TextView)itemView.findViewById(R.id.tv_post_frament_intro);
        }
    }
}
