package com.five.androidknowledge.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.MainActivity;
import com.five.androidknowledge.model.VideoBean;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.video.activity.DisplayActivity;

/**
 * @author:杜威
 * @Date:2018/12/12
 * @time:下午 4:28
 * @description:
 */
public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {
    private Context mContext;
    private List<VideoBean.ItemsBean> mList = new ArrayList<>();

    public VideoAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(mContext, R.layout.adapter_video_item, null);
        MyViewHolder holder = new MyViewHolder(view);
        holder.mSimpleDraweeView = view.findViewById(R.id.sdv_video_item_image);
        holder.mTextView = view.findViewById(R.id.tv_video_item_title);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int i) {
        holder.mSimpleDraweeView.setImageURI(mList.get(i).getImage());
        holder.mTextView.setText(mList.get(i).getTitle());
        holder.mSimpleDraweeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(((MainActivity)mContext), DisplayActivity.class);
                intent.putExtra("videoPosition",mList.get(i).getPosition());
                intent.putExtra("videoTitle",mList.get(i).getTitle());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<VideoBean.ItemsBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView mSimpleDraweeView;
        TextView mTextView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
