package com.five.androidknowledge.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.MainActivity;
import com.five.androidknowledge.model.HistoryBean;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.study.activity.LinkActivity;

/**
 * 作者：马利亚
 * 日期：2018/12/12
 * 内容：历史博文
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder>{
    private Context mContext;
    private List<HistoryBean.ItemsBean> mList=new ArrayList<>();

    public HistoryAdapter(Context context) {
        this.mContext=context;
    }

    @NonNull
    @Override
    public HistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=View.inflate(mContext, R.layout.adapter_article_layout,null);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryAdapter.MyViewHolder myViewHolder, final int i) {
        myViewHolder.mImg.setImageURI(mList.get(i).getNews_image());
        myViewHolder.mTitle.setText(mList.get(i).getNews_title());
        myViewHolder.mName.setText(mList.get(i).getNews_author());
        myViewHolder.mIntro.setText(mList.get(i).getNews_desc());
        myViewHolder.mTime.setText(mList.get(i).getNews_time());

        myViewHolder.mRellayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(((Activity) mContext), LinkActivity.class);
                String link = mList.get(i).getNews_link();
                intent.putExtra("link",link);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<HistoryBean.ItemsBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout mRellayout1;
        private TextView mTime;
        private TextView mIntro;
        private TextView mName;
        private TextView mTitle;
        private SimpleDraweeView mImg;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mRellayout1=(RelativeLayout)itemView.findViewById(R.id.rl_article_frament_rellayout1);
            mTime=(TextView)itemView.findViewById(R.id.tv_article_frament_time);
            mImg=(SimpleDraweeView)itemView.findViewById(R.id.iv_article_frament_img);
            mTitle=(TextView)itemView.findViewById(R.id.tv_article_frament_title);
            mName=(TextView)itemView.findViewById(R.id.tv_article_frament_name);
            mIntro=(TextView)itemView.findViewById(R.id.tv_article_frament_intro);
        }
    }
}
