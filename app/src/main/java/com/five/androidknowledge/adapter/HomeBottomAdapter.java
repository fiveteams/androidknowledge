package com.five.androidknowledge.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.five.androidknowledge.R;
import com.five.androidknowledge.model.HomeBottomBean;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidknowledge.five.com.study.activity.LinkActivity;

/**
 * 作者：秦永聪
 * 日期：2018/12/12
 * 内容：首页底部适配器
 */
public class HomeBottomAdapter extends RecyclerView.Adapter<HomeBottomAdapter.MyViewhodel> {
    List<HomeBottomBean.ItemsBean> mHomeBottomlist=new ArrayList<>();
    private Context mContext;

    public HomeBottomAdapter(List<HomeBottomBean.ItemsBean> homeBottomlist, Context context) {
        this.mHomeBottomlist = homeBottomlist;
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyViewhodel onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      //找布局
       View view=View.inflate(mContext,R.layout.adapter_home_bottom,null);
        MyViewhodel myViewhodel = new MyViewhodel(view);
        return myViewhodel;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewhodel myViewhodel, final int i) {
         //赋值
        myViewhodel.tv_home_content.setText(mHomeBottomlist.get(i).getNews_desc());
        myViewhodel.tv_home_time.setText(mHomeBottomlist.get(i).getNews_time());
        myViewhodel.tv_home_title.setText(mHomeBottomlist.get(i).getNews_title());
        Picasso.with(mContext).load(mHomeBottomlist.get(i).getNews_pic()).into(myViewhodel.im_home_bottomimg);
       //点击事件
        myViewhodel.ll_home_onclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, LinkActivity.class);
                String news_link = mHomeBottomlist.get(i).getNews_link();
              //  Toast.makeText(context,"link==="+news_link,Toast.LENGTH_LONG).show();
                intent.putExtra("link",news_link);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mHomeBottomlist.size();
    }

    public class MyViewhodel extends RecyclerView.ViewHolder {

        private TextView tv_home_content,tv_home_time,tv_home_title;
        private ImageView im_home_bottomimg;
        private LinearLayout ll_home_onclick;

        public MyViewhodel(@NonNull View itemView) {
            super(itemView);
            //找控件
            im_home_bottomimg = itemView.findViewById(R.id.im_home_bottomimg);
            tv_home_content = itemView.findViewById(R.id.tv_home_content);
            tv_home_time = itemView.findViewById(R.id.tv_home_time);
            tv_home_title = itemView.findViewById(R.id.tv_home_title);
            ll_home_onclick = itemView.findViewById(R.id.ll_home_onclick);
        }
    }
}
