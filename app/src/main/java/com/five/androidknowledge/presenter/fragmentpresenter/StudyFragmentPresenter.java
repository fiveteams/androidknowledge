package com.five.androidknowledge.presenter.fragmentpresenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.adapter.StudyAdapter;
import com.five.androidknowledge.model.StudyBean;

import java.util.List;

import androidknowledge.five.com.study.activity.StudyChildActivity;
import androidknowledge.five.com.study.activity.StudyChildTwoActivity;

/**
 * 作者：马利亚
 * 日期：2018/12/12
 * 内容：学习
 **/
public class StudyFragmentPresenter extends AppDelegate {
    private Context mContext;
    private RecyclerView mStudyShow;
    private MaterialRefreshLayout mMrlStudyShow;
    private StudyAdapter mStudyAdapter;

    @Override
    public int getlayoutId() {
        return R.layout.fragment_study;
    }

    @Override
    public void initData() {
        //初始化控件
        mStudyShow = get(R.id.rv_study_show);
        mMrlStudyShow = get(R.id.mrl_study_show);
        mStudyShow = get(R.id.rv_study_show);
        mMrlStudyShow = get(R.id.mrl_study_show);
        //设置布局管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mStudyShow.setLayoutManager(linearLayoutManager);
        //设置adapter
        mStudyAdapter = new StudyAdapter(mContext);
        mStudyShow.setAdapter(mStudyAdapter);
        //刷新
        initRefreshLayout();
        //请求学习网络数据
        doHttpStudy();


        //接口回调
        mStudyAdapter.result(new StudyAdapter.HttpLister() {
            @Override
            public void success(List<StudyBean.ItemsBean> list, int i) {
                String study_id = list.get(i).getStudy_id();
                int islist = list.get(i).getIslist();
                String title = list.get(i).getTitle();
                if (islist == 0) {
                    Intent intent = new Intent(mContext, StudyChildActivity.class);
                    intent.putExtra("study_id", study_id);
                    intent.putExtra("isList", islist);
                    intent.putExtra("title", title);
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, StudyChildTwoActivity.class);
                    intent.putExtra("study_id", study_id);
                    intent.putExtra("isList", islist);
                    intent.putExtra("title", title);
                    mContext.startActivity(intent);
                }

            }
        });
    }

    private void initRefreshLayout() {
        //设置可刷新
        mMrlStudyShow.setLoadMore(true);
        //设置支持下拉加载更多
        mMrlStudyShow.setLoadMore(true);
        //刷新以及加载回调
        mMrlStudyShow.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                //向服务器请求数据
                doHttpStudy();

            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                doHttpStudy();
            }
        });
    }


    /**
     * 请求学习网络数据
     */
    private void doHttpStudy() {
        new Utility(mContext).isLoadCache(Http.STUDY_SHOW, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null) {
                    return;
                }
                StudyBean studyBean = JsonToBean.jsonToBean(data, StudyBean.class);
                List<StudyBean.ItemsBean> itemsList = studyBean.getItems();
                mStudyAdapter.setList(itemsList);
                //加载完毕关闭刷新和加载更多
                mMrlStudyShow.finishRefresh();
                mMrlStudyShow.finishRefreshLoadMore();
                mMrlStudyShow.finishRefresh();
                mMrlStudyShow.finishRefreshLoadMore();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.STUDY_SHOW, null);
    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }
}
