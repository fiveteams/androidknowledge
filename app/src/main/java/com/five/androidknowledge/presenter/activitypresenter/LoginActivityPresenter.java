package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.CodeUtils;
import com.example.mvplibrary.utils.MD5Utils;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.LoginActivity;
import com.five.androidknowledge.activity.MainActivity;
import com.five.androidknowledge.activity.RegisteredActivity;
import com.five.androidknowledge.model.LoginBean;

import java.util.HashMap;
import java.util.Map;

import static com.example.mvplibrary.utils.JsonToBean.jsonToBean;

/**
 * 作者：刘进
 * 日期：2018/12/12
 * 内容：登录页面
 **/
public class LoginActivityPresenter extends AppDelegate implements View.OnClickListener {
    private Context context;
    private EditText mName, mPwd, mEtcode;
    private String name, pwd;
    private ImageView mCode;

    @Override
    public int getlayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initData() {
        //初始化控件
        mName = get(R.id.et_usename);
        mPwd = get(R.id.et_password);
        mCode = get(R.id.iv_img_code);
        mEtcode = get(R.id.et_img_code);
        //点击
        setClick(this, R.id.tv_registered, R.id.bt_login, R.id.iv_img_code);
        //Intent传值
        Intent intent = ((LoginActivity) context).getIntent();
        name = intent.getStringExtra("name");
        pwd = intent.getStringExtra("pwd");
        //赋值
//        mName.setText(name);
//        mPwd.setText(pwd);
        mCode.setImageBitmap(CodeUtils.getInstance().createBitmap());

    }

    @Override
    public void getContext(Context context) {
        this.context = context;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_registered://注册
                context.startActivity(new Intent(context, RegisteredActivity.class));
                break;
            case R.id.bt_login://登录
                String name = mName.getText().toString().trim();
                String pwd = mPwd.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    toast("邮箱不能为空");
                    return;
                }

                if (TextUtils.isEmpty(name)) {
                    toast("密码不能为空");
                    return;
                }
                String trim = mEtcode.getText().toString().trim();
                if (!trim.equals(CodeUtils.getInstance().getCode())) {
                    toast("验证码错误");
                    return;
                }
                loginHttp(name, pwd);
                break;
            case R.id.iv_img_code:
                mCode.setImageBitmap(CodeUtils.getInstance().createBitmap());
                break;
        }
    }

    /**
     * 登录
     */
    private void loginHttp(String name, String pwd) {
        //md5加密 密码
        String md5 = new MD5Utils().getMD5(pwd);
        Map<String, String> map = new HashMap<>();
        map.put("user_name", name);
        map.put("user_pass", md5);
        toast("name=" + name + "pwd=" + md5);
        //md5加密 密码
        new Utility(context).isLoadCache(Http.USERLOGIN, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                LoginBean loginBean = jsonToBean(data, LoginBean.class);
                if (loginBean.getStatus() == 0) {
                    Toast.makeText(context, "data===" + data, Toast.LENGTH_SHORT).show();
                    //存值
                    String useName = loginBean.getUser_name();
                    String userNickName = loginBean.getUser_nickname();
                    String userAddress = loginBean.getUser_address();
                    String userTime = loginBean.getUser_time();
                    String userEmail = loginBean.getUser_mail();
                    String userId = loginBean.getUser_id();
                    SharedPreUtils.put(context, "useName", useName);
                    SharedPreUtils.put(context, "userNickName", userNickName);
                    SharedPreUtils.put(context, "userAddress", userAddress);
                    SharedPreUtils.put(context, "userEmail", userEmail);
                    SharedPreUtils.put(context, "userTime", userTime);
                    SharedPreUtils.put(context, "userId", userId);
                    SharedPreUtils.put(context, "user_occupation", loginBean.getUser_occupation());
                    context.startActivity(new Intent(context, MainActivity.class));
                } else {
                    Toast.makeText(context, "登录失败", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.USERLOGIN, map);

    }

    //toast
    public void toast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

}
