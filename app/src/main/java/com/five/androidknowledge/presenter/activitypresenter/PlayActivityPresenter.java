package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.example.mvplibrary.utils.percentage.PercentRelativeLayout;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.LoginActivity;
import com.five.androidknowledge.adapter.PlayAdapter;
import com.five.androidknowledge.gif.GifEditText;
import com.five.androidknowledge.model.PlayBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者：秦永聪
 * 日期：2018/12/13
 * 内容：
 */
public class PlayActivityPresenter extends AppDelegate implements View.OnClickListener {
    //用于记录当前是第几页
    private int mCurPage = 10;
    private RecyclerView mRvPlayShow;
    private PlayAdapter mPlayAdapter;
    private GifEditText mEtPlayInput;
    private TextView mBPlaySend;
    private String mEtPlayInputGet;
    private MaterialRefreshLayout mMrlOpensourcePlayRefresh;
    private String mUserNickName;
    private String mUserId;

    @Override
    public int getlayoutId() {
        return R.layout.activity_play;
    }

    private Context mContext;

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    @Override
    public void initData() {
        mRvPlayShow = (RecyclerView) get(R.id.rv_play_show);
        mEtPlayInput = (GifEditText) get(R.id.edit_text);
        mBPlaySend = (TextView) get(R.id.send);
        mMrlOpensourcePlayRefresh = (MaterialRefreshLayout) get(R.id.mrl_opensource_play_refresh);
        //点击事件
        mBPlaySend.setOnClickListener(this);
        //展示弹一弹的消息
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mPlayAdapter = new PlayAdapter(mContext);
        mRvPlayShow.setAdapter(mPlayAdapter);
        mRvPlayShow.setLayoutManager(linearLayoutManager);
        //获取弹一弹的消息
        dohttp();
        initRefreshLayout();
    }

    private void initRefreshLayout() {
        //设置支持下拉加载更多
        mMrlOpensourcePlayRefresh.setLoadMore(true);
        //刷新以及加载回调
        mMrlOpensourcePlayRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                mCurPage = 10;
                dohttp();
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                mCurPage += 10;
                dohttp();
            }
        });
    }

    private void dohttp() {
        Map map = new HashMap<>();
        map.put("open_id", "100");
        map.put("open_num", mCurPage);
        map.put("open_page", 1);
        new Utility(mContext).isLoadCache(Http.BASE_PLAY, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                PlayBean playBean = JsonToBean.jsonToBean(data, PlayBean.class);
                List<PlayBean.ItemsBean> list = playBean.getItems();
                //playList.addAll(list);
                mPlayAdapter.setData(list);
                mMrlOpensourcePlayRefresh.finishRefresh();
                mMrlOpensourcePlayRefresh.finishRefreshLoadMore();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.BASE_PLAY, map);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send:
                mUserNickName = SharedPreUtils.getString(mContext, "userNickName", "");
                mEtPlayInputGet = mEtPlayInput.getText().toString().trim();
                if (mUserNickName == null && TextUtils.isEmpty(mUserNickName)) {
                    mContext.startActivity(new Intent(mContext, LoginActivity.class));
                } else {
                    dohttpSend();
                }
                break;
        }
    }

    //点击发送弹一弹
    private void dohttpSend() {
        Map map = new HashMap<>();
        mUserId = SharedPreUtils.getString(mContext, "userId", "");
        map.put("open_user", mUserId);
        map.put("open_nickname", mUserNickName);
        map.put("open_id", "100");
        map.put("open_content", mEtPlayInputGet);
        new Utility(mContext).isLoadCache(Http.BASE_SEND, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                Toast.makeText(mContext, "发送成功", Toast.LENGTH_LONG).show();
                dohttp();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.BASE_SEND, map);
    }

    public void onResume() {
        dohttp();
        mPlayAdapter.notifyDataSetChanged();
    }
}
