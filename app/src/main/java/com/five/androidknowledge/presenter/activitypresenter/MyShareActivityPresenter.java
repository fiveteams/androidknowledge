package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者：刘进
 * 日期：2018/12/26
 * 内容：
 **/
public class MyShareActivityPresenter extends AppDelegate {
    private TextView mRecyclerView;
    private Context mContext;
    @Override
    public int getlayoutId() {
        return R.layout.activity_my_share;
    }

    @Override
    public void initData() {
        //初始化控件
        //mRecyclerView = get(R.id.share_recyclerView);
        mRecyclerView = get(R.id.title);
        myShareHttp();
    }
    //我的分享
    private void myShareHttp() {
        Map<String,String> map = new HashMap<>();
        new Utility(mContext).isLoadCache(Http.MYSHARE,false,true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.MYSHARE,map);
    }

    @Override
    public void getContext(Context context) {
        this.mContext=context;
    }
}
