package com.five.androidknowledge.presenter.activitypresenter;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.Utils;
import com.example.mvplibrary.utils.listener.AnimatorListener;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.MainActivity;
import com.five.androidknowledge.activity.StartActivity;

/**
 * @author:杜威
 * @Date:2018/12/11
 * @time:下午 3:11
 * @description:启动页
 */
public class StartActivityPresenter extends AppDelegate {
    private Context mContext;
    private ImageView mImage;
    private ImageView mCircle;
    private View mBottom;
    private View mBackgroundTop;
    private String[] mMetrics;
    private TextView mAappname;
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0){
                Intent intent = new Intent(((StartActivity)mContext), MainActivity.class);
                mContext.startActivity(intent);
                ((StartActivity) mContext).finish();
            }
        }
    };
    private MediaPlayer mMediaPlayer;
    private float[] a;

    @Override
    public int getlayoutId() {
        return R.layout.activity_start;
    }

    @Override
    public void initData() {
        mImage = get(R.id.iv_start_android_image);
        mCircle = get(R.id.iv_start_red_circle);
        mBottom = get(R.id.view_start_background_bottom);
        mBackgroundTop = get(R.id.view_start_background_top);
        mAappname = get(R.id.tv_start_appname);
        //获取屏幕高度
        mMetrics = Utils.getMetrics(mContext);
        mMediaPlayer = MediaPlayer.create(mContext,R.raw.hava);
        //圆圈动画
        circleAnimator();

    }
    //圆圈动画
    private void circleAnimator() {
        ObjectAnimator circleAnimatorTwo = ObjectAnimator.ofFloat(mCircle,"scaleX",0,0.5f,1);
        ObjectAnimator circleAnimatorThree= ObjectAnimator.ofFloat(mCircle,"scaleY",0,0.5f,1);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(2000);
        animatorSet.playTogether(circleAnimatorTwo,circleAnimatorThree);
        animatorSet.start();
        animatorSet.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                backgroundAnimation();
            }
        });
    }
    //背景出现
    private void backgroundAnimation() {
        float mMetricHeight = Float.parseFloat(mMetrics[1]);
        float height = mMetricHeight/2;
        mBottom.setVisibility(View.VISIBLE);
        mBackgroundTop.setVisibility(View.VISIBLE);
        ObjectAnimator bacrkgroundAnimatorBottom = ObjectAnimator.ofFloat(mBottom,"translationY",-height,0);
        bacrkgroundAnimatorBottom.setDuration(2000);
        bacrkgroundAnimatorBottom.start();
        ObjectAnimator bacrkgroundAnimatorTop = ObjectAnimator.ofFloat(mBackgroundTop,"translationY",height,0);
        bacrkgroundAnimatorTop.setDuration(2000);
        bacrkgroundAnimatorTop.start();
        bacrkgroundAnimatorBottom.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                logoAnimator();
                appnameAnimation();

            }
        });
    }
    //图片出现
    private void logoAnimator(){
        mImage.setVisibility(View.VISIBLE);
        ObjectAnimator objectAnimatorOne = ObjectAnimator.ofFloat(mImage,"rotationY",0,360);
        ObjectAnimator objectAnimatorTwo = ObjectAnimator.ofFloat(mImage,"translationY",-400,0);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimatorOne, objectAnimatorTwo);
        animatorSet.setDuration(3000);
        animatorSet.start();
    }
    //appName出现
    private void appnameAnimation() {
        mAappname.setVisibility(View.VISIBLE);
        ObjectAnimator objectAnimatorOne =  ObjectAnimator.ofFloat(mAappname,"scaleY",0,0.5f,1);
        ObjectAnimator objectAnimatorTwo =  ObjectAnimator.ofFloat(mAappname,"scaleX",0,0.5f,1);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimatorOne, objectAnimatorTwo);
        animatorSet.setDuration(3000);
        animatorSet.start();
        animatorSet.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                handler.sendEmptyMessageDelayed(0,0);
            }
        });
    }

   /* public ObjectAnimator anim(View view, String type, float[] values){
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view,type,values);
        return objectAnimator;
    }*/
    /*public void animSet(){
        animatorSet = new AnimatorSet();
        a = new float[]{0,0.5f,1};
        animatorSet.playTogether(anim(iv_start_red_circle,"scaleX", a),anim(iv_start_red_circle,"scaleY", a));
        animatorSet.setDuration(3000);
        animatorSet.start();
        animatorSet.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onAnimationEnd(Animator animation) {
                float mMetricHeight = Float.parseFloat(mMetrics[1]);
                float height = mMetricHeight/2;
                view_start_background_bottom.setVisibility(View.VISIBLE);
                view_start_background_top.setVisibility(View.VISIBLE);
                float []b = {-height,0};
                float []c = {height,0};
                animatorSet.playTogether(anim(view_start_background_bottom,"translationY",b),anim(view_start_background_top,"translationY",c));
                animatorSet.setDuration(3000);
                animatorSet.start();
            }
        });
    }*/

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    public void mediaPlayerDestroy() {
        mMediaPlayer.release();//释放音频，销毁
    }

    public void mediaPlayerPause() {
        mMediaPlayer.pause();
    }

    public void mediaPlayerStart() {
        mMediaPlayer.start();
    }
}
