package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.BbsActivity;
import com.five.androidknowledge.activity.SendPostActivity;
import com.five.androidknowledge.adapter.HistoryAdapter;
import com.five.androidknowledge.fragment.AllPostFragment;
import com.five.androidknowledge.fragment.GoodPostFragment;
import com.five.androidknowledge.fragment.HotPostFragment;
import com.five.androidknowledge.model.HistoryBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者：秦永聪
 * 日期：2018/12/20
 * 内容：
 */
public class TechnicalActivityPresenter extends AppDelegate {


    private RecyclerView mRvTechinicalFramentRecycle;
    private RecyclerView mRvTechinicalFramentRecycle1;
    private HistoryAdapter mHistoryadapter;
    private MaterialRefreshLayout mRefresh;

    @Override
    public int getlayoutId() {
        return R.layout.activity_technical_layout;
    }


    private Context mContext;
    @Override
    public void getContext(Context context) {
       this.mContext=context;
    }
    @Override
    public void initData() {
        mRefresh = (MaterialRefreshLayout) get(R.id.mrl_history_frament_refresh);
        mRvTechinicalFramentRecycle1 = get(R.id.rv_techinical_frament_recycle);
        doHttp();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mHistoryadapter=new HistoryAdapter(mContext);
        mRvTechinicalFramentRecycle1.setAdapter(mHistoryadapter);
        mRvTechinicalFramentRecycle1.setLayoutManager(linearLayoutManager);
    }

    private void doHttp() {
        Map map=new HashMap<>();
        map.put("news_type","0");

        new Utility(mContext).isLoadCache(Http.BASE_ARTICLE,true,true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                HistoryBean historyBean= JsonToBean.jsonToBean(data, HistoryBean.class);
                mHistoryadapter.setList(historyBean.getItems());
                mRefresh.finishRefresh();
                mRefresh.finishRefreshLoadMore();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.BASE_ARTICLE,map);
    }

}
