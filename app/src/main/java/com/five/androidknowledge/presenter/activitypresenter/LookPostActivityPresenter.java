package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.BbsActivity;
import com.five.androidknowledge.activity.LookPostActivity;
import com.five.androidknowledge.adapter.GetMessageAdapter;
import com.five.androidknowledge.fragment.AllPostFragment;
import com.five.androidknowledge.fragment.GoodPostFragment;
import com.five.androidknowledge.fragment.HotPostFragment;
import com.five.androidknowledge.gif.GifEditText;
import com.five.androidknowledge.model.GetMessageBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者：秦永聪
 * 日期：2018/12/20
 * 内容：查看论坛
 */
public class LookPostActivityPresenter extends AppDelegate {


    private TextView mTvLookpostTitle;
    private TextView mTvLookpostCont;
    //    private GifEditText mEtLookPostInput;
    private TextView mBLookPostSend;
    private String mId;
    private RecyclerView mRvLookpostShow;
    private GetMessageAdapter mGetMessageAdapter;

    @Override
    public int getlayoutId() {
        return R.layout.activity_lookpost;
    }


    private Context mContext;

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    @Override
    public void initData() {
        //找控件
        mTvLookpostTitle = (TextView) get(R.id.tv_lookpost_title);
        mTvLookpostCont = (TextView) get(R.id.tv_lookpost_cont);
        mBLookPostSend = (TextView) get(R.id.send);
        mRvLookpostShow = (RecyclerView) get(R.id.rv_lookpost_show);
        //带值跳转过来
        Intent intent = ((LookPostActivity) mContext).getIntent();
        String title = intent.getStringExtra("title");
        String cont = intent.getStringExtra("cont");
        mId = intent.getStringExtra("id");
        //赋值
        mTvLookpostTitle.setText(title);
        mTvLookpostCont.setText(cont);
        //点击事件
        // mBLookPostSend.setOnClickListener(this);
        // getHttpMessage();
        //适配器
//        mGetMessageAdapter = new GetMessageAdapter(mContext);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
//        mRvLookpostShow.setLayoutManager(linearLayoutManager);
//        mRvLookpostShow.setAdapter(mGetMessageAdapter);
    }

//    private void getHttpMessage() {
//        Map map=new HashMap<>();
//        map.put("msg_id",mId);
//        new Utility(mContext).isLoadCache(Http.BASE_GETMESSAGE,true,true).result(new HttpListener() {
//            @Override
//            public void success(String data) {
//                if(data.trim().isEmpty()){
//                   // Toast.makeText(mContext,"k"+data,Toast.LENGTH_LONG).show();
//                    return;
//                }
//                else{
////                    Toast.makeText(mContext,"ooooooo"+data,Toast.LENGTH_LONG).show();
////                    GetMessageBean getMessageBean= JsonToBean.jsonToBean(data, GetMessageBean.class);
////                    List<GetMessageBean.ItemsBean> items = getMessageBean.getItems();
////                    mGetMessageAdapter.setList(items);
//                }
//
//
//
//
//
//            }
//
//            @Override
//            public void fail(String error) {
//
//            }
//        }).get(Http.BASE_GETMESSAGE,map);
//    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()){
//            case R.id.send:
//                doHttpSendPost();
//                break;
//        }
//    }

//    private void doHttpSendPost() {
//        String userId = SharedPreUtils.getString(mContext, "userId", "");
//        String userNickName = SharedPreUtils.getString(mContext, "userNickName", "");
//        String useName = SharedPreUtils.getString(mContext, "useName", "");
//        String user_occupation = SharedPreUtils.getString(mContext, "user_occupation", "");
////        String mEtLookPostInputGet=mEtLookPostInput.getText().toString();
//        Map map=new HashMap<>();
//        map.put("nike_name",userNickName);
//       // map.put("msg_content",mEtLookPostInputGet);
//        map.put("msg_user",useName);
//        map.put("msg_id",mId);
//        map.put("user_occupation",user_occupation);
//        new Utility(mContext).isLoadCache(Http.BASE_REPLYPOST,true,true).result(new HttpListener() {
//            @Override
//            public void success(String data) {
//                Toast.makeText(mContext,"回复成功"+data,Toast.LENGTH_LONG).show();
//                getHttpMessage();
//            }
//
//            @Override
//            public void fail(String error) {
//
//            }
//        }).get(Http.BASE_REPLYPOST,map);
//    }

}
