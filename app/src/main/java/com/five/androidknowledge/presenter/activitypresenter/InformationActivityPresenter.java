package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvplibrary.mvp.basemodel.IModel;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.five.androidknowledge.activity.InformationActivity;
import com.five.androidknowledge.model.InformationBean;
import com.five.androidknowledge.model.IsLove;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者：秦永聪
 * 日期：2018/12/19
 * 内容：
 */
public class InformationActivityPresenter extends AppDelegate implements View.OnClickListener {

    private ImageView mImInformationLove;
    private String mUserId;
    private String mUserNickName;
    private String mUserid;
    private String mUickname;
    private TextView mTvInformationName;
    private TextView mTvInformationSex;
    private TextView mTvInformationAge;
    private TextView mTvInformationAddress;
    private TextView mTvInformationPhone;
    private TextView mTvInformationWb;
    private TextView mTvInformationNinkname;
    private IsLove mIModel;
    @Override
    public int getlayoutId() {
        return R.layout.activity_information;
    }
      private Context mContext;
    @Override
    public void getContext(Context context) {
     this.mContext=context;
    }
    @Override
    public void initData() {
         //找控件
        mImInformationLove = get(R.id.im_information_love);
        mTvInformationName=get(R.id.tv_information_name);
        mTvInformationSex = get(R.id.tv_information_sex);
        mTvInformationAge = get(R.id.tv_information_age);
        mTvInformationAddress = get(R.id.tv_information_address);
        mTvInformationPhone = get(R.id.tv_information_phone);
        mTvInformationWb = get(R.id.tv_information_wb);
        mTvInformationNinkname = get(R.id.tv_information_ninkname);
        mImInformationLove.setOnClickListener(this);
        String islove = SharedPreUtils.getString(mContext, "islove");
        if(islove.equals("0")){
            mImInformationLove.setImageResource(R.drawable.love);
        }
        else{
            mImInformationLove.setImageResource(R.drawable.nolove);
        }
        //取数据
               //带值跳转过来
        mUserId = SharedPreUtils.getString(mContext, "userId", "");
        mUserNickName = SharedPreUtils.getString(mContext, "userNickName", "");
        Intent intent = ((InformationActivity) mContext).getIntent();
        mUserid = intent.getStringExtra("userId");
        mUickname = intent.getStringExtra("nickname");
        doHttpShow();

    }

    private void doHttpShow() {
        Map map=new HashMap<>();
        map.put("user_id",mUserid);
        new Utility(mContext).isLoadCache(Http.BASE_INFORMATIONSHOW,true,true).result(new HttpListener() {

            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                InformationBean iModel = JsonToBean.jsonToBean(data, InformationBean.class);
                InformationBean.ItemsBean items = iModel.getItems();
                mTvInformationAddress.setText(items.getUser_address());
                mTvInformationAge.setText(items.getUser_age());
                mTvInformationName.setText(items.getUser_name());
                mTvInformationPhone.setText(items.getUser_phone());
                mTvInformationSex.setText(items.getUser_sex());
                mTvInformationWb.setText(items.getUser_wb());
                mTvInformationNinkname.setText(items.getUser_nickname());
            }

            @Override
            public void fail(String error) {
            }
        }).get(Http.BASE_INFORMATIONSHOW,map);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.im_information_love:
                   doIsHttp();
                break;
        }
    }
//判断是否关注
    private void doIsHttp() {
        Map map=new HashMap<>();
        map.put("my_user",mUserId);
        map.put("student_user",mUserid);
        new Utility(mContext).isLoadCache(Http.BASE_ISLOVE,true,true).result(new HttpListener() {

            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                IsLove mIModel = JsonToBean.jsonToBean(data, IsLove.class);
                if(mIModel.getStatus()==0){
                    doHttpCancel();
                }
                else{
                    doHttpLove();
                }
        }

            @Override
            public void fail(String error) {



            }
        }).get(Http.BASE_ISLOVE,map);
    }
  //取消关注
    private void doHttpCancel() {
        Map map=new HashMap<>();
        map.put("my_user",mUserId);
        map.put("student_user",mUserid);
        new Utility(mContext).isLoadCache(Http.BASE_CANCEL,true,true).result(new HttpListener() {

            public void success(String data) {
                if (data == null){
                    return;
                }
                mImInformationLove.setImageResource(R.drawable.nolove);
                SharedPreUtils.put(mContext,"islove","1");
                Toast.makeText(mContext,"取消成功"+data,Toast.LENGTH_LONG).show();
            }

            @Override
            public void fail(String error) {
            }
        }).get(Http.BASE_CANCEL,map);
    }

    //关注喜欢
    private void doHttpLove() {
        Map map=new HashMap<>();
        map.put("my_user",mUserId);
        map.put("my_nick",mUserNickName);
        map.put("student_user",mUserid);
        map.put("student_nick",mUickname);
            new Utility(mContext).isLoadCache(Http.BASE_LOVE,true,true).result(new HttpListener() {
                    @Override
                public void success(String data) {
                        if (data == null){
                            return;
                        }
                    mImInformationLove.setImageResource(R.drawable.love);
                        SharedPreUtils.put(mContext,"islove","0");
                    Toast.makeText(mContext,"关注成功",Toast.LENGTH_LONG).show();
                }
                @Override
                public void fail(String error) {
                }
            }).get(Http.BASE_LOVE,map);
    }


}
