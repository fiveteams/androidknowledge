package com.five.androidknowledge.presenter.activitypresenter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.facebook.drawee.view.SimpleDraweeView;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.MainActivity;
import com.five.androidknowledge.activity.UpdateActivity;
import com.five.androidknowledge.activity.UserActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import me.leefeng.citypicker.CityPicker;
import me.leefeng.citypicker.CityPickerListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;


/**
 * 作者：刘进
 * 日期：2018/12/14
 * 内容：
 **/
public class UserActivityPresenter extends AppDelegate implements View.OnClickListener, CityPickerListener {
    private Context mContext;
    private TextView mNmae1, mUseraddress, mSex;
    private RelativeLayout mSelector;
    private CityPicker mCityPicker;
    private String mUseName;
    private TextView mUsername, mPhone, mCompany, mProfessiona, mAge;
    private SimpleDraweeView mUserImage;
    private Bitmap mHead;
    private static String path = "/sdcard/myHead/";// sd路径


    @Override
    public int getlayoutId() {
        return R.layout.activity_user;
    }

    @Override
    public void initData() {
        //path = Environment.getExternalStorageDirectory().getAbsolutePath();
        //初始化控件
        initView();
        //点击
        setClick(this, R.id.tv_username,
                R.id.btn_out,
                R.id.iv_head_image,

                R.id.iv_right_userpasswrod,//昵称
                R.id.iv_right_userphone,//手机号
                R.id.iv_right_company,//公司名称
                R.id.iv_right_wb_account,//职业
                R.id.age,//年龄
                R.id.update_password,//重置密码
                R.id.iv_head_image//头像
        );

        //地址联动
        city();

        //修改地址的网络请求
        String nickName = SharedPreUtils.getString(mContext, "userNickName", "");
        mUsername.setText(nickName);

    }


    //初始化控件
    private void initView() {
        mNmae1 = get(R.id.tv_name);
        //mPassword = get(R.id.tv_password);
        mUsername = get(R.id.tv_username);//呢称
        mUserImage = get(R.id.iv_head_image);//头像
        mPhone = get(R.id.tv_phone);//手机号
        mCompany = get(R.id.tv_compan);//公司
        mProfessiona = get(R.id.tv_professiona);//职业
        mAge = get(R.id.tv_age);//性别
        //mUseraddress = get(R.id.tv_address);
        mSex = get(R.id.tv_sex);
        mUseName = SharedPreUtils.getString(mContext, "useName");
        String userNickName = SharedPreUtils.getString(mContext, "userNickName");
        String userAddress = SharedPreUtils.getString(mContext, "userAddress");
        String userEmail = SharedPreUtils.getString(mContext, "userEmail");
        String userTime = SharedPreUtils.getString(mContext, "userTime");
        String userId = SharedPreUtils.getString(mContext, "userId");
        mNmae1.setText(mUseName);
    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    //点击事件
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(mContext, UpdateActivity.class);
        switch (view.getId()) {
            case R.id.iv_head_image:
                setPhoto();
                break;
            case R.id.btn_out://退出登陆
                outLogin();
                break;
            case R.id.iv_right_userpasswrod://昵称
                String userName = mUsername.getText().toString().trim();
                intent.putExtra("userName", userName);
                intent.putExtra("id", "1");
                mContext.startActivity(intent);
                break;
            case R.id.iv_right_userphone://手机号
                String mPhon = mPhone.getText().toString().trim();
                intent.putExtra("userName", mPhon);
                intent.putExtra("id", "2");
                mContext.startActivity(intent);
                break;
            case R.id.iv_right_company://公司名称
                String mCompan = mCompany.getText().toString().trim();
                intent.putExtra("userName", mCompan);
                intent.putExtra("id", "3");
                mContext.startActivity(intent);
                break;
            case R.id.iv_right_wb_account://职业
                String mProfession = mProfessiona.getText().toString().trim();
                intent.putExtra("userName", mProfession);
                intent.putExtra("id", "4");
                mContext.startActivity(intent);
                break;
            case R.id.age:
                //修改年龄

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                View views = View.inflate(mContext, R.layout.activity_age_alert, null);
                builder.setView(views);
                final AlertDialog alertDialog = builder.create();
                views.findViewById(R.id.photo_alert).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateAgeHttp("男");
                        alertDialog.dismiss();
                    }
                });
                views.findViewById(R.id.pic_alert).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateAgeHttp("女");
                        alertDialog.dismiss();
                    }
                });
                views.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
                break;
            case R.id.update_password:

                intent.putExtra("id", "5");
                mContext.startActivity(intent);
                break;
        }
    }

    //退出登录
    private void outLogin() {
        SharedPreUtils.put(mContext, "useName", "");
        SharedPreUtils.put(mContext, "userNickName", "");
        SharedPreUtils.put(mContext, "userAddress", "");
        SharedPreUtils.put(mContext, "userEmail", "");
        SharedPreUtils.put(mContext, "userTime", "");
        SharedPreUtils.put(mContext, "userId", "");
        ((UserActivity) mContext).finish();
    }

    //调用相机相册
    private void setPhoto() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = View.inflate(mContext, R.layout.activity_alert, null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        view.findViewById(R.id.photo_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent2.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
                        "head.png")));
                ((UserActivity) mContext).startActivityForResult(intent2, 2);//采用ForResult打开
                alertDialog.dismiss();

            }
        });
        view.findViewById(R.id.pic_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Intent.ACTION_PICK, null);
                intent1.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                ((UserActivity) mContext).startActivityForResult(intent1, 1);//采用ForResult打开
                alertDialog.dismiss();
            }
        });
        view.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    public void setData(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    cropPhoto(data.getData());// 裁剪图片
                }

                break;
            case 2:
                if (resultCode == RESULT_OK) {
                    File temp = new File(Environment.getExternalStorageDirectory() + "/head.png");
                    cropPhoto(Uri.fromFile(temp));// 裁剪图片
                }

                break;
            case 3:
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras == null) {
                        return;
                    }
                    mHead = extras.getParcelable("data");
                    if (mHead != null) {
                        mUserImage.setImageBitmap(mHead);
                        String fileName = path + "/head.png";//图片名字
                        setPicToView(mHead);//保存在SD卡中
                        File file1 = new File(fileName);
                        uploadPic(file1);
                    }
                }
                break;
            default:
                break;

        }
    }

    private void uploadPic(File file1) {
        RequestBody file = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", "head.png", file);
        String userId = SharedPreUtils.getString(mContext, "userId");
        Map<String, String> map = new HashMap<>();
        map.put("user_hidden", userId);
        new Utility(mContext).isLoadCache(Http.UPDATEIMAGE, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                toast("1");
            }

            @Override
            public void fail(String error) {
                toast("2");
            }
        }).part(Http.UPDATEIMAGE, map, part);

    }

    /**
     * 调用系统的裁剪功能
     *
     * @param uri
     */
    public void cropPhoto(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        ((UserActivity) mContext).startActivityForResult(intent, 3);
    }

    private void setPicToView(Bitmap mBitmap) {
        String sdStatus = Environment.getExternalStorageState();
        if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
            return;
        }
        FileOutputStream b = null;
        File file = new File(path);
        file.mkdirs();// 创建文件夹
        String fileName = path + "head.png";// 图片名字
        try {
            b = new FileOutputStream(fileName);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);// 把数据写入文件
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭流
                b.flush();
                b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //修改性别
    private void updateAgeHttp(final String people) {
        String useName = SharedPreUtils.getString(mContext, "useName");
        Map<String, String> map = new HashMap<>();
        map.put("user_name", useName);
        map.put("user_sex", people);
        new Utility(mContext).isLoadCache(Http.UPDATEAGE, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                toast("修改成功");
                mAge.setText(people);
                SharedPreUtils.put(mContext, "userSex", people);
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.UPDATEAGE, map);
    }

    //修改地址
    private void addressHttp(String address) {
        String useName = SharedPreUtils.getString(mContext, "useName");
        Map<String, String> map = new HashMap<>();
        map.put("user_name", useName);
        map.put("user_address", address);
        new Utility(mContext).isLoadCache(Http.UPDATAEADDRESS, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                //toast("修改成功");
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.UPDATAEADDRESS, map);
    }

    //三级联动
    private void city() {
        mCityPicker = new CityPicker((UserActivity) mContext, this);

        mSelector = get(R.id.relativeLayo);
        //显示
        mUseraddress = get(R.id.tv_address);

        mSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCityPicker.show();
            }
        });
        String userAddress = SharedPreUtils.getString(mContext, "userAddress");
        mUseraddress.setText(userAddress);
        addressHttp(userAddress);
    }

    //toast方法
    public void toast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public void getCity(String s) {
        mUseraddress.setText(s);
        SharedPreUtils.put(mContext, "userAddress", s);

    }


    public void onBackPressed() {
        if (mCityPicker.isShow()) {
            mCityPicker.close();
            return;
        }

    }

    //onResume()
    public void onResume() {
        String nickName = SharedPreUtils.getString(mContext, "userNickName", "");
        mUsername.setText(nickName);
        String userPhone = SharedPreUtils.getString(mContext, "userPhone", "");
        mPhone.setText(userPhone);
        String userCompany = SharedPreUtils.getString(mContext, "userCompany", "");
        mCompany.setText(userCompany);
        String useroCcupation = SharedPreUtils.getString(mContext, "useroCcupation", "");
        mProfessiona.setText(useroCcupation);
        String userSex = SharedPreUtils.getString(mContext, "userSex", "");
        mAge.setText(userSex);
    }

}
