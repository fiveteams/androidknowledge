package com.five.androidknowledge.presenter.fragmentpresenter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.Logger;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.adapter.HistoryAdapter;
import com.five.androidknowledge.model.HistoryBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者：马利亚
 * 日期：2018/12/11
 * 内容：历史博文
 */
public class HistoryFragmentPresenter extends AppDelegate{

    private static final String TAG=HistoryFragmentPresenter.class.getName();
    private Context mContext;
    //需要显示的数据
    private List<HistoryBean.ItemsBean> mItems;
    //用于记录当前的状态
    private RecyclerView mRecycle;
    private HistoryAdapter mHistoryadapter;
    private String mId;
    private MaterialRefreshLayout mRefresh;

    @Override
    public int getlayoutId() {
        return R.layout.fragment_history;
    }

    @Override
    public void initData() {
        //初始化控件
        mRefresh = (MaterialRefreshLayout) get(R.id.mrl_history_frament_refresh);
        mRecycle = (RecyclerView) get(R.id.rv_history_frament_recycle);
        //设置适配器
        //设置布局管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mHistoryadapter=new HistoryAdapter(mContext);
        mRecycle.setAdapter(mHistoryadapter);
        mRecycle.setLayoutManager(linearLayoutManager);
        //刷新
        initRefreshLayout();
        //解析数据
        doHttp();
    }
    /**
     * 刷新数据
     * */
    private void initRefreshLayout() {
        //设置支持下拉加载更多
        mRefresh.setLoadMore(true);
        //刷新以及加载回调
        mRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                //向服务器请求数据
                doHttp();

            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                doHttp();
            }
        });
    }
    /**
     * 解析数据
     * */
    private void doHttp() {
        //创建hashMap
        Map<String, String> map = new HashMap<>();
        map.put("news_type", mId);
        //解析数据
        new Utility(mContext).isLoadCache(Http.BASE_ARTICLE,true,true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                HistoryBean historyBean=JsonToBean.jsonToBean(data, HistoryBean.class);
                mHistoryadapter.setList(historyBean.getItems());
                mRefresh.finishRefresh();
                mRefresh.finishRefreshLoadMore();
            }
            @Override
            public void fail(String error) {

            }
        }).get(Http.BASE_ARTICLE,map);
    }

    @Override
    public void getContext(Context context) {
        this.mContext=context;
    }

    public void sentType(String id) {
        this.mId = id;
    }
}
