package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.LoginActivity;
import com.five.androidknowledge.adapter.GroupChatAdapter;
import com.five.androidknowledge.gif.GifEditText;
import com.five.androidknowledge.model.ForGroupChatBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者：秦永聪
 * 日期：2018/12/17
 * 内容：进入群聊
 */
public class GroupChatActivityPresenter extends AppDelegate implements View.OnClickListener {
    private int mCurPage = 10;
    private RecyclerView mRvGroupchatShow;
    private GroupChatAdapter mGroupChatAdapter;
    private String mEtGroupChatInputGet;
    private GifEditText mEtPlayInput;
    private TextView mBPlaySend;
    private MaterialRefreshLayout mMrlOpensourcePlayRefresh;
    private String mUserNickName;
    private String mUserId;

    @Override
    public int getlayoutId() {
        return R.layout.activity_groupchat;
    }

    private Context mContext;

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    @Override
    public void initData() {
        //找控件
        mRvGroupchatShow = (RecyclerView) get(R.id.rv_groupchat_show);
        mEtPlayInput = (GifEditText) get(R.id.edit_text);
        mBPlaySend = (TextView) get(R.id.send);
        mMrlOpensourcePlayRefresh = (MaterialRefreshLayout) get(R.id.mrl_opensource_play_refresh);
        //点击事件
        mBPlaySend.setOnClickListener(this);
        dohttp();
        //适配器
        mGroupChatAdapter = new GroupChatAdapter(mContext);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mRvGroupchatShow.setLayoutManager(linearLayoutManager);
        mRvGroupchatShow.setAdapter(mGroupChatAdapter);
        initRefreshLayout();
    }

    //上拉下拉
    private void initRefreshLayout() {
        //设置支持下拉加载更多
        mMrlOpensourcePlayRefresh.setLoadMore(true);
        //刷新以及加载回调
        mMrlOpensourcePlayRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                mCurPage = 10;
                dohttp();
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                mCurPage += 10;
                dohttp();
            }
        });
    }

    /**
     * 展示请求
     */
    private void dohttp() {
        Map map = new HashMap<>();
        map.put("open_page", 1);
        map.put("open_num", mCurPage);
        new Utility(mContext).isLoadCache(Http.BASE_GROUPCHAT, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data.trim() == null){
                    return;
                }
                ForGroupChatBean iModel = JsonToBean.jsonToBean(data, ForGroupChatBean.class);
                List<ForGroupChatBean.ItemsBean> items = iModel.getItems();
                //将数据传递到适配器
                mGroupChatAdapter.setList(items);
                mMrlOpensourcePlayRefresh.finishRefresh();
                mMrlOpensourcePlayRefresh.finishRefreshLoadMore();

            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.BASE_GROUPCHAT, map);
    }

    /**
     * 点击事件
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send:
                SharedPreUtils.put(mContext, "id", "1");
                mEtGroupChatInputGet = mEtPlayInput.getText().toString().trim();
                //判断是否有发送的消息
                if (TextUtils.isEmpty(mEtGroupChatInputGet)){
                    Toast.makeText(mContext, "请输入聊天信息后发送~", Toast.LENGTH_SHORT).show();
                    return;
                }
                //判断用户是否登录
                if (TextUtils.isEmpty(mUserNickName)) {
                    mContext.startActivity(new Intent(mContext, LoginActivity.class));
                } else {
                   dohttpSend();
                }
            break;
        }
    }

    /**
     * 点击发送群聊
     */
    private void dohttpSend() {
        SharedPreUtils.put(mContext, "id", "1");
        Map map = new HashMap<>();
        map.put("nike_name", mUserNickName);
        map.put("msg_user", mUserId);
        map.put("msg_content", mEtGroupChatInputGet);
        new Utility(mContext).isLoadCache(Http.BASE_ADDGROUPCHAT, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                mEtPlayInput.setText("");
                dohttp();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.BASE_ADDGROUPCHAT, map);
    }
    /**
    *   获取用户信息
    */
    public void getSharedPreUtils() {
        mUserNickName = SharedPreUtils.getString(mContext, "userNickName", "");
        mUserId = SharedPreUtils.getString(mContext, "userId", "");
    }
}
