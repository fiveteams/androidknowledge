package com.five.androidknowledge.presenter.fragmentpresenter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.percentage.PercentRelativeLayout;
import com.facebook.drawee.view.SimpleDraweeView;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.AttentionActivity;
import com.five.androidknowledge.activity.ConcernActivity;
import com.five.androidknowledge.activity.LoginActivity;
import com.five.androidknowledge.activity.MyShareActivity;
import com.five.androidknowledge.activity.PostsActivity;
import com.five.androidknowledge.activity.UserActivity;
import com.five.androidknowledge.customview.BounceScrollView;

/**
 * 作者：刘进
 * 日期：2018/12/11
 * 内容：我的页面
 **/
public class AccountFragmentPresenter extends AppDelegate implements View.OnClickListener {
    private Context mContext;
    private TextView mLogin;
    private LinearLayout mLlPop;
    private SimpleDraweeView mIvImageget;
    private String mUserId;

    @Override
    public int getlayoutId() {
        return R.layout.fragment_user;
    }

    @Override
    public void initData() {
        //初始化控件
        initView();
        //点击事件
        setClick(this, R.id.iv_image, R.id.tv_login
                , R.id.tv_set, R.id.pl_user_concern, R.id.pl_attention
                , R.id.pl_my_share, R.id.my_sercive
        );
    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    //初始化控件
    private void initView() {
        mLogin = get(R.id.tv_login);
        mIvImageget = get(R.id.iv_image);
        mLlPop = get(R.id.ll_pop);
    }

    //点击事件
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_image://头像
                mUserId = SharedPreUtils.getString(mContext, "userId", "");
                if (TextUtils.isEmpty(mUserId) && mUserId == "") {
                    mLogin.setText("未登录");
                    mContext.startActivity(new Intent(mContext, LoginActivity.class));
                } else {
                    String userNickName = SharedPreUtils.getString(mContext, "userNickName");
                    mLogin.setText(userNickName);
                }
                break;
            case R.id.tv_set://设置
                if (TextUtils.isEmpty(mUserId) && mUserId == "") {
                    toast("请先登陆");
                    return;
                } else {//登陆后跳转到用户的详情页面
                    mContext.startActivity(new Intent(mContext, UserActivity.class));
                }

                break;
            case R.id.pl_user_concern://我的关注
                mContext.startActivity(new Intent(mContext, ConcernActivity.class));
                break;
            case R.id.pl_attention://关注我的
                mContext.startActivity(new Intent(mContext, AttentionActivity.class));
                break;
            case R.id.pl_my_share://我的分享
                mContext.startActivity(new Intent(mContext, MyShareActivity.class));
                break;
            case R.id.my_sercive://我的贴子
                mContext.startActivity(new Intent(mContext, PostsActivity.class));
                break;
        }
    }


    //刷新
    public void onResume() {
        String useId = SharedPreUtils.getString(mContext, "userId");
        if (TextUtils.isEmpty(useId) && useId == "") {
            mLogin.setText("未登录");
        } else {
            String userNickName = SharedPreUtils.getString(mContext, "userNickName");
            mLogin.setText(userNickName);
        }
    }

    //toast
    public void toast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


}




