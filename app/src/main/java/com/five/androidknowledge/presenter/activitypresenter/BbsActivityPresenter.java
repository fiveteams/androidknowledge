package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.five.androidknowledge.R;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.five.androidknowledge.activity.BbsActivity;
import com.five.androidknowledge.activity.SendPostActivity;
import com.five.androidknowledge.fragment.AllPostFragment;
import com.five.androidknowledge.fragment.GoodPostFragment;
import com.five.androidknowledge.fragment.HotPostFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：秦永聪
 * 日期：2018/12/20
 * 内容：
 */
public class BbsActivityPresenter  extends AppDelegate {

    private TabLayout mTlBbsTablayout;
    private ViewPager mVpBbsViewpager;
    private List<Fragment> mList = new ArrayList<>();
    private List<String> mTitles = new ArrayList<>();
    private MyAdapter myAdapter;
    private ImageView mIvPostSendpost;

    @Override
    public int getlayoutId() {
        return R.layout.activity_bbs;
    }


    private Context mContext;
    @Override
    public void getContext(Context context) {
       this.mContext=context;
    }
    @Override
    public void initData() {
        mTlBbsTablayout =(TabLayout)get(R.id.tl_bbs_tablayout);
        mVpBbsViewpager =(ViewPager)get(R.id.vp_bbs_viewpager);
        mIvPostSendpost =(ImageView)get(R.id.iv_post_sendpost);
        //点击事件
         mIvPostSendpost.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
               mContext.startActivity(new Intent(mContext, SendPostActivity.class));
             }
         });
        //赋值
        mTitles.add("全部帖子");
        mTitles.add("获取精华");
        mTitles.add("获取热门");

        mList.add(new AllPostFragment());
        mList.add(new GoodPostFragment());
        mList.add(new HotPostFragment());
        //适配器
        myAdapter = new MyAdapter(((BbsActivity) mContext).getSupportFragmentManager());
        mVpBbsViewpager.setOffscreenPageLimit(4);
        mTlBbsTablayout.setupWithViewPager(mVpBbsViewpager);
        mVpBbsViewpager.setAdapter(myAdapter);
    }
    /**
     *
     *设置适配器
     */

    public class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            Bundle bundle = new Bundle();
            Fragment fragment = mList.get(i);
            String id = mTitles.get(i);
            bundle.putString("id",id);
            fragment.setArguments(bundle);
            return mList.get(i);
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }
    }
}
