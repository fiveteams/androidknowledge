package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.MD5Utils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.LoginActivity;
import com.five.androidknowledge.model.EmailBean;
import com.five.androidknowledge.model.IsRegisteredBean;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者：刘进
 * 日期：2018/12/12
 * 内容：注册页面
 **/
public class RegisteredActivityPresenter extends AppDelegate implements View.OnClickListener {
    private Context mContext;
    private EditText mName, mPwd, mPwds, mEmail;
    private String mNames;
    private int mCode;
    private Button mBtnEmail;
    private String mNameEmail;
    /**
     * 正则表达式：验证邮箱
     */
    public static final String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

    @Override
    public int getlayoutId() {
        return R.layout.activity_registered;
    }

    @Override
    public void initData() {
        //初始化控件
        mName = get(R.id.et_usename);
        mPwd = get(R.id.et_password);
        mPwds = get(R.id.et_password);
        mEmail = get(R.id.et_email);
        mBtnEmail = get(R.id.bt_email);
        //timeCount = new TimeCount(60000,1000);
        //点击事件
        setClick(this, R.id.bt_registered, R.id.bt_email);

    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    //点击事件
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_registered://注册
                mNames = mName.getText().toString().trim();
                String pwd = mPwd.getText().toString().trim();
                String pwds = mPwds.getText().toString().trim();
                String email = mEmail.getText().toString().trim();

                toast("输入的验证码" + email);
                //if (REGEX_EMAIL.equals(name)){
                if (TextUtils.isEmpty(mNames)) {
                    toast("邮箱不能为空");
                    return;
                }

                if (!email.contains(mCode + "")) {
                    toast("您输入的验证码错误" + email + "获得" + mCode);
                    return;
                }
                if (TextUtils.isEmpty(pwd)) {
                    toast("密码不能为空");
                    return;
                }

                if (TextUtils.isEmpty(pwds)) {
                    toast("密码不能为空");
                    return;
                }

                if (!pwd.contains(pwds)) {
                    toast("您两次输入的密码不同,请重新输入");
                    return;
                }
                registeredHttp(email, mNames, pwd);
                break;
            case R.id.bt_email:
                mNameEmail = mName.getText().toString().trim();
                //timeCount.start();
                //判断是否已经注册
                //if (REGEX_EMAIL.equals(nameEmail)){
                if (TextUtils.isEmpty(mNameEmail)) {
                    toast("邮箱不能为空");
                } else {
                    isRegistered(mNameEmail);
                }
                break;
        }
    }

    //判断用户是否已经注册
    private void isRegistered(final String nameEmail) {
        Map<String, String> map = new HashMap<>();
        map.put("user_name", nameEmail);
        new Utility(mContext).isLoadCache(Http.ISREGISTERED, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                IsRegisteredBean isRegisteredBean = JsonToBean.jsonToBean(data, IsRegisteredBean.class);
                if (isRegisteredBean.getStatus() == 0) {
                    toast("用户已经注册");
                    return;
                } else {
                    emailHttp(nameEmail);
                    return;
                }
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.ISREGISTERED, map);

    }

    //获取验证码
    private void emailHttp(String nameEmail) {
        Map<String, String> map = new HashMap<>();
        map.put("user_name", nameEmail);
        new Utility(mContext).isLoadCache(Http.REGISTERED_EMAIL, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                EmailBean emailBean = JsonToBean.jsonToBean(data, EmailBean.class);
                mCode = emailBean.getCode();
                toast("code===" + mCode);
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.REGISTERED_EMAIL, map);
    }

    //网络请求
    private void registeredHttp(String email, final String name, final String pwd) {
        //md5加密 密码
        String md5 = new MD5Utils().getMD5(pwd);
        Map<String, String> map = new HashMap<>();
        map.put("user_name", name);
        map.put("user_pass", md5);
        Toast.makeText(mContext, name + "kk" + md5, Toast.LENGTH_LONG).show();
        new Utility(mContext).isLoadCache(Http.REGISTERED, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                //RegisteredBean registeredBean = JsonToBean.jsonToBean(data, RegisteredBean.class);
                //int status = registeredBean.getStatus();
                if (data.contains("0")) {
                    Toast.makeText(mContext, "注册成功", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    intent.putExtra("name", name);
                    intent.putExtra("pwd", pwd);
                    mContext.startActivity(intent);
                } else {
                    toast("注册失败");
                }
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.REGISTERED, map);

    }

    //toast
    public void toast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


    /**
     *
     * public class TimeCount extends CountDownTimer{

     public TimeCount(long millisInFuture, long countDownInterval) {
     super(millisInFuture, countDownInterval);
     }

     @Override public void onTick(long millisUntilFinished) {
     mBtnEmail.setBackgroundColor(Color.parseColor("#d43c3c"));
     mBtnEmail.setClickable(false);
     mBtnEmail.setText("("+millisUntilFinished/1000 +")秒后可重新发送");

     }

     @Override public void onFinish() {
     mBtnEmail.setText("重新获取");
     mBtnEmail.setClickable(true);
     mBtnEmail.setBackgroundColor(Color.parseColor("#d43c3c"));
     }
     }
     */

}
