package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.CodeUtils;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.MD5Utils;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.LoginActivity;
import com.five.androidknowledge.activity.MainActivity;
import com.five.androidknowledge.activity.RegisteredActivity;
import com.five.androidknowledge.adapter.ConcernAdapter;
import com.five.androidknowledge.model.ConcernBean;
import com.five.androidknowledge.model.LoginBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.mvplibrary.utils.JsonToBean.jsonToBean;

/**
 * 作者：刘进
 * 日期：2018/12/12
 * 内容：关注我的
 **/
public class AttentionActivityPresenter extends AppDelegate{
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ConcernAdapter mAdapter;
    @Override
    public int getlayoutId() {
        return R.layout.activity_attention;
    }

    @Override
    public void initData() {
        mRecyclerView = get(R.id.recyclerView);
        mAdapter = new ConcernAdapter(mContext);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mAdapter);

//        mAdapter.result(new ConcernAdapter.onClick() {
//            @Override
//            public void click(String student_user) {
//                //myAttentionHttp();
//            }
//        });
        //展示关注我的
        myAttentionHttp();
    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }


    //展示关注我的
    private void myAttentionHttp() {
        String userId = SharedPreUtils.getString(mContext, "userId");
        Map<String,String> map = new HashMap<>();
        map.put("my_user",userId);
        toast(userId);
        new Utility(mContext).isLoadCache(Http.GETHISCONCERN,false,true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                ConcernBean concernBean = JsonToBean.jsonToBean(data, ConcernBean.class);
                List<ConcernBean.ItemsBean> items = concernBean.getItems();
                mAdapter.setList(items);
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.GETHISCONCERN,map);
    }

    //toast
    public void toast(String msg){
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

}
