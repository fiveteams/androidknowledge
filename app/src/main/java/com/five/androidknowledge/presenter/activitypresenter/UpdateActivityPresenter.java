package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.MD5Utils;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.UpdateActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者：刘进
 * 日期：2018/12/14
 * 内容：
 **/
public class UpdateActivityPresenter extends AppDelegate implements View.OnClickListener {
    private Context mContext;
    private EditText mEditText, mBeforPassword, mNewsPassword;
    private Button mUpdate;
    private String mText;
    private String mEdit;
    private String mId;
    private Intent mIntent;
    private RelativeLayout mUpdatePassword, mUpdateHide;

    @Override
    public int getlayoutId() {
        return R.layout.activity_update;
    }

    @Override
    public void initData() {

        //初始化
        mEditText = get(R.id.ed_update);
        mUpdate = get(R.id.btn_update);
        mBeforPassword = get(R.id.ed_befor_password);
        mNewsPassword = get(R.id.ed_new_password);

        mUpdatePassword = get(R.id.update);
        mUpdateHide = get(R.id.relativeLayout);

        mIntent = ((UpdateActivity) mContext).getIntent();
        mText = mIntent.getStringExtra("userName");


        mEditText.setText(mText);
        mId = mIntent.getStringExtra("id");
        if (mId.equals("5")) {
            mUpdateHide.setVisibility(View.GONE);
            mUpdatePassword.setVisibility(View.VISIBLE);
        } else {
            mUpdateHide.setVisibility(View.VISIBLE);
            mUpdatePassword.setVisibility(View.GONE);
        }
        //点击
        setClick(this, R.id.btn_update, R.id.btn_update_password);
        //修改用户信息

    }

    //点击
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_update:
                mEdit = mEditText.getText().toString().trim();
                mId = mIntent.getStringExtra("id");
                if (mId.equals("1")) {
                    updateHttp();//修改昵称
                }
                if (mId.equals("2")) {
                    updateHttpPhone();//修改手机号
                }
                if (mId.equals("3")) {
                    updateHttpCompany();//修改公司名称
                }
                if (mId.equals("4")) {
                    updateHttpProfessional();//修改职业
                }
                break;

            case R.id.btn_update_password:
                mId = mIntent.getStringExtra("id");
                String mBefor = mBeforPassword.getText().toString().trim();
                String mMd5Befor = new MD5Utils().getMD5(mBefor);
                String mNew = mNewsPassword.getText().toString().trim();
                String md5 = new MD5Utils().getMD5(mNew);
                if (mId.equals("5")) {
                    String userPass = SharedPreUtils.getString(mContext, "userPass");
                    Log.d("UpdateActivityPresenter", "未加密" + userPass);
                    //String userPass1 = new MD5Utils().getMD5(userPass);
                    Log.d("UpdateActivityPresenter", "==========" + mMd5Befor + "==========" + userPass);
                    if (mMd5Befor.equals(userPass) && !TextUtils.isEmpty(mNew)) {
                        updateHttpPassword(md5);
                    } else {
                        Toast.makeText(mContext, "修改失败", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    //修改密码
    private void updateHttpPassword(String newpassword) {
        String useName = SharedPreUtils.getString(mContext, "useName");
        Map<String, String> map = new HashMap<>();
        map.put("user_name", useName);
        map.put("user_pass", newpassword);
        new Utility(mContext).isLoadCache(Http.UPDATEPASSWORD, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                toast("密码修改成功");
                destroy();
                ///SharedPreUtils.put(mContext,"useroCcupation",mEdit);
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.UPDATEPASSWORD, map);

    }

    //修改职业
    private void updateHttpProfessional() {
        String useName = SharedPreUtils.getString(mContext, "useName");
        Map<String, String> map = new HashMap<>();
        map.put("user_name", useName);
        map.put("user_occupation", mEdit);
        new Utility(mContext).isLoadCache(Http.UPDATEPROFESSIONAL, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null) {
                    return;
                }
                toast("职业修改成功");
                SharedPreUtils.put(mContext, "user_occupation", mEdit);
                destroy();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.UPDATEPROFESSIONAL, map);

    }

    //修改公司
    private void updateHttpCompany() {
        String useName = SharedPreUtils.getString(mContext, "useName");
        String nickName = SharedPreUtils.getString(mContext, "userNickName");
        Map<String, String> map = new HashMap<>();
        map.put("user_name", useName);
        map.put("user_company", mEdit);
        new Utility(mContext).isLoadCache(Http.UPDATAECOMPANY, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null) {
                    return;
                }
                toast("公司修改成功");
                SharedPreUtils.put(mContext, "userCompany", mEdit);
                destroy();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.UPDATAECOMPANY, map);

    }

    //修改手机号
    private void updateHttpPhone() {
        String useName = SharedPreUtils.getString(mContext, "useName");
        String nickName = SharedPreUtils.getString(mContext, "userNickName");
        Map<String, String> map = new HashMap<>();
        map.put("user_name", useName);

        if (mEdit.length() == 11) {
            map.put("user_phone", mEdit);
        } else {
            Toast.makeText(mContext, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
        }
        new Utility(mContext).isLoadCache(Http.UPDATEPHONE, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                toast("手机号修改成功");
                SharedPreUtils.put(mContext, "userPhone", mEdit);
                destroy();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.UPDATEPHONE, map);

    }

    //修改昵称
    private void updateHttp() {
        //取值
        String useName = SharedPreUtils.getString(mContext, "useName");
        String nickName = SharedPreUtils.getString(mContext, "userNickName");
        Map<String, String> map = new HashMap<>();
        map.put("user_name", useName);
        map.put("user_nickname", mEdit);
        new Utility(mContext).isLoadCache(Http.UPDATEUSER, false, false).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                toast("昵称修改成功");
                SharedPreUtils.put(mContext, "userNickName", mEdit);
                destroy();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.UPDATEUSER, map);
    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    //toast方法
    protected void toast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    //销毁当前页面
    public void destroy() {
        ((UpdateActivity) mContext).finish();
    }
}
