package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.graphics.Color;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.MainActivity;
import com.five.androidknowledge.fragment.UserFragment;
import com.five.androidknowledge.fragment.ScripturesFragment;
import com.five.androidknowledge.fragment.HomeFragment;
import com.five.androidknowledge.fragment.StudyFragment;
import com.five.androidknowledge.fragment.VideoFragment;
import com.hjm.bottomtabbar.BottomTabBar;

/**
 * author：杜威
 * date:2018/12/10
 * function:主页
 * */
public class MainActivityPresenter extends AppDelegate {
    private Context mContext;
    private BottomTabBar mBottomTabBar;

    @Override
    public int getlayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initData() {
        //初始化控件
        mBottomTabBar = get(R.id.bt_tab_bar);
        mBottomTabBar.init(((MainActivity)mContext).getSupportFragmentManager())
                .setImgSize(50,50)
                .setFontSize(13)
                .setTabPadding(4,6,10)
                .setChangeColor(Color.RED,Color.DKGRAY)
                .addTabItem("首页",R.drawable.index_yes,R.drawable.index_no, HomeFragment.class)
                .addTabItem("学习",R.drawable.study_yes,R.drawable.study_no,StudyFragment.class)
                .addTabItem("视频",R.drawable.live_yes,R.drawable.live_no,VideoFragment.class)
                .addTabItem("经典",R.drawable.find_yes,R.drawable.find_no,ScripturesFragment.class)
                .addTabItem("我的",R.drawable.account_yes,R.drawable.account_no,UserFragment.class)
                .isShowDivider(false)
                .setOnTabChangeListener(new BottomTabBar.OnTabChangeListener() {
                    @Override
                    public void onTabChange(int position, String name) {
                        switch (position){
                            case 0:
                                ((MainActivity) mContext).setTitleName("首页");
                                break;
                            case 1:
                                ((MainActivity) mContext).setTitleName("学习");
                                break;
                            case 2:
                                ((MainActivity) mContext).setTitleName("视频");
                                break;
                            case 3:
                                ((MainActivity) mContext).setTitleName("经典");
                                break;
                            case 4:
                                ((MainActivity) mContext).setTitleName("我的");
                                break;
                        }
                    }
                });
    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }
}
