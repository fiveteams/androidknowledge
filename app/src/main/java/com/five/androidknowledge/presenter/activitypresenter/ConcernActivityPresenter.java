package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.adapter.ConcernAdapter;
import com.five.androidknowledge.model.ConcernBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者：刘进
 * 日期：2018/12/12
 * 内容：我的关注
 **/
public class ConcernActivityPresenter extends AppDelegate{
    private Context mContext;
    private ConcernAdapter mAdapter;
    private RecyclerView mRecyclerView;

    @Override
    public int getlayoutId() {

        return R.layout.activity_concern;
    }

    @Override
    public void initData() {
        mRecyclerView = get(R.id.recyclerView);
        mAdapter = new ConcernAdapter(mContext);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.result(new ConcernAdapter.onClick() {
            @Override
            public void click(String student_user) {
                cancelHttp(student_user);
            }
        });
        myConcernHttp();
    }
    //取消关注
    private void cancelHttp(String student_user) {
        String userId = SharedPreUtils.getString(mContext, "userId");
        Map<String,String> map = new HashMap<>();
        map.put("my_user",userId);
        map.put("student_user",student_user);
        new Utility(mContext).isLoadCache(Http.CANCELCONCERN,false,true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                Toast.makeText(mContext, "取消关注成功", Toast.LENGTH_SHORT).show();
                myConcernHttp();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.CANCELCONCERN,map);
    }

    //展示我的关注
    private void myConcernHttp() {
        String userId = SharedPreUtils.getString(mContext, "userId");
        Map<String,String> map = new HashMap<>();
        map.put("my_user",userId);
        new Utility(mContext).isLoadCache(Http.MYCONCERN,false,true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                ConcernBean concernBean = JsonToBean.jsonToBean(data, ConcernBean.class);
                List<ConcernBean.ItemsBean> items = concernBean.getItems();
                if (TextUtils.isEmpty(items+"")){
                    Toast.makeText(mContext, "没有关注", Toast.LENGTH_SHORT).show();
                    return;
                }
                mAdapter.setList(items);
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.MYCONCERN,map);
    }

    @Override
    public void getContext(Context context) {
        this.mContext=context;
    }

    public void onResume() {
        //myConcernHttp();
    }
}
