package com.five.androidknowledge.presenter.fragmentpresenter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.adapter.PostAdapter;
import com.five.androidknowledge.model.PostBean;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者：秦永聪
 * 日期：2018/12/20
 * 内容：
 */
public class HostPostFragmentPresenter extends AppDelegate {

    private MaterialRefreshLayout mMrlPostFramentRefresh;
    private RecyclerView mRvPostFramentRecycle;
    private String mId;
    private PostAdapter mPostAdapter;

    @Override
    public int getlayoutId() {
        return R.layout.fragment_post;
    }

    private Context mContext;

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    @Override
    public void initData() {
        //初始化控件
        mMrlPostFramentRefresh = (MaterialRefreshLayout) get(R.id.mrl_post_frament_refresh);
        mRvPostFramentRecycle = (RecyclerView) get(R.id.rv_post_frament_recycle);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mPostAdapter = new PostAdapter(mContext);
        mRvPostFramentRecycle.setAdapter(mPostAdapter);
        mRvPostFramentRecycle.setLayoutManager(linearLayoutManager);
        initRefreshLayout();
        doHttp();
    }

    /**
     * 刷新数据
     */
    private void initRefreshLayout() {
        //设置支持下拉加载更多
        mMrlPostFramentRefresh.setLoadMore(true);
        //刷新以及加载回调
        mMrlPostFramentRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                //向服务器请求数据
                doHttp();

            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                doHttp();
            }
        });
    }

    private void doHttp() {
        //创建hashMap
        Map<String, String> map = new HashMap<>();
        map.put("size", "1");
        //解析数据
        new Utility(mContext).isLoadCache(Http.BASE_HOSTPOST, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                PostBean allPostBean = JsonToBean.jsonToBean(data, PostBean.class);
                mPostAdapter.setList(allPostBean.getItems());
                mMrlPostFramentRefresh.finishRefresh();
                mMrlPostFramentRefresh.finishRefreshLoadMore();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.BASE_HOSTPOST, map);
    }

}
