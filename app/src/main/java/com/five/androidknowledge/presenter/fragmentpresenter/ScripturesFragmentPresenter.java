package com.five.androidknowledge.presenter.fragmentpresenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mvplibrary.mvp.basemodel.IModel;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.MainActivity;
import com.five.androidknowledge.fragment.HistoryFragment;
import com.five.androidknowledge.model.ScripturesTitle;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：马利亚
 * 日期：2018/12/11
 * 内容：经典
 **/
public class ScripturesFragmentPresenter extends AppDelegate {
    private Context mContext;
    private List<HistoryFragment> mList = new ArrayList<>();
    private MyAdapter myAdapter;
    private List<ScripturesTitle.TitlesBean> mTitles = new ArrayList<>();
    private TabLayout mTablayout;
    private ViewPager mViewpager;

    @Override
    public int getlayoutId() {
        return R.layout.fragment_scriptures;
    }

    @Override
    public void initData() {
        //初始化控件
        mTablayout = (TabLayout) get(R.id.tl_scriptures_tablayout);
        mViewpager = (ViewPager) get(R.id.vp_scriptures_viewpager);
        //设置适配器
        myAdapter = new MyAdapter(((MainActivity) mContext).getSupportFragmentManager());
        mViewpager.setOffscreenPageLimit(4);
        mTablayout.setupWithViewPager(mViewpager);
        mViewpager.setAdapter(myAdapter);

        doHttp();
    }

    /**
     * 解析数据
     */
    private void doHttp() {
        new Utility(mContext).isLoadCache(Http.SCRIPTRURES_TITLE, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                ScripturesTitle scripturesTitle = JsonToBean.jsonToBean(data, ScripturesTitle.class);
                mTitles = scripturesTitle.getTitles();
                for (int i = 0; i < mTitles.size(); i++) {
                    mList.add(new HistoryFragment());
                }
                myAdapter.notifyDataSetChanged();

            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.SCRIPTRURES_TITLE, null);
    }

    /**
     * 设置适配器
     */
    public class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            Bundle bundle = new Bundle();
            HistoryFragment fragment = mList.get(i);
            String id = mTitles.get(i).getId();
            bundle.putString("id", id);
            fragment.setArguments(bundle);
            return mList.get(i);
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position).getName();
        }
    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }
}
