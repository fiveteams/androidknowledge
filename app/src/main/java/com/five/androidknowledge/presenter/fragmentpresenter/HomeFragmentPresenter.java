package com.five.androidknowledge.presenter.fragmentpresenter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.example.mvplibrary.utils.percentage.PercentLinearLayout;
import com.example.mvplibrary.utils.percentage.PercentRelativeLayout;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.BbsActivity;
import com.five.androidknowledge.activity.GroupcChatActivity;
import com.five.androidknowledge.activity.PlayActivity;
import com.five.androidknowledge.activity.ProjectActivity;
import com.five.androidknowledge.activity.QuestionActivity;
import com.five.androidknowledge.activity.TechnicalActivity;
import com.five.androidknowledge.activity.VipActivity;
import com.five.androidknowledge.adapter.HomeBottomAdapter;
import com.five.androidknowledge.model.HomeBottomBean;
import com.five.androidknowledge.model.HomeShufflingBean;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidknowledge.five.com.study.activity.LinkActivity;

/**
 * 作者：秦永聪
 * 日期：2018/12/11
 * 内容：首页
 **/
public class HomeFragmentPresenter extends AppDelegate implements View.OnClickListener {
    private List<HomeShufflingBean.DataBean> mImageList = new ArrayList<>();
    private ViewPager mVphHomeShuffling;
    private List<HomeBottomBean.ItemsBean> mHomeBottomlist = new ArrayList<>();
    private List<String> mImageUris = new ArrayList<>();
    private MyAdPater mMyAdPater;
    private LinearLayout mLlHomePoint;
    private RecyclerView mRvHomeBottomshow;
    private HomeBottomAdapter mHomeBottomAdapter;
    private MaterialRefreshLayout mRefresh;
    private PercentRelativeLayout mRlHomeEveny;
    private PercentLinearLayout mPrlGroupInto;
    private PercentRelativeLayout mRlHomeLive;
    private ImageView mIvHomeTop;
    private ImageView mIvHomeGithub;
    private ImageView mIvHomeWen;
    private PercentRelativeLayout mRlHomeVip;
    private ImageView mIvHomeDay;

    @Override
    public int getlayoutId() {
        return R.layout.fragment_home;
    }

    //初始化方法
    @Override
    public void initData() {
        //找控件
        mVphHomeShuffling = (ViewPager) get(R.id.vp_home_shuffling);
        mLlHomePoint = (LinearLayout) get(R.id.ll_home_point);
        mRvHomeBottomshow = (RecyclerView) get(R.id.rv_home_bottomshow);
        mRefresh = get(R.id.mrl_opensource_home_refresh);
        mRlHomeEveny = get(R.id.rl_home_eveny);
        mRlHomeLive = (PercentRelativeLayout) get(R.id.rl_home_live);
        mIvHomeTop = (ImageView) get(R.id.iv_home_top);
        mIvHomeGithub =(ImageView)get(R.id.iv_home_github);
        mIvHomeWen =(ImageView)get(R.id.iv_home_wen);
        mRlHomeVip =(PercentRelativeLayout)get(R.id.rl_home_vip);
        mIvHomeDay = get(R.id.iv_home_day);
        //设置点击事件
        mIvHomeDay.setOnClickListener(this);
        mRlHomeEveny.setOnClickListener(this);
        mRlHomeLive.setOnClickListener(this);
        mIvHomeTop.setOnClickListener(this);
        mIvHomeGithub.setOnClickListener(this);
        mIvHomeWen.setOnClickListener(this);
        mRlHomeVip.setOnClickListener(this);
        //设置允许上拉加载
        mRefresh.setLoadMore(true);
        mRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                dohttpbottom();
                dohttp();
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                super.onRefreshLoadMore(materialRefreshLayout);
                dohttpbottom();
                dohttp();
            }
        });
        //联网请求
        dohttp();
        mMyAdPater = new MyAdPater();
        handler.sendEmptyMessageDelayed(0, 3000);
        //底部的适配器
        mHomeBottomAdapter = new HomeBottomAdapter(mHomeBottomlist, context);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mRvHomeBottomshow.setLayoutManager(linearLayoutManager);
        mRvHomeBottomshow.setAdapter(mHomeBottomAdapter);
        //请求底部
        dohttpbottom();
        mVphHomeShuffling.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                setPoint(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    //获取上下文
    private Context context;

    @Override
    public void getContext(Context context) {
        this.context = context;
    }

    //底部网络请求
    private void dohttpbottom() {
        Map map = new HashMap<>();
        map.put("news_type", 1);

        new Utility(context).isLoadCache(Http.BASE_BOTTOM, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                HomeBottomBean homeBottomBean = JsonToBean.jsonToBean(data, HomeBottomBean.class);
                List<HomeBottomBean.ItemsBean> items = homeBottomBean.getItems();
                mHomeBottomlist.addAll(items);
                mHomeBottomAdapter.notifyDataSetChanged();
                mRefresh.finishRefresh();
                mRefresh.finishRefreshLoadMore();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.BASE_BOTTOM, map);
    }

    //轮播联网请求
    private void dohttp() {
        new Utility(context).isLoadCache(Http.BASE_SHUFFLING, true, true).result(new HttpListener() {

            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                HomeShufflingBean homeBeanShuffling = JsonToBean.jsonToBean(data, HomeShufflingBean.class);
                mImageList = homeBeanShuffling.getData();
                setPoint(0);
                mVphHomeShuffling.setAdapter(mMyAdPater);
                mVphHomeShuffling.setCurrentItem((Integer.MAX_VALUE / 2) % mImageList.size());
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.BASE_SHUFFLING, null);
    }

    //小圆点
    private void setPoint(int p) {
        mLlHomePoint.removeAllViews();
        for (int i = 0; i < mImageList.size(); i++) {
            ImageView imageView = new ImageView(context);
            if (i == p % mImageList.size()) {
                imageView.setBackgroundResource(R.drawable.pointwhite);
            } else {
                imageView.setBackgroundResource(R.drawable.pointgray);
            }
            mLlHomePoint.addView(imageView);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imageView.getLayoutParams();
            params.width = 15;
            params.height = 15;
            params.leftMargin = 10;
            imageView.setLayoutParams(params);

        }
    }

    //点击事件
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_home_eveny:
                context.startActivity(new Intent(context, PlayActivity.class));
                break;
            case R.id.rl_home_live:
                context.startActivity(new Intent(context, GroupcChatActivity.class));
                break;
            case R.id.iv_home_top:
                context.startActivity(new Intent(context, BbsActivity.class));
                break;
            case R.id.rl_home_vip:
                context.startActivity(new Intent(context,VipActivity.class));
                break;
            case R.id.iv_home_wen:
                context.startActivity(new Intent(context, QuestionActivity.class));
                break;
            case R.id.iv_home_github:
                context.startActivity(new Intent(context,ProjectActivity.class));
                break;
            case R.id.iv_home_day:
                context.startActivity(new Intent(context,TechnicalActivity.class));
                break;

        }
    }


    //轮播适配器
    class MyAdPater extends PagerAdapter {
        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            //赋值
            ImageView view = new ImageView(context);
            Picasso.with(context).load(mImageList.get(position % mImageList.size()).getImage()).fit().into(view);
            container.addView(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, LinkActivity.class);
                    intent.putExtra("link", mImageList.get(position%mImageList.size()).getLink());
                    context.startActivity(intent);
                }
            });
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    //发送消息
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0) {
                int item = mVphHomeShuffling.getCurrentItem();
                mVphHomeShuffling.setCurrentItem(item += 1);
                handler.sendEmptyMessageDelayed(0, 3000);
            }
        }
    };

}
