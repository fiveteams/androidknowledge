package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.BbsActivity;
import com.five.androidknowledge.activity.LoginActivity;
import com.five.androidknowledge.activity.SendPostActivity;
import com.five.androidknowledge.fragment.AllPostFragment;
import com.five.androidknowledge.fragment.GoodPostFragment;
import com.five.androidknowledge.fragment.HotPostFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者：秦永聪
 * 日期：2018/12/24
 * 内容：
 */
public class SendPostActivityPresenter extends AppDelegate {


    private EditText mEtSendpostTitle;
    private EditText mEtSendpostCont;
    private Button mBSendpostSend;
    private String mEtSendpostTitleGet;
    private String mEtSendpostContGet;
    private String mUserId;

    @Override
    public int getlayoutId() {
        return R.layout.activity_sendpost;
    }

    private Context mContext;

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }

    @Override
    public void initData() {
        mEtSendpostTitle = (EditText) get(R.id.et_sendpost_title);
        mEtSendpostCont = (EditText) get(R.id.et_sendpost_cont);
        mBSendpostSend = (Button) get(R.id.b_sendpost_send);
        //点击事件
        mBSendpostSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUserId = SharedPreUtils.getString(mContext, "userId", "");
                mEtSendpostTitleGet = mEtSendpostTitle.getText().toString().trim();
                mEtSendpostContGet = mEtSendpostCont.getText().toString().trim();
                if (TextUtils.isEmpty(mEtSendpostTitleGet)) {
                    Toast.makeText(mContext, "标题不能为空", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(mEtSendpostContGet)) {
                    Toast.makeText(mContext, "内容不能为空", Toast.LENGTH_LONG).show();
                    return;
                }

                if (mUserId == null && TextUtils.isEmpty(mUserId)) {
                    mContext.startActivity(new Intent(mContext, LoginActivity.class));
                    return;
                }
                doHttp();
            }


        });
    }

    private void doHttp() {
        String mUserNickName = SharedPreUtils.getString(mContext, "userNickName", "");
        Map map = new HashMap<>();
        map.put("msg_user", mUserId);
        map.put("msg_nick", mUserNickName);
        map.put("msg_content", mEtSendpostContGet);
        map.put("msg_zan", mEtSendpostTitleGet);
        map.put("msg_id", 0);
        map.put("msg_hot", 0);
        new Utility(mContext).isLoadCache(Http.BASE_SENDPOST, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
            }

            @Override
            public void fail(String error) {
                Toast.makeText(mContext, "发表成功", Toast.LENGTH_LONG).show();
                ((SendPostActivity) mContext).finish();
            }

        }).get(Http.BASE_SENDPOST, map);


    }
}
