package com.five.androidknowledge.presenter.activitypresenter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.SharedPreUtils;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.five.androidknowledge.R;
import com.five.androidknowledge.adapter.PostsAdapter;
import com.five.androidknowledge.model.PostsBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author：刘进
 * date:2018/12/27
 * 我的贴子
 * */
public class PostsActivityPresenter extends AppDelegate {
    private Context mContext;
    private RecyclerView mRecyclerView;
    private PostsAdapter postsAdapter;

    @Override
    public int getlayoutId() {
        return R.layout.activity_posts;
    }

    @Override
    public void initData() {
        mRecyclerView = get(R.id.recyclerView1);
        postsAdapter = new PostsAdapter(mContext);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(postsAdapter);
        //我的贴子
        postsHttp();
    }
    //我的贴子

    private void postsHttp() {
        Map<String,String> map = new HashMap<>();
        String userId = SharedPreUtils.getString(mContext, "userId");
        map.put("msg_user",userId);
        new Utility(mContext).isLoadCache(Http.MYPOSTS,false,true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null){
                    return;
                }
                PostsBean postsBean = JsonToBean.jsonToBean(data, PostsBean.class);
                List<PostsBean.ItemsBean> items = postsBean.getItems();
                postsAdapter.setList(items);
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.MYPOSTS,map);
    }

    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }
}
