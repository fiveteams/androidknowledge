package com.five.androidknowledge.presenter.fragmentpresenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.example.mvplibrary.mvp.view.AppDelegate;
import com.example.mvplibrary.utils.JsonToBean;
import com.example.mvplibrary.utils.net.Http;
import com.example.mvplibrary.utils.net.HttpListener;
import com.example.mvplibrary.utils.net.Utility;
import com.facebook.drawee.view.SimpleDraweeView;
import com.five.androidknowledge.R;
import com.five.androidknowledge.activity.MainActivity;
import com.five.androidknowledge.adapter.VideoAdapter;
import com.five.androidknowledge.model.VideoBean;

import androidknowledge.five.com.video.activity.DisplayActivity;
import androidknowledge.five.com.video.activity.MoreVideoActivity;

/**
 * 作者：杜威
 * 日期：2018/12/11
 * 内容：视频页面
 **/
public class VideoFragmentPresenter extends AppDelegate {

    private Context mContext;
    private SimpleDraweeView mVideoImage;
    private TextView mVideoTitle;
    private RecyclerView mVideoItem;
    private VideoAdapter mVideoAdapter;
    private ScrollView mScrollView;
    private RelativeLayout mRelativeLayout;
    private VideoBean mVideoBean;
    private Button mBtnVideoMore;
    private MaterialRefreshLayout mRefresh;

    /**
     * 获取布局
     */
    @Override
    public int getlayoutId() {
        return R.layout.fragment_video;
    }

    /**
     * 初始化方法
     */
    @Override
    public void initData() {
        //初始化控件
        mVideoImage = get(R.id.sdv_video_title_image);
        mVideoTitle = get(R.id.tv_video_title);
        mVideoItem = get(R.id.rv_video_videoitem);
        mScrollView = get(R.id.sv_video_scrollview);
        mRelativeLayout = get(R.id.rl_video_title);
        mBtnVideoMore = get(R.id.btn_video_more);
        mRefresh = get(R.id.mrl_video_refresh);
        //设置布局管理器
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mVideoItem.setLayoutManager(staggeredGridLayoutManager);
        //设置adapter
        mVideoAdapter = new VideoAdapter(mContext);
        mVideoItem.setAdapter(mVideoAdapter);
        //头部Image点击事件
        mVideoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(((MainActivity) mContext), DisplayActivity.class);
                intent.putExtra("videoPosition", mVideoBean.getPosition());
                intent.putExtra("videoTitle", mVideoBean.getTitle());
                mContext.startActivity(intent);
            }
        });
        //更多视频点击事件
        mBtnVideoMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转至MoreViedoActivity
                Intent intent = new Intent(((MainActivity) mContext), MoreVideoActivity.class);
                mContext.startActivity(intent);
            }
        });
        //刷新监听事件
        mRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                doHttpVideo();
            }
        });
        //video请求数据
        doHttpVideo();
    }

    /**
     * Video请求网络数据
     */
    private void doHttpVideo() {
        new Utility(mContext).isLoadCache(Http.VIDEO_SHOW, true, true).result(new HttpListener() {
            @Override
            public void success(String data) {
                if (data == null) {
                    return;
                }
                mVideoBean = JsonToBean.jsonToBean(data, VideoBean.class);
                mVideoImage.setImageURI(mVideoBean.getImage());
                mVideoTitle.setText(mVideoBean.getTitle());
                mVideoAdapter.setList(mVideoBean.getItems());
                //关闭刷新
                mRefresh.finishRefreshLoadMore();
                mRefresh.finishRefresh();
            }

            @Override
            public void fail(String error) {

            }
        }).get(Http.VIDEO_SHOW, null);
    }

    /**
     * 获取上下文
     */
    @Override
    public void getContext(Context context) {
        this.mContext = context;
    }
}
