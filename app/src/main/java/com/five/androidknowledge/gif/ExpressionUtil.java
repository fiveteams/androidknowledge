package com.five.androidknowledge.gif;

import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import com.five.androidknowledge.R;


public class ExpressionUtil {
	public static String zhengze = "face[0-9]{1,}";
    
    public static SpannableString getExpressionString(Context context, String str, Hashtable<Integer, GifDrawalbe> cache, Vector<GifDrawalbe> drawables){
    	SpannableString spannableString = new SpannableString(str);
        Pattern sinaPatten = Pattern.compile(zhengze, Pattern.CASE_INSENSITIVE);		//ͨ�����������ʽ�����һ��pattern
        try {
            dealExpression(context, spannableString, sinaPatten, 0, cache, drawables);
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return spannableString;
    }
    
	public static void dealExpression(Context context, SpannableString spannableString, Pattern patten, int start, Hashtable<Integer, GifDrawalbe> cache, Vector<GifDrawalbe> drawables) throws Exception {
		Matcher matcher = patten.matcher(spannableString);
		while (matcher.find()) {
			String key = matcher.group();
			if (matcher.start() < start) {
				continue;
			}
			Field field = R.drawable.class.getDeclaredField(key);
			int id = Integer.parseInt(field.get(null).toString());	
			if (id != 0) {
				GifDrawalbe mSmile = null;
				if (cache.containsKey(id)) {
					mSmile = cache.get(id);
				} else {
					mSmile = new GifDrawalbe(context, id);
					cache.put(id, mSmile);
				}
				//GifDrawalbe drawable= (GifDrawalbe) context.getResources().getDrawable(id);
				/*Drawable drawable =context.getResources().getDrawable(id);
				drawable.setBounds(2, 2,
						48+2,
						48+2);*/
				ImageSpan span = new ImageSpan(mSmile, ImageSpan.ALIGN_BASELINE);
				int mstart = matcher.start();
				int end = mstart + key.length();
				spannableString.setSpan(span, mstart, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				if (!drawables.contains(mSmile))
					drawables.add(mSmile);
			}
		}
	}
}