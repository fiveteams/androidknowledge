package com.five.androidknowledge.gif;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.five.androidknowledge.R;

/**
 * Created by xiaoming.li on 2016/5/25.
 */
public class GifExpression extends RelativeLayout implements AdapterView.OnItemClickListener, ViewPager.OnPageChangeListener {
    public  Context context;
    private ArrayList<GridView> grids = new ArrayList<GridView>();
    public GridView gView1,gView2,gView3,gView4;
    private int[] expressionImages1 = null;
    private int[] expressionImages2 = null;
    private int[] expressionImages3 = null;
    private int[] expressionImages4 = null;
    private String[] expressionImageNames1 = null;
    private String[] expressionImageNames2 = null;
    private String[] expressionImageNames3 = null;
    private String[] expressionImageNames4 = null;
    private ViewPager mViewPager;
    private LinearLayout mPoint;
    private GifEditText viewEdit;

    //    private InteractionPager interactionPager;
//
//    public void setInteractionPager(InteractionPager interactionPager){
//        this.interactionPager=interactionPager;
//    }
    public GifExpression(Context context) {
        super(context);
        this.context=context;
        initView(context);
    }

    public GifExpression(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        initView(context);
    }
    public GifExpression(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
        initView(context);
    }

    private void initView(Context context) {
        View view= View.inflate(context, R.layout.gif_expression,null);
        mViewPager=(ViewPager)view.findViewById(R.id.face_viewpager);
        mPoint=(LinearLayout)view.findViewById(R.id.face_dots_container);
        initExpressions(context);
        initViewPager(context);
        addView(view);
    }

    //引入表情
    private void initExpressions(Context context) {
        expressionImages1 = Expressions.expressionImgs1;
        expressionImages2 = Expressions.expressionImgs2;
        expressionImages3 = Expressions.expressionImgs3;
        expressionImages4 = Expressions.expressionImgs4;
        expressionImageNames1 = Expressions.expressionImgNames1;
        expressionImageNames2 = Expressions.expressionImgNames2;
        expressionImageNames3 = Expressions.expressionImgNames3;
        expressionImageNames4 = Expressions.expressionImgNames4;
    }
    private void initViewPager(Context context) {
        grids = new ArrayList<GridView>();
        gView1 = new GridView(context);
        gView2 = new GridView(context);
        gView3 = new GridView(context);
        gView4 = new GridView(context);

        gView1.setNumColumns(6);
        gView2.setNumColumns(6);
        gView3.setNumColumns(6);
        gView4.setNumColumns(6);

        gView1.setVerticalSpacing(10);
        gView2.setVerticalSpacing(10);
        gView3.setVerticalSpacing(10);
        gView4.setVerticalSpacing(10);

        gView1.setGravity(Gravity.CENTER);
        gView2.setGravity(Gravity.CENTER);
        gView3.setGravity(Gravity.CENTER);
        gView4.setGravity(Gravity.CENTER);

        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        // 生成24个表情
        for (int i = 0; i < 24; i++) {
            Map<String, Object> listItem = new HashMap<String, Object>();
            listItem.put("image", expressionImages1[i]);
            listItems.add(listItem);
        }
        SimpleAdapter simpleAdapter = new SimpleAdapter(context, listItems, R.layout.singleexpression,
                new String[] { "image" }, new int[] { R.id.image });
        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(this);

        grids.add(gView1);
        grids.add(gView2);
        grids.add(gView3);
        grids.add(gView4);


        mFacePagerAdapter=new FacePagerAdapter();
        mViewPager.setAdapter(mFacePagerAdapter);
        mViewPager.addOnPageChangeListener(this);


        selectedPoint(0);

    }

    private void selectedPoint(int position){
        mPoint.setVisibility(VISIBLE);
        mPoint.removeAllViews();
        for (int a=0;a<grids.size();a++){
            ImageView imageView=new ImageView(context);
            if(position==a){
                imageView.setBackgroundResource(R.drawable.gif_selected);
            }else{
                imageView.setBackgroundResource(R.drawable.gif_no);
            }
            mPoint.addView(imageView);
            LinearLayout.LayoutParams params= (LinearLayout.LayoutParams) imageView.getLayoutParams();
            params.leftMargin=10;
            imageView.setLayoutParams(params);
        }
    }

    private FacePagerAdapter mFacePagerAdapter;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        if (parent == gView4){
            if(position==11){
                pagerClick();
                return;
            }
        }else{
            if(position==23){
                pagerClick();
                return;
            }
        }
        if (parent == gView1) {
            viewEdit.insertGif(expressionImageNames1[position].substring(1, expressionImageNames1[position].length() - 1)+" ");
        } else if (parent == gView2) {
            viewEdit.insertGif(expressionImageNames2[position].substring(1, expressionImageNames2[position].length() - 1)+" ");
        } else if (parent == gView3) {
            viewEdit.insertGif(expressionImageNames3[position].substring(1, expressionImageNames3[position].length() - 1)+" ");
        }else if (parent == gView4) {
            viewEdit.insertGif(expressionImageNames4[position].substring(1, expressionImageNames4[position].length() - 1)+" ");
        }
        String msg=viewEdit.getText().toString();
        viewEdit.setSelection(msg.length());
    }

    private void pagerClick() {
        int action = KeyEvent.ACTION_DOWN;
        int code = KeyEvent.KEYCODE_DEL;
        KeyEvent event = new KeyEvent(action, code);
        String msg=viewEdit.getText().toString();
        if(msg.endsWith(" ")){
            viewEdit.onKeyDown(KeyEvent.KEYCODE_DEL, event);
            viewEdit.onKeyDown(KeyEvent.KEYCODE_DEL, event);
        }else{
            viewEdit.onKeyDown(KeyEvent.KEYCODE_DEL, event);
        }
    }

    public void setViewEdit(GifEditText viewEdit) {
        this.viewEdit = viewEdit;
    }


    private class FacePagerAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return grids.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ((ViewPager) container).addView(grids.get(position));
            return grids.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView(grids.get(position));
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        selectedPoint(position);
        switch (position) {
            case 1:
                List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
                // 生成24个表情
                for (int i = 0; i < 24; i++) {
                    Map<String, Object> listItem = new HashMap<String, Object>();
                    listItem.put("image", expressionImages2[i]);
                    listItems.add(listItem);
                }
                SimpleAdapter simpleAdapter = new SimpleAdapter(
                        context, listItems,
                        R.layout.singleexpression, new String[] { "image" },
                        new int[] { R.id.image });
                gView2.setAdapter(simpleAdapter);
                gView2.setOnItemClickListener(this);
                break;
            case 2:
                List<Map<String, Object>> listItems1 = new ArrayList<Map<String, Object>>();
                // 生成24个表情
                for (int i = 0; i < 24; i++) {
                    Map<String, Object> listItem = new HashMap<String, Object>();
                    listItem.put("image", expressionImages3[i]);
                    listItems1.add(listItem);
                }

                SimpleAdapter simpleAdapter1 = new SimpleAdapter(
                        context, listItems1,
                        R.layout.singleexpression, new String[] { "image" },
                        new int[] { R.id.image });
                gView3.setAdapter(simpleAdapter1);
                gView3.setOnItemClickListener(this);
                break;
            case 3:
                List<Map<String, Object>> listItems2 = new ArrayList<Map<String, Object>>();
                // 生成12个表情
                for (int i = 0; i < 12; i++) {
                    Map<String, Object> listItem = new HashMap<String, Object>();
                    listItem.put("image", expressionImages4[i]);
                    listItems2.add(listItem);
                }

                SimpleAdapter simpleAdapter2 = new SimpleAdapter(
                        context, listItems2,
                        R.layout.singleexpression, new String[] { "image" },
                        new int[] { R.id.image });
                gView4.setAdapter(simpleAdapter2);
                gView4.setOnItemClickListener(this);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
