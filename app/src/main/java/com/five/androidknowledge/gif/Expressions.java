package com.five.androidknowledge.gif;


import com.five.androidknowledge.R;

public class Expressions {
    public static int[] expressionImgs1 = new int[]{
            R.drawable.face1,R.drawable.face2, R.drawable.face3, R.drawable.face4,
            R.drawable.face5, R.drawable.face6, R.drawable.face7, R.drawable.face8,
            R.drawable.face9, R.drawable.face10, R.drawable.face11, R.drawable.face12,
            R.drawable.face13, R.drawable.face14, R.drawable.face15, R.drawable.face16,
            R.drawable.face17, R.drawable.face18, R.drawable.face19, R.drawable.face20,
            R.drawable.face21, R.drawable.face22, R.drawable.face23, R.drawable.face0};

    public static String[] expressionImgNames1 = new String[]{
            "[face1]", "[face2]", "[face3]", "[face4]", "[face5]", "[face6]",
            "[face7]", "[face8]", "[face9]", "[face10]", "[face11]", "[face12]",
            "[face13]", "[face14]", "[face15]", "[face16]", "[face17]", "[face18]",
            "[face19]", "[face20]", "[face21]", "[face22]", "[face23]", "[face0]"};

    public static int[] expressionImgs2 = new int[]{
            R.drawable.face24, R.drawable.face25, R.drawable.face26, R.drawable.face27,
            R.drawable.face28, R.drawable.face29, R.drawable.face30, R.drawable.face31,
            R.drawable.face32, R.drawable.face33, R.drawable.face34, R.drawable.face35,
            R.drawable.face36, R.drawable.face37, R.drawable.face38, R.drawable.face39,
            R.drawable.face40, R.drawable.face41, R.drawable.face42, R.drawable.face43,
            R.drawable.face44, R.drawable.face45, R.drawable.face46, R.drawable.face0};

    public static String[] expressionImgNames2 = new String[]{
            "[face24]", "[face25]", "[face26]", "[face27]", "[face28]", "[face29]",
            "[face30]", "[face31]", "[face32]", "[face33]", "[face34]", "[face35]",
            "[face36]", "[face37]", "[face38]", "[face39]", "[face40]", "[face41]",
            "[face42]", "[face43]", "[face44]", "[face45]", "[face46]", "[face0]"};

   public static int[] expressionImgs3 = new int[]{
            R.drawable.face47,  R.drawable.face48, R.drawable.face49, R.drawable.face50,
            R.drawable.face51, R.drawable.face52, R.drawable.face53, R.drawable.face54,
            R.drawable.face55, R.drawable.face56, R.drawable.face57, R.drawable.face58,
            R.drawable.face59, R.drawable.face60, R.drawable.face61, R.drawable.face62,
            R.drawable.face63, R.drawable.face64, R.drawable.face65, R.drawable.face66,
            R.drawable.face67, R.drawable.face68, R.drawable.face69, R.drawable.face0
            };

    public static String[] expressionImgNames3 = new String[]{
            "[face47]", "[face48]",
            "[face49]", "[face50]", "[face51]", "[face52]", "[face53]", "[face54]",
            "[face55]", "[face56]", "[face57]", "[face58]", "[face59]", "[face60]",
            "[face61]", "[face62]", "[face63]", "[face64]", "[face65]", "[face66]",
            "[face67]", "[face68]", "[face69]",  "[face0]"};

  public static int[] expressionImgs4 = new int[]{
            R.drawable.face70,R.drawable.face71,
            R.drawable.face72, R.drawable.face73, R.drawable.face74, R.drawable.face75, R.drawable.face76,
            R.drawable.face77, R.drawable.face78, R.drawable.face79, R.drawable.face80, R.drawable.face0
    };
    public static String[] expressionImgNames4 = new String[]{
            "[face70]", "[face71]",
            "[face72]", "[face73]", "[face74]", "[face75]", "[face76]", "[face77]", "[face78]",
            "[face79]", "[face80]", "[face0]",
    };

    public static String[] replaceStrings(String[] str, String[] str2) {
        String newStr[] = new String[str.length - 1];
        for (int i = 0; i < str.length; i++) {
            newStr[i] = str[i].replace(str[i], str2[i]);
        }
        return newStr;
    }

}
