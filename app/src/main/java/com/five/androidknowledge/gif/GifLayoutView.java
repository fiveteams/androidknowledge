package com.five.androidknowledge.gif;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.five.androidknowledge.R;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * author:AbnerMing
 * date:2018/12/14
 */
public class GifLayoutView extends RelativeLayout implements View.OnClickListener {
    private GifExpression mGifExpression;
    private ImageView mImageView;
    private GifEditText mEditText;

    public GifLayoutView(Context context) {
        super(context);
        initView(context);
    }


    public GifLayoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public GifLayoutView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private Context context;

    private void initView(Context context) {
        this.context = context;
        View view = View.inflate(context, R.layout.gif_layout_view, null);
        mGifExpression = (GifExpression) view.findViewById(R.id.gifexpression);
        mImageView = (ImageView) view.findViewById(R.id.emoji_click);
        mEditText = (GifEditText) view.findViewById(R.id.edit_text);

        mGifExpression.setViewEdit(mEditText);
        mImageView.setOnClickListener(this);
        view.findViewById(R.id.send).setOnClickListener(this);

        new KeyboardChangeListener((Activity) context).setKeyBoardListener(new KeyboardChangeListener.KeyBoardListener() {
            @Override
            public void onKeyboardChange(boolean isShow, int keyboardHeight) {
                if(isShow){
                    setShowGif(!isShow);
                }

            }
        });


        addView(view);

    }


    private boolean isClick = true;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.emoji_click:
                if (isClick) {
                    hintInputMethodManager();
                    setShowGif(isClick);
                } else {
                    setShowGif(isClick);
                }
                break;
            case R.id.send:
                String message=mEditText.getText().toString().trim();
                if(listener!=null){
                    listener.send(message);
                }
                break;
        }
    }

    //发送数据
    private SendMessageListener listener;
    public void sendMessage(SendMessageListener listener){
        this.listener=listener;
    }

    public interface SendMessageListener{
        void send(String message);
    }

    private void setShowGif(boolean isShow) {
        if (isShow) {
            mImageView.setImageResource(R.drawable.emoji_fill);
            setGifExpressionHeight(600);
            isClick = false;
        } else {
            mImageView.setImageResource(R.drawable.emoji_no);
            setGifExpressionHeight(0);
            isClick = true;
        }
    }

    private void setGifExpressionHeight(int size) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGifExpression.getLayoutParams();
        params.height = size;
        mGifExpression.setLayoutParams(params);
    }

    //隐藏
    public void hintInputMethodManager() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        //隐藏键盘
        imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }

    //显示
    public void showInputMethodManager() {
        InputMethodManager inputManager = (InputMethodManager) mEditText.getContext().getSystemService(INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(mEditText, 0);
    }


    private boolean isSoftShowing() {
        //获取当前屏幕内容的高度
        int screenHeight = ((Activity) context).getWindow().getDecorView().getHeight();
        //获取View可见区域的bottom
        Rect rect = new Rect();
        //DecorView即为activity的顶级view
        ((Activity) context).getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        //考虑到虚拟导航栏的情况（虚拟导航栏情况下：screenHeight = rect.bottom + 虚拟导航栏高度）
        //选取screenHeight*2/3进行判断
        return screenHeight*2 / 3 > rect.bottom;
    }

}
