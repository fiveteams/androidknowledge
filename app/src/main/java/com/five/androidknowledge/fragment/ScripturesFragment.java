package com.five.androidknowledge.fragment;

import com.example.mvplibrary.mvp.presenter.BaseFragmentPresenter;
import com.five.androidknowledge.presenter.fragmentpresenter.ScripturesFragmentPresenter;

/**
 * 作者：马利亚
 * 日期：2018/12/11
 * 内容：经典
 **/
public class ScripturesFragment extends BaseFragmentPresenter<ScripturesFragmentPresenter>{

    @Override
    public Class<ScripturesFragmentPresenter> getPresenter() {
        return ScripturesFragmentPresenter.class;
    }
}
