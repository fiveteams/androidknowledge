package com.five.androidknowledge.fragment;

import com.example.mvplibrary.mvp.presenter.BaseFragmentPresenter;
import com.five.androidknowledge.presenter.fragmentpresenter.VideoFragmentPresenter;

/**
 * 作者：杜威
 * 日期：2018/12/11
 * 内容：视频
 **/
public class VideoFragment extends BaseFragmentPresenter<VideoFragmentPresenter>{
    @Override
    public Class<VideoFragmentPresenter> getPresenter() {
        return VideoFragmentPresenter.class;
    }
}
