package com.five.androidknowledge.fragment;

import com.example.mvplibrary.mvp.presenter.BaseFragmentPresenter;
import com.five.androidknowledge.presenter.fragmentpresenter.HomeFragmentPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/11
 * 内容：首页
 **/
public class HomeFragment extends BaseFragmentPresenter<HomeFragmentPresenter>{
    @Override
    public Class<HomeFragmentPresenter> getPresenter() {
        return HomeFragmentPresenter.class;
    }
}
