package com.five.androidknowledge.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.mvplibrary.mvp.presenter.BaseFragmentPresenter;
import com.five.androidknowledge.presenter.fragmentpresenter.AccountFragmentPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/11
 * 内容：我的
 **/
public class UserFragment extends BaseFragmentPresenter<AccountFragmentPresenter>{
    @Override
    public Class<AccountFragmentPresenter> getPresenter() {
        return AccountFragmentPresenter.class;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //presenter.onactivityresult(requestCode,resultCode,data);
    }
}
