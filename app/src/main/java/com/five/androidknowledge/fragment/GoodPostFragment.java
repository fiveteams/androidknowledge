package com.five.androidknowledge.fragment;

import com.example.mvplibrary.mvp.presenter.BaseFragmentPresenter;
import com.five.androidknowledge.presenter.fragmentpresenter.GoodPostFragmentPresenter;
import com.five.androidknowledge.presenter.fragmentpresenter.HostPostFragmentPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/21
 * 内容：
 */
public class GoodPostFragment extends BaseFragmentPresenter<GoodPostFragmentPresenter> {
    @Override
    public Class<GoodPostFragmentPresenter> getPresenter() {
        return GoodPostFragmentPresenter.class;
    }
}
