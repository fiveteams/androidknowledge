package com.five.androidknowledge.fragment;


import com.example.mvplibrary.mvp.presenter.BaseFragmentPresenter;
import com.five.androidknowledge.presenter.fragmentpresenter.HistoryFragmentPresenter;

public class HistoryFragment extends BaseFragmentPresenter<HistoryFragmentPresenter>{

    @Override
    public Class<HistoryFragmentPresenter> getPresenter() {
        return HistoryFragmentPresenter.class;
    }

    @Override
    public void initWeight() {
        super.initWeight();
        String id = getArguments().getString("id");
        presenter.sentType(id);
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
