package com.five.androidknowledge.fragment;

import com.example.mvplibrary.mvp.presenter.BaseFragmentPresenter;
import com.five.androidknowledge.presenter.fragmentpresenter.StudyFragmentPresenter;

/**
 * 作者：刘进
 * 日期：2018/12/11
 * 内容：学习
 **/
public class StudyFragment extends BaseFragmentPresenter<StudyFragmentPresenter>{
    @Override
    public Class<StudyFragmentPresenter> getPresenter() {
        return StudyFragmentPresenter.class;
    }
}
