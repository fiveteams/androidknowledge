package com.five.androidknowledge.fragment;

import com.example.mvplibrary.mvp.presenter.BaseFragmentPresenter;
import com.five.androidknowledge.presenter.fragmentpresenter.AllPostFragmentPresenter;

/**
 * 作者：秦永聪
 * 日期：2018/12/20
 * 内容：
 */
public class AllPostFragment extends BaseFragmentPresenter<AllPostFragmentPresenter> {
    @Override
    public Class<AllPostFragmentPresenter> getPresenter() {
        return AllPostFragmentPresenter.class;
    }
    @Override
    public void initWeight() {
        super.initWeight();
        String id = getArguments().getString("id");
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }
}
