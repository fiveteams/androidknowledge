package com.example.mvplibrary.utils.net;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.mvplibrary.utils.CacheUtils;
import com.example.mvplibrary.utils.LodingDialog;
import com.example.mvplibrary.utils.NetworkUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/**
 * 作者：刘进
 * 日期：2018/12/10
 * 网络请求工具类
 **/
public class Utility {
    private Context context;
    private BaseService mBaseService;
    private String type;
    private boolean isCache;
    private boolean load;
    private LodingDialog lodingDialog;

    public Utility(Context context) {
        this.context = context;
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Http.BASE_URL)
                .build();
        mBaseService = retrofit.create(BaseService.class);
        //获取数据库
        lodingDialog = new LodingDialog.Builder(context).create();
    }

    /**
     * 判断存储类型和是否进行缓存
     */
    public Utility isLoadCache(String type, boolean isCache, boolean load) {
        this.type = type;
        this.isCache = isCache;
        this.load = load;
        return this;
    }

    /**
     * get请求
     */

    public Utility get(String url, Map<String, String> map) {
        if (load) {
            lodingDialog.show();
        }
        if (isCache && !NetworkUtils.isConnected(context)) {
            String data = CacheUtils.getCacheUtils().query(type);
            listener.success(data);
            lodingDialog.dismiss();
            Toast.makeText(context,"请检查网络状态！",Toast.LENGTH_SHORT).show();
            return this;
        }
        if (map == null) {
            map = new HashMap<>();
        }
        mBaseService.get(url, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
        return this;
    }
    public Utility part(String url, Map<String,String> map, MultipartBody.Part part){
        if(map==null){
            map=new HashMap<>();
        }
        mBaseService.part(url,map,part)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
        return this;
    }
    /**
     * post请求
     */
    public Utility post(String url, Map<String, String> map) {
        if (isCache && !NetworkUtils.isConnected(context)) {
            String data = CacheUtils.getCacheUtils().query(type);
            listener.success(data);
            lodingDialog.dismiss();
            return this;
        }
        if (map == null) {
            map = new HashMap<>();
        }
        mBaseService.post(url, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
        return this;
    }

    private Observer observer;

    {
        observer = new Observer<ResponseBody>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ResponseBody responseBody) {
                try {
                    String data = responseBody.string();
                    //请求网络数据成功后判断是否需要缓存数据
                    Log.i("aaa", type );
                    if (isCache) {
                        if (CacheUtils.getCacheUtils().query(type) == null) {
                            CacheUtils.getCacheUtils().insert(data, type);
                        } else {
                            CacheUtils.getCacheUtils().update(data, type);
                        }
                    }
                    listener.success(data);
                    lodingDialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                String error = e.getMessage();
                lodingDialog.dismiss();
                listener.fail(error);
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private HttpListener listener;

    public Utility result(HttpListener listener) {
        this.listener = listener;
        return this;
    }
}
