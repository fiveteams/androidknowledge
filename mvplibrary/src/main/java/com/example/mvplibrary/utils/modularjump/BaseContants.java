package com.example.mvplibrary.utils.modularjump;

/**
 * Created by lixiaoming on 7/21/16.
 */
public class BaseContants {

    public static final String MAIN_PROCESS_PACKAGE_NAME = "com.zhengjin99.infinitefinance";

    /**
     * 登录activity name
     */
    public static final String ACTION_FILTER_COMPONENT_LOGIN_ACTIVITY_NAME = "LoginActivity";
    /****
     * 登录界面
     */
    public static final String ACTION_FILTER_COMPONENT_ACTIVITY_SIGNIN_NAME = "com.jrj.infinitefinance.module.account.ActivitySignin";
}
