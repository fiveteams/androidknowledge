package com.example.mvplibrary.utils.net;

/**
 *作者：刘进
 *日期：2018/12/10
 **/
public interface HttpListener {
    void success(String data);
    void fail(String error);
}
