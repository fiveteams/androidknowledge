package com.example.mvplibrary.utils.modularjump;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;


/**
 * Created by lixiaoming on 8/4/16.
 */
public class BaseCoreHelperImpl implements BaseCoreHelper {
    public BaseCoreHelperImpl() {
    }
    /**
     * login
     *
     * @param mContext
     */
    public void openLogin(Context mContext) {
        Intent intent = new Intent();
        ComponentName componentName = new ComponentName(BaseContants.MAIN_PROCESS_PACKAGE_NAME,
                BaseContants.ACTION_FILTER_COMPONENT_LOGIN_ACTIVITY_NAME);
        intent.setComponent(componentName);
        mContext.startActivity(intent);

    }

    /**
     * 登录界面
     * @param mContext
     */
    public void openActivitySignin(Context mContext) {
        Intent intent = new Intent();
        ComponentName componentName = new ComponentName(BaseContants.MAIN_PROCESS_PACKAGE_NAME,
                BaseContants.ACTION_FILTER_COMPONENT_ACTIVITY_SIGNIN_NAME);
        intent.setComponent(componentName);
        //mContext.startActivity(intent);
        ((Activity) mContext).startActivityForResult(intent, 2000);
    }

}


