package com.example.mvplibrary.utils.net;
/**
 * @author:杜威
 * @date：2018/12/10
 * @function:接口
 * */
public class Http {
    public static final String BASE_URL = "http://www.abnerming.cn/";
    //轮播
    public static final String BASE_SHUFFLING = "/txt/banner_1.txt";
    //首页底部展示
    public static final String BASE_BOTTOM = "/cert/bw_get_index_news.php";
    //Video页面
    public static final String VIDEO_SHOW = "/txt/live/video_index.txt";
    //学习页面
    public static final String STUDY_SHOW = "/txt/study.txt";
    //经典
    public static String BASE_ARTICLE = "/cert/bw_get_index_news.php";
    //注册
    public static final String REGISTERED = "/cert/RegisterUser.php";
    //获取弹一弹的消息
    public static final String BASE_PLAY = "/cert/get_news_speak.php";
    //登陆
    public static final String USERLOGIN = "/cert/LoginUser.php";
    //获取邮箱验证码
    public static final String REGISTERED_EMAIL="http://www.vipandroid.cn/cert/SendMail.php";
    //判断用户是否已经注册
    public static final String ISREGISTERED="/cert/ISRegisterUser.php";
    //发送弹一弹

    //学习
    public static final String STUDY_CHILD="/txt/study_";
    //经典头部
    public static final String SCRIPTRURES_TITLE="txt/jingdian_title.txt";

    public static final String BASE_SEND="/cert/add_news_speak.php";
    //视频详情
    public static final String DISPLAY_URL="/txt/";
    //视频流
    public static final String VIDEO_STREAMING="/cert/gan_live.php?stock=";
    //更多视频头部
    public static final String MORE_VIDEO_TITLE="/txt/more_live.txt";
    //更多视频
    public static final String MORE_VIDEO_INFO="/txt/live_";
    //上传头像
    public static final String BASE_UPIMG="/cert/upload_image.php";
    //修改地址
    public static final String UPDATAEADDRESS="cert/ChangeUser_address.php";
    //修改昵称
    public static final String UPDATEUSER="/cert/ChangeUser_nickname.php";
    //修改手机号
    public static final String UPDATEPHONE="/cert/ChangeUser_phone.php";
    //修改公司名称
    public static final String UPDATAECOMPANY="/cert/ChangeUser_company.php";
    //修改职业
    public static final String UPDATEPROFESSIONAL="/cert/ChangeUser_occupation.php";
    //修改性别
    public static final String UPDATEAGE="/cert/ChangeUser_sex.php";

//    //修改年龄
//    public static final String UPDATEAGE="/cert/ChangeUser_sex.php";
//
//    //修改微博账号
//    public static final String UPDATEAGE="/cert/ChangeUser_sex.php";

    //修改性别
    public static final String UPDATEPASSWORD="/cert/change_pass.php";

    //上传头像
    public static final String UPDATEIMAGE="/cert/upload_image.php";

    //获取用户信息
    public static final String GETACCOUNT="/cert/get_user.php";

    //添加关注
    public static final String ADDCONCERN="/cert/add_my_student.php";

    //获取关注
    public static final String GETCONCERN="/cert/add_my_student.php";

    //判断是否关注
    public static final String ISADDCONCERN="/cert/is_my_student.php";

    //我的分享
    public static final String MYSHARE="cert/get_shared_ariticle_all.php";

    //获取我的关注
    public static final String MYCONCERN="/cert/get_my_student.php";

    //获取他的关注
    public static final String GETHISCONCERN="/cert/get_my_student_other.php";

    //取消关注
    public static final String CANCELCONCERN="/cert/delete_my_student.php";

    //我的贴子
    public static final String MYPOSTS="/cert/get_tree_msg_bbs_all_me.php";

    //获取群聊大厅
    public static final String BASE_GROUPCHAT = "/cert/get_android_msg.php";
    //添加群聊大厅
    public static final String BASE_ADDGROUPCHAT = "/cert/add_android_msg.php";
    //添加关注
    public static final String BASE_LOVE= "/cert/add_my_student.php";
    //判断是否关注
    public static final String BASE_ISLOVE= "/cert/is_my_student.php";
    //展示全部帖子
    public static final String BASE_ALLPOST= "/cert/get_tree_msg_bbs.php";
    //热门帖子
    public static final String BASE_HOSTPOST= "/cert/get_tree_msg_bbs3.php";
    //好帖子
    public static final String BASE_GOODPOST= "/cert/get_tree_msg_bbs2.php";
    //回复论坛
    public static final String BASE_REPLYPOST= "/cert/add_tree_msg_pl.php";
    //获取论坛消息
    public static final String BASE_GETMESSAGE= "/cert/get_tree_msg_pl.php";
    //发送论坛
    public static final String BASE_SENDPOST= "/cert/add_tree_msg.php";
    //展示信息
    public static final String BASE_INFORMATIONSHOW= "/cert/get_user_id.php";
    //取消关注
    public static final String BASE_CANCEL ="/cert/delete_my_student.php";
   //VIP
   public static final String BASE_VIP ="/cert/get_shared_ariticle_all.php";

}

