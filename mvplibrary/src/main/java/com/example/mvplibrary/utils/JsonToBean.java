package com.example.mvplibrary.utils;

import com.example.mvplibrary.mvp.basemodel.IModel;
import com.google.gson.Gson;
/**
 * @author:杜威
 * @date：2018/12/10
 * @function:Json转换Bean
 * */
public class JsonToBean {

    public static  <D extends IModel>D jsonToBean(String json, Class cls){
        Gson gson = new Gson();
        return (D) gson.fromJson(json,cls);
    }
}
