package com.example.mvplibrary.utils.modularjump;

import android.content.Context;
/**
 * Created by lixiaoming on 8/4/16.
 */
public class ActionHelper {
    BaseCoreHelper helper;

    public ActionHelper() {
        helper = new BaseCoreHelperImpl();
    }

    public void openLogin(Context mContext)  {
        helper.openLogin(mContext);
    }

}
