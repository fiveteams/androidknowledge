package com.example.mvplibrary.utils.listener;

import android.animation.Animator;

/**
 * @author:杜威
 * @Date:2018/12/11
 * @time:下午 6:43
 * @description:
 */
public abstract class AnimatorListener implements Animator.AnimatorListener {
    @Override
    public abstract void onAnimationStart(Animator animation);



    @Override
    public abstract void onAnimationEnd(Animator animation);

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
