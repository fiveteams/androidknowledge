package com.example.mvplibrary.mvp.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.mvplibrary.R;
import com.example.mvplibrary.mvp.view.IDelegate;
import com.example.mvplibrary.mvp.view.LayoutHead;
import com.example.mvplibrary.utils.UltimateBar;

/**
 * @author: 杜威
 * @date:2018/12/10
 * @function:Activity基类
 * */
public abstract class BaseActivityPresenter<T extends IDelegate> extends AppCompatActivity {

    protected T presenter;
    private RelativeLayout mLayoutHead;
    private LayoutHead mLayoutHeadView;

    public abstract Class<T> getPresenter();
    /**
     * 获取到对应的presenter
     * */
    public BaseActivityPresenter(){
        try {
            //返回对应的presenter
            presenter = getPresenter().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    /**
     *布局、视图、方法、上下文的创建和获取
     * */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //创建布局
        presenter.create(getLayoutInflater(),null,savedInstanceState);

        View view=View.inflate(this, R.layout.activity_base,null);
        mLayoutHead=(RelativeLayout)view.findViewById(R.id.layout_child);
        mLayoutHeadView=(LayoutHead)view.findViewById(R.id.layouthead);
        mLayoutHead.addView(presenter.rootView());
        //设置视图
        setContentView(view);
        //获取context
        presenter.getContext(this);
        initMethod();
        //初始化数据
        presenter.initData();
    }


    public void initMethod(){

    }
    public void setTitleShowOrHint(boolean isShow){
        if(isShow){
            mLayoutHeadView.setVisibility(View.VISIBLE);
        }else {
            mLayoutHeadView.setVisibility(View.GONE);
        }
    }

    public void setTitleName(String titleName){
        mLayoutHeadView.setmTitleName(titleName);
    }

    public void setImageBack(boolean showOrHide){
        mLayoutHeadView.setmImageBack(showOrHide);
    }
}
