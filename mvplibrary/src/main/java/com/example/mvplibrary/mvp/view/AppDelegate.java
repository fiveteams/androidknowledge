package com.example.mvplibrary.mvp.view;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
/**
 * @author:杜威
 * @date:2018/12/10
 * @function:presenter基类
 * */
public abstract class AppDelegate implements IDelegate {

    private View rootView;

    //获取视图
    @Override
    public void create(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {
      rootView =  inflater.inflate(getlayoutId(),viewGroup,false);
    }

    public abstract int getlayoutId();

    //返回视图
    @Override
    public View rootView() {
        return rootView;
    }
    //获取控件ID
    private SparseArray<View> views = new SparseArray<>();
    public <T extends View>T get(int id){
        T view = (T) views.get(id);
        if (view == null){//集合是否存在该view
            view = rootView.findViewById(id);
            views.put(id,view);
        }
        return view;
    }
    //点击事件
    public void setClick(View.OnClickListener listener,int...ids){
        if (ids == null){
            return;
        }
        for(int id : ids){
            get(id).setOnClickListener(listener);
        }
    }
}
