package com.example.mvplibrary.mvp.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mvplibrary.mvp.view.IDelegate;
/**
 * @author: 杜威
 * @date:2018/12/10
 * @function:Fragment基类
 * */
public abstract class BaseFragmentPresenter<T extends IDelegate> extends Fragment {

    protected   T presenter;

    public abstract Class<T> getPresenter();

    public BaseFragmentPresenter(){
        try {
            //获取对应的presenters
            presenter = getPresenter().newInstance();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //创建布局
        presenter.create(inflater,container,savedInstanceState);
        //返回视图
        return presenter.rootView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //获取上下文
        presenter.getContext(getActivity());
        initWeight();
        //初始化数据
        presenter.initData();
    }

    public void initWeight(){

    }
}
