package com.example.mvplibrary.mvp.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mvplibrary.R;
import com.google.gson.Gson;

/**
 * @author:杜威
 * @Date:2018/12/12
 * @time:下午 1:03
 * @description:
 */
public class LayoutHead extends RelativeLayout{
    private TextView mTitleName;
    private ImageView mImageBack;

    public LayoutHead(Context context) {
        super(context);
        initView(context);
    }

    public LayoutHead(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public LayoutHead(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(final Context context) {
        View view=View.inflate(context,R.layout.include_title,null);
        mTitleName=(TextView)view.findViewById(R.id.include_title_name);
        mImageBack = view.findViewById(R.id.include_back);
        mImageBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity)context).finish();
            }
        });
        addView(view);
    }

    /**
     * 设置Title
     * */
    public void setmTitleName(String titleName){
        mTitleName.setText(titleName);
    }

    public void setmImageBack(boolean showOrHide){
        if (showOrHide){
            mImageBack.setVisibility(VISIBLE);
        }else{
            mImageBack.setVisibility(GONE);
        }
    }

}
