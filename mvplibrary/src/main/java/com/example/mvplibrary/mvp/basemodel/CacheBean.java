package com.example.mvplibrary.mvp.basemodel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * @author:杜威
 * @Date:2018/12/13
 * @time:下午 4:23
 * @description:
 */
@Entity
public class CacheBean {
    @Id(autoincrement = true)
    private Long id;
    private String type;
    private String data;
    @Generated(hash = 1210120587)
    public CacheBean(Long id, String type, String data) {
        this.id = id;
        this.type = type;
        this.data = data;
    }
    @Generated(hash = 573552170)
    public CacheBean() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getData() {
        return this.data;
    }
    public void setData(String data) {
        this.data = data;
    }
    
}
